<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class IndexController extends AbstractController
{
    /**
     * @Route("/index", name="home_index")
     */
    public function index()
    {
        $number = random_int(0, 100);
		return $this->render('ccp/index.html.twig', array( 'listBooks'   => "listBooks" ));

/*
        return $this->render('cpp\index.html.twig', [
            'number' => $number,
        ]);*/
    }
}