<?php

namespace App\Controller\dashboard;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Distrito;
use App\Entity\Canton;
use App\Entity\Provincia;


class TableroController extends AbstractController
{
    /**
     * @Route("/Inicio", name="tablero_index")
     */
    public function home()
    {
        return $this->render('cpp\index.html.twig');
    }
    /**
     * @Route("/Estadisticas", name="tablero_estadisticas")
     */
    public function tablero_estadisticas(Request $request)
    {
      $anio = $request->get('anio');
      $ccp  = $request->get('ccp');
      $dis  = $request->get('distro');
      $ini  = $request->get('dinit');
      $fin  = $request->get('dend');
      $std  = $request->get('state');
      $form['vanio'] = $anio;
      $form['vccp'] = $ccp;
      $form['vdistro'] = $dis;
      $form['vdinit'] = $ini;
      $form['vdend'] = $fin;

      $em = $this->getDoctrine()->getManager();
      // GENERACION DE FORMULARIOS
      $form['ccp'] = $em->getRepository('App:Centrocivico')->findBy( [], ['centrocivicodescripcion' => 'ASC'] );
      $form['provincia'] = $em->getRepository('App:Provincia')->findBy( [], ['provinciadescripcion' => 'ASC'] );
      $statement = $em->getConnection()->prepare( "SELECT p.ProvinciaDescripcion, c.CantonDescripcion, d.DistritoID, d.DistritoDescripcion FROM Distrito d JOIN Canton c ON c.CantonID = d.CantonID JOIN Provincia p ON p.ProvinciaID = c.ProvinciaID " );
      $statement->execute();
      $form['distrito'] = $statement->fetchAll();
      // GENERACION DE GRAFICOS
      $vars['distroh'] = $this->getSql( 'distroh', $anio, $ccp, $dis, $ini, $fin, $std);
      $vars['distrom'] = $this->getSql( 'distrom', $anio, $ccp, $dis, $ini, $fin, $std);
      $vars['distrot'] = $this->getSql( 'distrot', $anio, $ccp, $dis, $ini, $fin, $std);
      $vars['permanenciac'] = $this->getSql( 'permanenciac', $anio, $ccp, $dis, $ini, $fin, $std);
      $vars['permanencian'] = $this->getSql( 'permanencian', $anio, $ccp, $dis, $ini, $fin, $std);

      $vars['permanencia_edu12h'] = $this->getSql( 'permanencia_edu12h', $anio, $ccp, $dis, $ini, $fin, $std);
      $vars['permanencia_edu12m'] = $this->getSql( 'permanencia_edu12m', $anio, $ccp, $dis, $ini, $fin, $std);
      $vars['permanencia_edu17h'] = $this->getSql( 'permanencia_edu17h', $anio, $ccp, $dis, $ini, $fin, $std);
      $vars['permanencia_edu17m'] = $this->getSql( 'permanencia_edu17m', $anio, $ccp, $dis, $ini, $fin, $std);

      $vars['influencia'] = $this->getSql( 'influencia', $anio, $ccp, $dis, $ini, $fin, $std);

      $vars['geojson'] = $this->getGeoJson( 0, 0 );
      $vars['geojson2'] = $this->getGeoJson2( 0, 0 );

      return $this->render('ccp\board\index.html.twig', array( 'form'=> $form, 'vars'=> $vars ));
    }

    /**
    * @Route("/Estadisticas/tabla/json", name="tablero_estadisticas_tabla_json", methods={"GET","POST"})
    */
    public function json_params(Request $request){
      $anio = ( $request->get('anio')) ? $request->get('anio') : -1 ;
      $ccp  = ( $request->get('ccp')) ? $request->get('ccp') : -1 ;
      $dis  = ( $request->get('distro')) ? $request->get('distro') : -1 ;
      $ini  = $request->get('dinit');
      $fin  = $request->get('dend');
      $std  = $request->get('state');
      if ( $anio > -1 ) {
        $ini = "$anio-01-01";
        $fin = "$anio-12-31";
      } elseif ( strlen( $ini ) > 5 ) {
        $ini = ( strlen( $ini ) > 5 ) ? $ini : "2016-01-01" ;
        $fin = ( strlen( $fin ) > 5 ) ? $fin : date("Y-m-d") ;
      } else {
        $ini = "2016-01-01";
        $fin = date("Y-m-d");
      }
      $whr  = '';// cc.CentroCivicoID = AND ex.ExpedienteNumero IN ( SELECT r.ExpedienteNumero FROM Residencia r WHERE r.ExpedienteNumero = ex.ExpedienteNumero AND r.DistritoID = )
      $whr .= ( $ccp > -1 ) ? " AND cc.CentroCivicoID = $ccp " : '' ;
      $whr .= ( $dis > -1 ) ? "  AND ex.ExpedienteNumero IN ( SELECT r.ExpedienteNumero FROM Residencia r WHERE r.ExpedienteNumero = ex.ExpedienteNumero AND r.DistritoID = $dis ) " : '' ;
//      $whr .= ( $std > -1 ) ? " AND pf.ProcesoFormativoEstadoProceso = $std " : '' ;
      $sql  = " SELECT ccp, cur, Total,
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 27, 1, 1 ) 'EP11',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 28, 1, 1 ) 'C11',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 29, 1, 1 ) 'NC11',
                  0 'PI11',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 27, 1, 2 ) 'EP12',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 28, 1, 2 ) 'C12',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 29, 1, 2 ) 'NC12',
                  0 'PI12',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 27, 2, 1 ) 'EP21',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 28, 2, 1 ) 'C21',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 29, 2, 1 ) 'NC21',
                  0 'PI21',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 27, 2, 2 ) 'EP22',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 28, 2, 2 ) 'C22',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 29, 2, 2 ) 'NC22',
                  0 'PI22',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 27, 3, 1 ) 'EP31',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 28, 3, 1 ) 'C31',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 29, 3, 1 ) 'NC31',
                  0 'PI31',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 27, 3, 2 ) 'EP32',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 28, 3, 2 ) 'C32',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 29, 3, 2 ) 'NC32',
                  0 'PI32',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 27, 4, 1 ) 'EP41',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 28, 4, 1 ) 'C41',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 29, 4, 1 ) 'NC41',
                  0 'PI41',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 27, 4, 2 ) 'EP42',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 28, 4, 2 ) 'C42',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 29, 4, 2 ) 'NC42',
                  0 'PI42',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 27, 5, 1 ) 'EP51',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 28, 5, 1 ) 'C51',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 29, 5, 1 ) 'NC51',
                  0 'PI51',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 27, 5, 2 ) 'EP52',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 28, 5, 2 ) 'C52',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 29, 5, 2 ) 'NC52',
                  0 'PI52',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 27, 6, 1 ) 'EP61',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 28, 6, 1 ) 'C61',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 29, 6, 1 ) 'NC61',
                  0 'PI61',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 27, 6, 2 ) 'EP62',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 28, 6, 2 ) 'C62',
                  dbo.fnc_get_participante( ccp, cur, 0, '$ini', '$fin', 29, 6, 2 ) 'NC62',
                  0 'PI62'
                FROM
                  (SELECT cc.CentroCivicoDescripcion 'ccp', pf.ProcesoFormativoNombre 'cur', COUNT(1) 'Total'
                    FROM ProcesoFormativo pf INNER JOIN Expediente ex ON ex.ExpedienteNumero = pf.ExpedienteNumero
                      INNER JOIN CentroCivico cc ON SUBSTRING( ex.ExpedienteNumero , 1, 3 ) = cc.CentroCivicoSiglas
                    WHERE CentroCivicoID < 8 $whr GROUP BY cc.CentroCivicoDescripcion, pf.ProcesoFormativoNombre
                  ) t ORDER BY ccp, cur ";
      $em = $this->getDoctrine()->getManager();
      $statement = $em->getConnection()->prepare($sql);
      $statement->execute();
      $result = $statement->fetchAll();

      return new JsonResponse([ 'data' => $result ], Response::HTTP_OK, ['content-type'=>'application/json']);
    }

 private function getGeoJson( $provincia = 0, $canton = 0 ){
   $em = $this->getDoctrine()->getManager();
   $sql  = " SELECT e.CentroCivicoID AS id, p.ProvinciaDescripcion AS nombre, p.ProvinciaGeoJson AS geoJson FROM CentroCivico e INNER JOIN Provincia p ON p.ProvinciaID = e.ProvinciaID WHERE e.ProvinciaID > 0 ";
   $statement = $em->getConnection()->prepare($sql);
   $statement->execute();
   return $statement->fetchAll();
 }

 private function getGeoJson2( $provincia = 0, $canton = 0 ){
    $em = $this->getDoctrine()->getManager();
    $sql  = " SELECT e.CentroCivicoID AS id, e.CentroCivicoDescripcion AS nombre, c.CantonGeoJson AS geoJson FROM CentroCivico e INNER JOIN Provincia p ON p.ProvinciaID = e.ProvinciaID INNER JOIN Canton c ON c.ProvinciaID = p.ProvinciaID WHERE e.ProvinciaID > 0 AND CONVERT(VARCHAR, c.CantonGeoJson) != '{}'";
    $statement = $em->getConnection()->prepare($sql);
    $statement->execute();
    return $statement->fetchAll();
  }

 private function getSql($q = -1, $anio = -1, $ccp = 0, $dis = -1, $ini = '', $fin = '', $std = -1){
   $em = $this->getDoctrine()->getManager();
   $sql = '';
   $whr = '';
   if ( $q == '' ) {
     return '';
   } elseif ( $q == 'distroh' ) {
     if ( $anio > -1 ) {
       $whr .= " AND pf.ProcesoFormativoFecInscripcion >= '$anio-01-01' AND pf.ProcesoFormativoFecInscripcion <= '$anio-12-31' ";
     } elseif ( strlen( $ini ) > 5 ) {
       $whr .= " AND pf.ProcesoFormativoFecInscripcion >= '$ini' ";
       $whr .= ( strlen( $fin ) > 5 ) ? " AND pf.ProcesoFormativoFecInscripcion <= '$fin' " : '' ;
     }
     $whr .= ( $ccp > -1 ) ? " AND cc.CentroCivicoID = $ccp " : '' ;
     $whr .= ( $dis > -1 ) ? "  AND ex.ExpedienteNumero IN ( SELECT r.ExpedienteNumero FROM Residencia r WHERE r.ExpedienteNumero = ex.ExpedienteNumero AND r.DistritoID = $dis ) " : '' ;
     $whr .= ( $std > -1 ) ? " AND pf.ProcesoFormativoEstadoProceso = $std " : '' ;
     $sql = "SELECT cc.CentroCivicoDescripcion AS cat, COUNT(1) AS val
            FROM ProcesoFormativo pf INNER JOIN Expediente ex ON ex.ExpedienteNumero = pf.ExpedienteNumero
              INNER JOIN CentroCivico cc ON SUBSTRING( ex.ExpedienteNumero , 1, 3 ) = cc.CentroCivicoSiglas
            WHERE CentroCivicoID < 8 AND ex.SexoID = 1 $whr
            GROUP BY cc.CentroCivicoDescripcion ORDER BY cc.CentroCivicoDescripcion";
   } elseif ( $q == 'distrom' ) {
     if ( $anio > -1 ) {
       $whr .= " AND pf.ProcesoFormativoFecInscripcion >= '$anio-01-01' AND pf.ProcesoFormativoFecInscripcion <= '$anio-12-31' ";
     } elseif ( strlen( $ini ) > 5 ) {
       $whr .= " AND pf.ProcesoFormativoFecInscripcion >= '$ini' ";
       $whr .= ( strlen( $fin ) > 5 ) ? " AND pf.ProcesoFormativoFecInscripcion <= '$fin' " : '' ;
     }
     $whr .= ( $ccp > -1 ) ? " AND cc.CentroCivicoID = $ccp " : '' ;
     $whr .= ( $dis > -1 ) ? "  AND ex.ExpedienteNumero IN ( SELECT r.ExpedienteNumero FROM Residencia r WHERE r.ExpedienteNumero = ex.ExpedienteNumero AND r.DistritoID = $dis ) " : '' ;
     $whr .= ( $std > -1 ) ? " AND pf.ProcesoFormativoEstadoProceso = $std " : '' ;
     $sql = "SELECT cc.CentroCivicoDescripcion AS cat, COUNT(1) AS val
            FROM ProcesoFormativo pf INNER JOIN Expediente ex ON ex.ExpedienteNumero = pf.ExpedienteNumero
              INNER JOIN CentroCivico cc ON SUBSTRING( ex.ExpedienteNumero , 1, 3 ) = cc.CentroCivicoSiglas
            WHERE CentroCivicoID < 8 AND ex.SexoID = 2 $whr
            GROUP BY cc.CentroCivicoDescripcion ORDER BY cc.CentroCivicoDescripcion";
   } elseif ( $q == 'distrot' ) {
     if ( $anio > -1 ) {
       $whr .= " AND pf.ProcesoFormativoFecInscripcion >= '$anio-01-01' AND pf.ProcesoFormativoFecInscripcion <= '$anio-12-31' ";
     } elseif ( strlen( $ini ) > 5 ) {
       $whr .= " AND pf.ProcesoFormativoFecInscripcion >= '$ini' ";
       $whr .= ( strlen( $fin ) > 5 ) ? " AND pf.ProcesoFormativoFecInscripcion <= '$fin' " : '' ;
     }
     $whr .= ( $ccp > -1 ) ? " AND cc.CentroCivicoID = $ccp " : '' ;
     $whr .= ( $dis > -1 ) ? "  AND ex.ExpedienteNumero IN ( SELECT r.ExpedienteNumero FROM Residencia r WHERE r.ExpedienteNumero = ex.ExpedienteNumero AND r.DistritoID = $dis ) " : '' ;
     $whr .= ( $std > -1 ) ? " AND pf.ProcesoFormativoEstadoProceso = $std " : '' ;
     $sql = "SELECT cc.CentroCivicoDescripcion AS cat, COUNT(1) AS val
            FROM ProcesoFormativo pf INNER JOIN Expediente ex ON ex.ExpedienteNumero = pf.ExpedienteNumero
              INNER JOIN CentroCivico cc ON SUBSTRING( ex.ExpedienteNumero , 1, 3 ) = cc.CentroCivicoSiglas
            WHERE CentroCivicoID < 8 $whr
            GROUP BY cc.CentroCivicoDescripcion ORDER BY cc.CentroCivicoDescripcion";
   } elseif ( $q == 'permanenciac' ) {
     if ( $anio > -1 ) {
       $whr .= " AND pf.ProcesoFormativoFecInscripcion >= '$anio-01-01' AND pf.ProcesoFormativoFecInscripcion <= '$anio-12-31' ";
     } elseif ( strlen( $ini ) > 5 ) {
       $whr .= " AND pf.ProcesoFormativoFecInscripcion >= '$ini' ";
       $whr .= ( strlen( $fin ) > 5 ) ? " AND pf.ProcesoFormativoFecInscripcion <= '$fin' " : '' ;
     }
     $whr .= ( $ccp > -1 ) ? " AND cc.CentroCivicoID = $ccp " : '' ;
     $whr .= ( $dis > -1 ) ? "  AND ex.ExpedienteNumero IN ( SELECT r.ExpedienteNumero FROM Residencia r WHERE r.ExpedienteNumero = ex.ExpedienteNumero AND r.DistritoID = $dis ) " : '' ;
     $whr .= ( $std > -1 ) ? " AND pf.ProcesoFormativoEstadoProceso = $std " : '' ;
     $sql = "SELECT s.SexoDescripcion AS cat, COUNT(1) AS val
            FROM ProcesoFormativo pf INNER JOIN Expediente ex ON ex.ExpedienteNumero = pf.ExpedienteNumero
              INNER JOIN CentroCivico cc ON SUBSTRING( ex.ExpedienteNumero , 1, 3 ) = cc.CentroCivicoSiglas
              INNER JOIN Sexo s ON s.SexoID = ex.SexoID
            WHERE pf.ProcesoFormativoEstadoProceso = 28 $whr
            GROUP BY s.SexoDescripcion ORDER BY s.SexoDescripcion ";
   } elseif ( $q == 'permanencian' ) {
     if ( $anio > -1 ) {
       $whr .= " AND pf.ProcesoFormativoFecInscripcion >= '$anio-01-01' AND pf.ProcesoFormativoFecInscripcion <= '$anio-12-31' ";
     } elseif ( strlen( $ini ) > 5 ) {
       $whr .= " AND pf.ProcesoFormativoFecInscripcion >= '$ini' ";
       $whr .= ( strlen( $fin ) > 5 ) ? " AND pf.ProcesoFormativoFecInscripcion <= '$fin' " : '' ;
     }
     $whr .= ( $ccp > -1 ) ? " AND cc.CentroCivicoID = $ccp " : '' ;
     $whr .= ( $dis > -1 ) ? "  AND ex.ExpedienteNumero IN ( SELECT r.ExpedienteNumero FROM Residencia r WHERE r.ExpedienteNumero = ex.ExpedienteNumero AND r.DistritoID = $dis ) " : '' ;
     $whr .= ( $std > -1 ) ? " AND pf.ProcesoFormativoEstadoProceso = $std " : '' ;
     $sql = "SELECT s.SexoDescripcion AS cat, COUNT(1) AS val
            FROM ProcesoFormativo pf INNER JOIN Expediente ex ON ex.ExpedienteNumero = pf.ExpedienteNumero
              INNER JOIN CentroCivico cc ON SUBSTRING( ex.ExpedienteNumero , 1, 3 ) = cc.CentroCivicoSiglas
              INNER JOIN Sexo s ON s.SexoID = ex.SexoID
            WHERE pf.ProcesoFormativoEstadoProceso != 28 $whr
            GROUP BY s.SexoDescripcion ORDER BY s.SexoDescripcion ";
   } elseif ( $q == 'permanencia_edu12h' ) {
     if ( $anio > -1 ) {
       $whr .= " AND pf.ProcesoFormativoFecInscripcion >= '$anio-01-01' AND pf.ProcesoFormativoFecInscripcion <= '$anio-12-31' ";
     } elseif ( strlen( $ini ) > 5 ) {
       $whr .= " AND pf.ProcesoFormativoFecInscripcion >= '$ini' ";
       $whr .= ( strlen( $fin ) > 5 ) ? " AND pf.ProcesoFormativoFecInscripcion <= '$fin' " : '' ;
     }
     $whr .= ( $ccp > -1 ) ? " AND cc.CentroCivicoID = $ccp " : '' ;
     $whr .= ( $dis > -1 ) ? "  AND ex.ExpedienteNumero IN ( SELECT r.ExpedienteNumero FROM Residencia r WHERE r.ExpedienteNumero = ex.ExpedienteNumero AND r.DistritoID = $dis ) " : '' ;
     $whr .= ( $std > -1 ) ? " AND pf.ProcesoFormativoEstadoProceso = $std " : '' ;
     $sql = "SELECT re.RespuestaDescripcion AS cat, COUNT(1) AS val
            FROM ProcesoFormativo pf INNER JOIN Expediente ex ON ex.ExpedienteNumero = pf.ExpedienteNumero
              INNER JOIN CentroCivico cc ON SUBSTRING( ex.ExpedienteNumero , 1, 3 ) = cc.CentroCivicoSiglas
              INNER JOIN ExpEducacion ee ON ex.ExpedienteNumero = ee.ExpedienteNumero
              INNER JOIN Respuesta re ON re.RespuestaID = ee.ExpEducacionNivelEduc
            WHERE ex.SexoID = 1 AND DATEDIFF(YEAR, ex.ExpedienteFecNac, GETDATE()) >= 7 AND DATEDIFF(YEAR, ex.ExpedienteFecNac, GETDATE()) <= 12 $whr
            GROUP BY re.RespuestaDescripcion ORDER BY re.RespuestaDescripcion ";
   } elseif ( $q == 'permanencia_edu12m' ) {
     if ( $anio > -1 ) {
       $whr .= " AND pf.ProcesoFormativoFecInscripcion >= '$anio-01-01' AND pf.ProcesoFormativoFecInscripcion <= '$anio-12-31' ";
     } elseif ( strlen( $ini ) > 5 ) {
       $whr .= " AND pf.ProcesoFormativoFecInscripcion >= '$ini' ";
       $whr .= ( strlen( $fin ) > 5 ) ? " AND pf.ProcesoFormativoFecInscripcion <= '$fin' " : '' ;
     }
     $whr .= ( $ccp > -1 ) ? " AND cc.CentroCivicoID = $ccp " : '' ;
     $whr .= ( $dis > -1 ) ? "  AND ex.ExpedienteNumero IN ( SELECT r.ExpedienteNumero FROM Residencia r WHERE r.ExpedienteNumero = ex.ExpedienteNumero AND r.DistritoID = $dis ) " : '' ;
     $whr .= ( $std > -1 ) ? " AND pf.ProcesoFormativoEstadoProceso = $std " : '' ;
     $sql = "SELECT re.RespuestaDescripcion AS cat, COUNT(1) AS val
            FROM ProcesoFormativo pf INNER JOIN Expediente ex ON ex.ExpedienteNumero = pf.ExpedienteNumero
              INNER JOIN CentroCivico cc ON SUBSTRING( ex.ExpedienteNumero , 1, 3 ) = cc.CentroCivicoSiglas
              INNER JOIN ExpEducacion ee ON ex.ExpedienteNumero = ee.ExpedienteNumero
              INNER JOIN Respuesta re ON re.RespuestaID = ee.ExpEducacionNivelEduc
            WHERE ex.SexoID = 2 AND DATEDIFF(YEAR, ex.ExpedienteFecNac, GETDATE()) >= 7 AND DATEDIFF(YEAR, ex.ExpedienteFecNac, GETDATE()) <= 12 $whr
            GROUP BY re.RespuestaDescripcion ORDER BY re.RespuestaDescripcion ";
   } elseif ( $q == 'permanencia_edu17h' ) {
     if ( $anio > -1 ) {
       $whr .= " AND pf.ProcesoFormativoFecInscripcion >= '$anio-01-01' AND pf.ProcesoFormativoFecInscripcion <= '$anio-12-31' ";
     } elseif ( strlen( $ini ) > 5 ) {
       $whr .= " AND pf.ProcesoFormativoFecInscripcion >= '$ini' ";
       $whr .= ( strlen( $fin ) > 5 ) ? " AND pf.ProcesoFormativoFecInscripcion <= '$fin' " : '' ;
     }
     $whr .= ( $ccp > -1 ) ? " AND cc.CentroCivicoID = $ccp " : '' ;
     $whr .= ( $dis > -1 ) ? "  AND ex.ExpedienteNumero IN ( SELECT r.ExpedienteNumero FROM Residencia r WHERE r.ExpedienteNumero = ex.ExpedienteNumero AND r.DistritoID = $dis ) " : '' ;
     $whr .= ( $std > -1 ) ? " AND pf.ProcesoFormativoEstadoProceso = $std " : '' ;
     $sql = "SELECT re.RespuestaDescripcion AS cat, COUNT(1) AS val
            FROM ProcesoFormativo pf INNER JOIN Expediente ex ON ex.ExpedienteNumero = pf.ExpedienteNumero
              INNER JOIN CentroCivico cc ON SUBSTRING( ex.ExpedienteNumero , 1, 3 ) = cc.CentroCivicoSiglas
              INNER JOIN ExpEducacion ee ON ex.ExpedienteNumero = ee.ExpedienteNumero
              INNER JOIN Respuesta re ON re.RespuestaID = ee.ExpEducacionNivelEduc
            WHERE ex.SexoID = 1 AND DATEDIFF(YEAR, ex.ExpedienteFecNac, GETDATE()) >= 12 AND DATEDIFF(YEAR, ex.ExpedienteFecNac, GETDATE()) <= 17 $whr
            GROUP BY re.RespuestaDescripcion ORDER BY re.RespuestaDescripcion ";
   } elseif ( $q == 'permanencia_edu17m' ) {
     if ( $anio > -1 ) {
       $whr .= " AND pf.ProcesoFormativoFecInscripcion >= '$anio-01-01' AND pf.ProcesoFormativoFecInscripcion <= '$anio-12-31' ";
     } elseif ( strlen( $ini ) > 5 ) {
       $whr .= " AND pf.ProcesoFormativoFecInscripcion >= '$ini' ";
       $whr .= ( strlen( $fin ) > 5 ) ? " AND pf.ProcesoFormativoFecInscripcion <= '$fin' " : '' ;
     }
     $whr .= ( $ccp > -1 ) ? " AND cc.CentroCivicoID = $ccp " : '' ;
     $whr .= ( $dis > -1 ) ? "  AND ex.ExpedienteNumero IN ( SELECT r.ExpedienteNumero FROM Residencia r WHERE r.ExpedienteNumero = ex.ExpedienteNumero AND r.DistritoID = $dis ) " : '' ;
     $whr .= ( $std > -1 ) ? " AND pf.ProcesoFormativoEstadoProceso = $std " : '' ;
     $sql = "SELECT re.RespuestaDescripcion AS cat, COUNT(1) AS val
            FROM ProcesoFormativo pf INNER JOIN Expediente ex ON ex.ExpedienteNumero = pf.ExpedienteNumero
              INNER JOIN CentroCivico cc ON SUBSTRING( ex.ExpedienteNumero , 1, 3 ) = cc.CentroCivicoSiglas
              INNER JOIN ExpEducacion ee ON ex.ExpedienteNumero = ee.ExpedienteNumero
              INNER JOIN Respuesta re ON re.RespuestaID = ee.ExpEducacionNivelEduc
            WHERE ex.SexoID = 2 AND DATEDIFF(YEAR, ex.ExpedienteFecNac, GETDATE()) >= 13 AND DATEDIFF(YEAR, ex.ExpedienteFecNac, GETDATE()) <= 17 $whr
            GROUP BY re.RespuestaDescripcion ORDER BY re.RespuestaDescripcion ";
   } elseif ( $q == 'influencia' ) {
     if ( $anio > -1 ) {
       $whr .= " AND cp.CantonPoblacionAnio>= $anio AND dp.DistritoPoblacionAnio = $anio";
     }
     $whr .= ( $dis > -1 ) ? " AND d.DistritoID = $dis " : '' ;
     $whr .= ( $ccp > -1 ) ? " AND c.CantonID IN ( SELECT cc.CantonID FROM CentroCivico cc WHERE cc.CentroCivicoID = $ccp ) " : '' ;
     $sql = "SELECT c.CantonDescripcion, cp.CantonPoblacionTotal, d.DistritoDescripcion, dp.DistritoPoblacionTotal , CAST( (100*dp.DistritoPoblacionTotal/cp.CantonPoblacionTotal) AS DECIMAL(5,2)) 'porcent'
            FROM Canton c INNER JOIN CantonPoblacion cp ON cp.CantonID = c.CantonID
              INNER JOIN Distrito d ON d.CantonID = c.CantonID INNER JOIN DistritoPoblacion dp ON dp.DistritoID = d.DistritoID
            WHERE c.CantonID != 0 $whr
            ORDER BY c.CantonDescripcion, d.DistritoDescripcion ";
   }

   if ( strlen( $sql ) > 5 ) {
     $statement = $em->getConnection()->prepare( $sql );
     $statement->execute();
     return $statement->fetchAll();
   }

   return array('error' => '' );
 }
}
