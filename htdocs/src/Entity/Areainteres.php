<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Areainteres
 *
 * @ORM\Table(name="AreaInteres", indexes={@ORM\Index(name="IAREAINTERES1", columns={"TipoInteresID"})})
 * @ORM\Entity
 */
class Areainteres
{
    /**
     * @var int
     *
     * @ORM\Column(name="AreaInteresID", type="integer", nullable=false, options={"comment"="Llave primaria de la Tabla"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $areainteresid;

    /**
     * @var string
     *
     * @ORM\Column(name="AreaInteresDescripcion", type="string", length=300, nullable=false, options={"comment"="Registra la descripción del Área de Interés."})
     */
    private $areainteresdescripcion;

    /**
     * @var bool
     *
     * @ORM\Column(name="AreaInteresActivo", type="boolean", nullable=false, options={"comment"="Indica el estado del Área de Interés 1 Activo y 0 Inactivo."})
     */
    private $areainteresactivo;

    /**
     * @var \Tipointeres
     *
     * @ORM\ManyToOne(targetEntity="Tipointeres")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="TipoInteresID", referencedColumnName="TipoInteresID")
     * })
     */
    private $tipointeresid;

    public function getAreainteresid(): ?int
    {
        return $this->areainteresid;
    }

    public function getAreainteresdescripcion(): ?string
    {
        return $this->areainteresdescripcion;
    }

    public function setAreainteresdescripcion(string $areainteresdescripcion): self
    {
        $this->areainteresdescripcion = $areainteresdescripcion;

        return $this;
    }

    public function getAreainteresactivo(): ?bool
    {
        return $this->areainteresactivo;
    }

    public function setAreainteresactivo(bool $areainteresactivo): self
    {
        $this->areainteresactivo = $areainteresactivo;

        return $this;
    }

    public function getTipointeresid(): ?Tipointeres
    {
        return $this->tipointeresid;
    }

    public function setTipointeresid(?Tipointeres $tipointeresid): self
    {
        $this->tipointeresid = $tipointeresid;

        return $this;
    }


}
