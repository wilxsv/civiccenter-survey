<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Areatrabajo
 *
 * @ORM\Table(name="AreaTrabajo", indexes={@ORM\Index(name="IDX_2B4F1840DC4988E9", columns={"AreaTrabajo_ID"})})
 * @ORM\Entity
 */
class Areatrabajo
{
    /**
     * @var int
     *
     * @ORM\Column(name="AreaTrabajoID", type="integer", nullable=false, options={"comment"="Llave Primaria de la Tabla"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $areatrabajoid;

    /**
     * @var string
     *
     * @ORM\Column(name="AreaTrabajoDescripcion", type="string", length=100, nullable=false, options={"comment"="Se guarda la descripción de las distintas Áreas de Trabajo."})
     */
    private $areatrabajodescripcion;

    /**
     * @var \Areatrabajo
     *
     * @ORM\ManyToOne(targetEntity="Areatrabajo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="AreaTrabajo_ID", referencedColumnName="AreaTrabajoID")
     * })
     */
    private $areatrabajo;

    public function getAreatrabajoid(): ?int
    {
        return $this->areatrabajoid;
    }

    public function getAreatrabajodescripcion(): ?string
    {
        return $this->areatrabajodescripcion;
    }

    public function setAreatrabajodescripcion(string $areatrabajodescripcion): self
    {
        $this->areatrabajodescripcion = $areatrabajodescripcion;

        return $this;
    }

    public function getAreatrabajo(): ?self
    {
        return $this->areatrabajo;
    }

    public function setAreatrabajo(?self $areatrabajo): self
    {
        $this->areatrabajo = $areatrabajo;

        return $this;
    }


}
