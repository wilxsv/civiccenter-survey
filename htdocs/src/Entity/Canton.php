<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Canton
 *
 * @ORM\Table(name="Canton", indexes={@ORM\Index(name="ICANTON1", columns={"ProvinciaID"})})
 * @ORM\Entity
 */
class Canton
{
    /**
     * @var int
     *
     * @ORM\Column(name="CantonID", type="integer", nullable=false, options={"comment"="Llave Primaria de la Tabla"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cantonid;

    /**
     * @var string
     *
     * @ORM\Column(name="CantonDescripcion", type="string", length=300, nullable=false, options={"comment"="Almacena la Descripción de cada uno de los Cantones del País."})
     */
    private $cantondescripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="CantonGeoJson", type="text", length=16, nullable=false, options={"default"="{}"})
     */
    private $cantongeojson = '{}';

    /**
     * @var \Provincia
     *
     * @ORM\ManyToOne(targetEntity="Provincia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ProvinciaID", referencedColumnName="ProvinciaID")
     * })
     */
    private $provinciaid;

    public function getCantonid(): ?int
    {
        return $this->cantonid;
    }

    public function getCantondescripcion(): ?string
    {
        return $this->cantondescripcion;
    }

    public function setCantondescripcion(string $cantondescripcion): self
    {
        $this->cantondescripcion = $cantondescripcion;

        return $this;
    }

    public function getCantongeojson(): ?string
    {
        return $this->cantongeojson;
    }

    public function setCantongeojson(string $cantongeojson): self
    {
        $this->cantongeojson = $cantongeojson;

        return $this;
    }

    public function getProvinciaid(): ?Provincia
    {
        return $this->provinciaid;
    }

    public function setProvinciaid(?Provincia $provinciaid): self
    {
        $this->provinciaid = $provinciaid;

        return $this;
    }


}
