<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cantonpoblacion
 *
 * @ORM\Table(name="CantonPoblacion", indexes={@ORM\Index(name="IDX_58F2EAE0A05B05A9", columns={"CantonID"})})
 * @ORM\Entity
 */
class Cantonpoblacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="CantonPoblacionAnio", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $cantonpoblacionanio;

    /**
     * @var int
     *
     * @ORM\Column(name="CantonPoblacionEdad", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $cantonpoblacionedad;

    /**
     * @var float
     *
     * @ORM\Column(name="CantonPoblacionTotal", type="float", precision=24, scale=0, nullable=false)
     */
    private $cantonpoblaciontotal;

    /**
     * @var float
     *
     * @ORM\Column(name="CantonPoblacionHombre", type="float", precision=24, scale=0, nullable=false)
     */
    private $cantonpoblacionhombre;

    /**
     * @var float
     *
     * @ORM\Column(name="CantonPoblacionMujer", type="float", precision=24, scale=0, nullable=false)
     */
    private $cantonpoblacionmujer;

    /**
     * @var \Canton
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Canton")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CantonID", referencedColumnName="CantonID")
     * })
     */
    private $cantonid;

    public function getCantonpoblacionanio(): ?int
    {
        return $this->cantonpoblacionanio;
    }

    public function getCantonpoblacionedad(): ?int
    {
        return $this->cantonpoblacionedad;
    }

    public function getCantonpoblaciontotal(): ?float
    {
        return $this->cantonpoblaciontotal;
    }

    public function setCantonpoblaciontotal(float $cantonpoblaciontotal): self
    {
        $this->cantonpoblaciontotal = $cantonpoblaciontotal;

        return $this;
    }

    public function getCantonpoblacionhombre(): ?float
    {
        return $this->cantonpoblacionhombre;
    }

    public function setCantonpoblacionhombre(float $cantonpoblacionhombre): self
    {
        $this->cantonpoblacionhombre = $cantonpoblacionhombre;

        return $this;
    }

    public function getCantonpoblacionmujer(): ?float
    {
        return $this->cantonpoblacionmujer;
    }

    public function setCantonpoblacionmujer(float $cantonpoblacionmujer): self
    {
        $this->cantonpoblacionmujer = $cantonpoblacionmujer;

        return $this;
    }

    public function getCantonid(): ?Canton
    {
        return $this->cantonid;
    }

    public function setCantonid(?Canton $cantonid): self
    {
        $this->cantonid = $cantonid;

        return $this;
    }


}
