<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Catrol
 *
 * @ORM\Table(name="CatRol")
 * @ORM\Entity
 */
class Catrol
{
    /**
     * @var int
     *
     * @ORM\Column(name="CatRolID", type="integer", nullable=false, options={"comment"="Registra un dato consecutivo para cada uno de los distintos roles del sistema y se utilza como llave primaria"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $catrolid;

    /**
     * @var string
     *
     * @ORM\Column(name="CatRolDescripcion", type="string", length=100, nullable=false, options={"comment"="Almacena la Descripción del Rol que se otorgará a cada usuario"})
     */
    private $catroldescripcion;

    public function getCatrolid(): ?int
    {
        return $this->catrolid;
    }

    public function getCatroldescripcion(): ?string
    {
        return $this->catroldescripcion;
    }

    public function setCatroldescripcion(string $catroldescripcion): self
    {
        $this->catroldescripcion = $catroldescripcion;

        return $this;
    }


}
