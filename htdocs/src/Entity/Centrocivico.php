<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Centrocivico
 *
 * @ORM\Table(name="CentroCivico", indexes={@ORM\Index(name="IDX_6B20B352A513EDD5", columns={"ProvinciaID"})})
 * @ORM\Entity
 */
class Centrocivico
{
    /**
     * @var int
     *
     * @ORM\Column(name="CentroCivicoID", type="integer", nullable=false, options={"comment"="Registra un consecutivo de cada uno de los Centros Cívicos y se utiliza como la llave principal para los registros."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $centrocivicoid;

    /**
     * @var string
     *
     * @ORM\Column(name="CentroCivicoSiglas", type="string", length=3, nullable=false, options={"comment"="Guarda cada una de las siglas de cada Centro Cívico, utilizadas para generar los expedientes y otros datos."})
     */
    private $centrocivicosiglas;

    /**
     * @var string
     *
     * @ORM\Column(name="CentroCivicoDescripcion", type="string", length=100, nullable=false, options={"comment"="Mantiene la descripción de cada uno de los Centros Cívicos."})
     */
    private $centrocivicodescripcion;

    /**
     * @var int
     *
     * @ORM\Column(name="CentroCivicoConsec", type="integer", nullable=false, options={"comment"="Almacena el número consecutivo que se utiliza para generar cada uno de los expedientes por Centro Cívico."})
     */
    private $centrocivicoconsec;

    /**
     * @var int
     *
     * @ORM\Column(name="CentroCivicoConsecCurso", type="integer", nullable=false, options={"comment"="Preserva el número consecutivo para generar cada uno de los cursos de los Procesos Formativos para cada Centro Cívico."})
     */
    private $centrocivicoconseccurso;

    /**
     * @var \Provincia
     *
     * @ORM\ManyToOne(targetEntity="Provincia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ProvinciaID", referencedColumnName="ProvinciaID")
     * })
     */
    private $provinciaid;

    public function getCentrocivicoid(): ?int
    {
        return $this->centrocivicoid;
    }

    public function getCentrocivicosiglas(): ?string
    {
        return $this->centrocivicosiglas;
    }

    public function setCentrocivicosiglas(string $centrocivicosiglas): self
    {
        $this->centrocivicosiglas = $centrocivicosiglas;

        return $this;
    }

    public function getCentrocivicodescripcion(): ?string
    {
        return $this->centrocivicodescripcion;
    }

    public function setCentrocivicodescripcion(string $centrocivicodescripcion): self
    {
        $this->centrocivicodescripcion = $centrocivicodescripcion;

        return $this;
    }

    public function getCentrocivicoconsec(): ?int
    {
        return $this->centrocivicoconsec;
    }

    public function setCentrocivicoconsec(int $centrocivicoconsec): self
    {
        $this->centrocivicoconsec = $centrocivicoconsec;

        return $this;
    }

    public function getCentrocivicoconseccurso(): ?int
    {
        return $this->centrocivicoconseccurso;
    }

    public function setCentrocivicoconseccurso(int $centrocivicoconseccurso): self
    {
        $this->centrocivicoconseccurso = $centrocivicoconseccurso;

        return $this;
    }

    public function getProvinciaid(): ?Provincia
    {
        return $this->provinciaid;
    }

    public function setProvinciaid(?Provincia $provinciaid): self
    {
        $this->provinciaid = $provinciaid;

        return $this;
    }


}
