<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Centrocivicorep
 *
 * @ORM\Table(name="CentroCivicoRep")
 * @ORM\Entity
 */
class Centrocivicorep
{
    /**
     * @var int
     *
     * @ORM\Column(name="CentroCivicoRepID", type="integer", nullable=false, options={"comment"="Almacena un número consecutivo de los Centros Cívicos y se utiliza como llave principal."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $centrocivicorepid;

    /**
     * @var string
     *
     * @ORM\Column(name="CentroCivicoRepSiglas", type="string", length=3, nullable=false, options={"comment"="Registra las Siglas del Centro Cívico."})
     */
    private $centrocivicorepsiglas;

    /**
     * @var string
     *
     * @ORM\Column(name="CentroCivicoRepDescripcion", type="string", length=200, nullable=false, options={"comment"="Mantiene la descripción de cada uno de los Centros Cívicos."})
     */
    private $centrocivicorepdescripcion;

    public function getCentrocivicorepid(): ?int
    {
        return $this->centrocivicorepid;
    }

    public function getCentrocivicorepsiglas(): ?string
    {
        return $this->centrocivicorepsiglas;
    }

    public function setCentrocivicorepsiglas(string $centrocivicorepsiglas): self
    {
        $this->centrocivicorepsiglas = $centrocivicorepsiglas;

        return $this;
    }

    public function getCentrocivicorepdescripcion(): ?string
    {
        return $this->centrocivicorepdescripcion;
    }

    public function setCentrocivicorepdescripcion(string $centrocivicorepdescripcion): self
    {
        $this->centrocivicorepdescripcion = $centrocivicorepdescripcion;

        return $this;
    }


}
