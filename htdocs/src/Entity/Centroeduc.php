<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Centroeduc
 *
 * @ORM\Table(name="CentroEduc", indexes={@ORM\Index(name="ICENTROEDUC1", columns={"DistritoID"}), @ORM\Index(name="ICENTROEDUC2", columns={"NivelMEPId"}), @ORM\Index(name="ICENTROEDUC3", columns={"SectorMEPId"})})
 * @ORM\Entity
 */
class Centroeduc
{
    /**
     * @var int
     *
     * @ORM\Column(name="CentroEducId", type="integer", nullable=false, options={"comment"="Genera un número consecutivo para cada uno de los Centros Educativos y se utiliza como llave principal."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $centroeducid;

    /**
     * @var string
     *
     * @ORM\Column(name="CentroEducNombre", type="string", length=150, nullable=false, options={"comment"="Contiene el Nombre de cada uno de los Centros Educativos del País."})
     */
    private $centroeducnombre;

    /**
     * @var string
     *
     * @ORM\Column(name="CentroEducDirecReg", type="string", length=100, nullable=false, options={"comment"="Almacena la Dirección Regional a la cual pertenece el Centro Educativo."})
     */
    private $centroeducdirecreg;

    /**
     * @var string
     *
     * @ORM\Column(name="CentroEducCircuito", type="string", length=3, nullable=false, options={"comment"="Registrar el Circuito del Centro Educativo."})
     */
    private $centroeduccircuito;

    /**
     * @var string
     *
     * @ORM\Column(name="CentroEducPoblado", type="string", length=100, nullable=false, options={"comment"="Mantiene la información del barrio o poblado donde se encuentra ubicado el Centro Educativo."})
     */
    private $centroeducpoblado;

    /**
     * @var \Distrito
     *
     * @ORM\ManyToOne(targetEntity="Distrito")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="DistritoID", referencedColumnName="DistritoID")
     * })
     */
    private $distritoid;

    /**
     * @var \Nivelmep
     *
     * @ORM\ManyToOne(targetEntity="Nivelmep")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="NivelMEPId", referencedColumnName="NivelMEPId")
     * })
     */
    private $nivelmepid;

    /**
     * @var \Sectormep
     *
     * @ORM\ManyToOne(targetEntity="Sectormep")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SectorMEPId", referencedColumnName="SectorMEPId")
     * })
     */
    private $sectormepid;

    public function getCentroeducid(): ?int
    {
        return $this->centroeducid;
    }

    public function getCentroeducnombre(): ?string
    {
        return $this->centroeducnombre;
    }

    public function setCentroeducnombre(string $centroeducnombre): self
    {
        $this->centroeducnombre = $centroeducnombre;

        return $this;
    }

    public function getCentroeducdirecreg(): ?string
    {
        return $this->centroeducdirecreg;
    }

    public function setCentroeducdirecreg(string $centroeducdirecreg): self
    {
        $this->centroeducdirecreg = $centroeducdirecreg;

        return $this;
    }

    public function getCentroeduccircuito(): ?string
    {
        return $this->centroeduccircuito;
    }

    public function setCentroeduccircuito(string $centroeduccircuito): self
    {
        $this->centroeduccircuito = $centroeduccircuito;

        return $this;
    }

    public function getCentroeducpoblado(): ?string
    {
        return $this->centroeducpoblado;
    }

    public function setCentroeducpoblado(string $centroeducpoblado): self
    {
        $this->centroeducpoblado = $centroeducpoblado;

        return $this;
    }

    public function getDistritoid(): ?Distrito
    {
        return $this->distritoid;
    }

    public function setDistritoid(?Distrito $distritoid): self
    {
        $this->distritoid = $distritoid;

        return $this;
    }

    public function getNivelmepid(): ?Nivelmep
    {
        return $this->nivelmepid;
    }

    public function setNivelmepid(?Nivelmep $nivelmepid): self
    {
        $this->nivelmepid = $nivelmepid;

        return $this;
    }

    public function getSectormepid(): ?Sectormep
    {
        return $this->sectormepid;
    }

    public function setSectormepid(?Sectormep $sectormepid): self
    {
        $this->sectormepid = $sectormepid;

        return $this;
    }


}
