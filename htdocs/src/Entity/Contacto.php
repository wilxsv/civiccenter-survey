<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contacto
 *
 * @ORM\Table(name="Contacto", indexes={@ORM\Index(name="ICONTACTO1", columns={"ExpedienteNumero"})})
 * @ORM\Entity
 */
class Contacto
{
    /**
     * @var int
     *
     * @ORM\Column(name="ContactoID", type="integer", nullable=false, options={"comment"="Almacena el número de registro para cada contacto ingresado y junto con el Número de Expediente forman una llave compuesta.
 "})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $contactoid;

    /**
     * @var int
     *
     * @ORM\Column(name="ContactoTelefono", type="integer", nullable=false, options={"comment"="Mantiene el Número de Teléfono que registró la persona."})
     */
    private $contactotelefono;

    /**
     * @var int
     *
     * @ORM\Column(name="ContactoCelular", type="integer", nullable=false, options={"comment"="Registra el Número de Teléfono Celular que brindó la persona para el contacto."})
     */
    private $contactocelular;

    /**
     * @var string
     *
     * @ORM\Column(name="ContactoCorreo", type="string", length=40, nullable=false, options={"comment"="Conserva el dato del correo electrónico proporcionado por la persona que participa en el Centro Cívico para el contacto de la misma."})
     */
    private $contactocorreo;

    /**
     * @var string
     *
     * @ORM\Column(name="ContactoOtroMedio", type="string", length=40, nullable=false, options={"comment"="Contiene la información relacionada a otra forma de contacto que posea la persona."})
     */
    private $contactootromedio;

    /**
     * @var string
     *
     * @ORM\Column(name="ContactoOpcion", type="string", length=40, nullable=false, options={"comment"="Guarda la manera en que se desea ser contactado"})
     */
    private $contactoopcion;

    /**
     * @var string
     *
     * @ORM\Column(name="ContactoUsrCrea", type="string", length=255, nullable=false, options={"comment"="Almacena la Información del usuario que hace el resgitro del dato."})
     */
    private $contactousrcrea;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ContactoFecCrea", type="datetime", nullable=false, options={"comment"="Mantiene la Fecha en que se realizó el registro de la información."})
     */
    private $contactofeccrea;

    /**
     * @var bool
     *
     * @ORM\Column(name="ContactoActivo", type="boolean", nullable=false, options={"comment"="Al ser una tabla que mantiene un historico de los medios de contacto de la persona que participa en un Centro Cívico, se marca como activo el resgitro vigente, en caso contario se coloca un 0 para indicar que dicho registro no es el actual. "})
     */
    private $contactoactivo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ContactoFecMod", type="datetime", nullable=false, options={"comment"="Conserva la fecha en la que se hizo alguna modificación al registro."})
     */
    private $contactofecmod;

    /**
     * @var string
     *
     * @ORM\Column(name="ContactoUsrMod", type="string", length=255, nullable=false, options={"comment"="Preserva la información del usuario que realiza una modificación a la información."})
     */
    private $contactousrmod;

    /**
     * @var \Expediente
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Expediente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ExpedienteNumero", referencedColumnName="ExpedienteNumero")
     * })
     */
    private $expedientenumero;

    public function getContactoid(): ?int
    {
        return $this->contactoid;
    }

    public function getContactotelefono(): ?int
    {
        return $this->contactotelefono;
    }

    public function setContactotelefono(int $contactotelefono): self
    {
        $this->contactotelefono = $contactotelefono;

        return $this;
    }

    public function getContactocelular(): ?int
    {
        return $this->contactocelular;
    }

    public function setContactocelular(int $contactocelular): self
    {
        $this->contactocelular = $contactocelular;

        return $this;
    }

    public function getContactocorreo(): ?string
    {
        return $this->contactocorreo;
    }

    public function setContactocorreo(string $contactocorreo): self
    {
        $this->contactocorreo = $contactocorreo;

        return $this;
    }

    public function getContactootromedio(): ?string
    {
        return $this->contactootromedio;
    }

    public function setContactootromedio(string $contactootromedio): self
    {
        $this->contactootromedio = $contactootromedio;

        return $this;
    }

    public function getContactoopcion(): ?string
    {
        return $this->contactoopcion;
    }

    public function setContactoopcion(string $contactoopcion): self
    {
        $this->contactoopcion = $contactoopcion;

        return $this;
    }

    public function getContactousrcrea(): ?string
    {
        return $this->contactousrcrea;
    }

    public function setContactousrcrea(string $contactousrcrea): self
    {
        $this->contactousrcrea = $contactousrcrea;

        return $this;
    }

    public function getContactofeccrea(): ?\DateTimeInterface
    {
        return $this->contactofeccrea;
    }

    public function setContactofeccrea(\DateTimeInterface $contactofeccrea): self
    {
        $this->contactofeccrea = $contactofeccrea;

        return $this;
    }

    public function getContactoactivo(): ?bool
    {
        return $this->contactoactivo;
    }

    public function setContactoactivo(bool $contactoactivo): self
    {
        $this->contactoactivo = $contactoactivo;

        return $this;
    }

    public function getContactofecmod(): ?\DateTimeInterface
    {
        return $this->contactofecmod;
    }

    public function setContactofecmod(\DateTimeInterface $contactofecmod): self
    {
        $this->contactofecmod = $contactofecmod;

        return $this;
    }

    public function getContactousrmod(): ?string
    {
        return $this->contactousrmod;
    }

    public function setContactousrmod(string $contactousrmod): self
    {
        $this->contactousrmod = $contactousrmod;

        return $this;
    }

    public function getExpedientenumero(): ?Expediente
    {
        return $this->expedientenumero;
    }

    public function setExpedientenumero(?Expediente $expedientenumero): self
    {
        $this->expedientenumero = $expedientenumero;

        return $this;
    }


}
