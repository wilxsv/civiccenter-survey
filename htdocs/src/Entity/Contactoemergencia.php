<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contactoemergencia
 *
 * @ORM\Table(name="ContactoEmergencia", indexes={@ORM\Index(name="ICONTACTOEMERGENCIA1", columns={"ExpedienteNumero"})})
 * @ORM\Entity
 */
class Contactoemergencia
{
    /**
     * @var int
     *
     * @ORM\Column(name="ContactoEmergenciaID", type="integer", nullable=false, options={"comment"="Guarda el número consecutivo de cada contacto de emergencia registrado a cada persona que participa en un Centro Cívico, la misma se utiliza como una llave compuesta junto con el Número de Expediente, donde el mismo ira aumentando según la cantidad de registros que tenga una persona."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $contactoemergenciaid;

    /**
     * @var string
     *
     * @ORM\Column(name="ContactoEmergenciaNombComp", type="string", length=100, nullable=false, options={"comment"="Contiene la Información del Nombre Completo de la persona que se estableció como Contacto de Emergencia."})
     */
    private $contactoemergencianombcomp;

    /**
     * @var string
     *
     * @ORM\Column(name="ContactoEmergenciaRelacion", type="string", length=40, nullable=false, options={"comment"="Registra la información de la relación de la persona registrada como contacto de emergencia, con la persona que participa en el Centro Cívico."})
     */
    private $contactoemergenciarelacion;

    /**
     * @var int
     *
     * @ORM\Column(name="ContactoEmergenciaTelefono", type="integer", nullable=false, options={"comment"="Mantiene el número de teléfono del contacto de emergencia."})
     */
    private $contactoemergenciatelefono;

    /**
     * @var string
     *
     * @ORM\Column(name="ContactoEmergenciaUsrCrea", type="string", length=255, nullable=false, options={"comment"="Preserva la información del usuario que realiza el registro inicial de la información."})
     */
    private $contactoemergenciausrcrea;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ContactoEmergenciaFecCrea", type="datetime", nullable=false, options={"comment"="Guarda la informaicón de la fecha en la cual se realiza el primer registro de la información."})
     */
    private $contactoemergenciafeccrea;

    /**
     * @var string
     *
     * @ORM\Column(name="ContactoEmergenciaUsrMod", type="string", length=255, nullable=false, options={"comment"="Registra la información del usuario que realiza una modificación de la información."})
     */
    private $contactoemergenciausrmod;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ContactoEmergenciaFecMod", type="datetime", nullable=false, options={"comment"="Almacena la fecha en la cual se realizó una modificación de los datos registrados."})
     */
    private $contactoemergenciafecmod;

    /**
     * @var bool
     *
     * @ORM\Column(name="ContactoEmergenciaActivo", type="boolean", nullable=false, options={"comment"="Mantiene el estado del contacto de emergencia, como la persona que participa en un Centro Cívico, puede cambiar el contacto de emergencia por otro o volver a poner uno anterior se registra con un 1 para el contacto actual y 0 para los contactos anteriores."})
     */
    private $contactoemergenciaactivo;

    /**
     * @var \Expediente
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Expediente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ExpedienteNumero", referencedColumnName="ExpedienteNumero")
     * })
     */
    private $expedientenumero;

    public function getContactoemergenciaid(): ?int
    {
        return $this->contactoemergenciaid;
    }

    public function getContactoemergencianombcomp(): ?string
    {
        return $this->contactoemergencianombcomp;
    }

    public function setContactoemergencianombcomp(string $contactoemergencianombcomp): self
    {
        $this->contactoemergencianombcomp = $contactoemergencianombcomp;

        return $this;
    }

    public function getContactoemergenciarelacion(): ?string
    {
        return $this->contactoemergenciarelacion;
    }

    public function setContactoemergenciarelacion(string $contactoemergenciarelacion): self
    {
        $this->contactoemergenciarelacion = $contactoemergenciarelacion;

        return $this;
    }

    public function getContactoemergenciatelefono(): ?int
    {
        return $this->contactoemergenciatelefono;
    }

    public function setContactoemergenciatelefono(int $contactoemergenciatelefono): self
    {
        $this->contactoemergenciatelefono = $contactoemergenciatelefono;

        return $this;
    }

    public function getContactoemergenciausrcrea(): ?string
    {
        return $this->contactoemergenciausrcrea;
    }

    public function setContactoemergenciausrcrea(string $contactoemergenciausrcrea): self
    {
        $this->contactoemergenciausrcrea = $contactoemergenciausrcrea;

        return $this;
    }

    public function getContactoemergenciafeccrea(): ?\DateTimeInterface
    {
        return $this->contactoemergenciafeccrea;
    }

    public function setContactoemergenciafeccrea(\DateTimeInterface $contactoemergenciafeccrea): self
    {
        $this->contactoemergenciafeccrea = $contactoemergenciafeccrea;

        return $this;
    }

    public function getContactoemergenciausrmod(): ?string
    {
        return $this->contactoemergenciausrmod;
    }

    public function setContactoemergenciausrmod(string $contactoemergenciausrmod): self
    {
        $this->contactoemergenciausrmod = $contactoemergenciausrmod;

        return $this;
    }

    public function getContactoemergenciafecmod(): ?\DateTimeInterface
    {
        return $this->contactoemergenciafecmod;
    }

    public function setContactoemergenciafecmod(\DateTimeInterface $contactoemergenciafecmod): self
    {
        $this->contactoemergenciafecmod = $contactoemergenciafecmod;

        return $this;
    }

    public function getContactoemergenciaactivo(): ?bool
    {
        return $this->contactoemergenciaactivo;
    }

    public function setContactoemergenciaactivo(bool $contactoemergenciaactivo): self
    {
        $this->contactoemergenciaactivo = $contactoemergenciaactivo;

        return $this;
    }

    public function getExpedientenumero(): ?Expediente
    {
        return $this->expedientenumero;
    }

    public function setExpedientenumero(?Expediente $expedientenumero): self
    {
        $this->expedientenumero = $expedientenumero;

        return $this;
    }


}
