<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Curso
 *
 * @ORM\Table(name="Curso", indexes={@ORM\Index(name="ICURSO1", columns={"CentroCivicoID"}), @ORM\Index(name="ICURSO2", columns={"TipoProcesoID"}), @ORM\Index(name="ICURSO3", columns={"InstitucionID"})})
 * @ORM\Entity
 */
class Curso
{
    /**
     * @var int
     *
     * @ORM\Column(name="CursoID", type="integer", nullable=false, options={"comment"="Mantiene un Número Consecutivo para cada registro de información de los distintos procesos formativos."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cursoid;

    /**
     * @var string
     *
     * @ORM\Column(name="CursoNombre", type="string", length=300, nullable=false, options={"comment"="Preserva la información del nombre del Proceso Formativo creado."})
     */
    private $cursonombre;

    /**
     * @var string
     *
     * @ORM\Column(name="CursoEstado", type="string", length=40, nullable=false, options={"comment"="Conserva el Estado en que se encuentra el Proceso Formativo, en caso de que el proceso no esté disponible se indica por medio de la palabra NO, en caso de estar activo aparece un SI."})
     */
    private $cursoestado;

    /**
     * @var string
     *
     * @ORM\Column(name="CursoCodigo", type="string", length=40, nullable=false, options={"comment"="Mantiene el Código del Curso que se forma con las siglas PF de Proceso Formativo, las siglas del Centro Cívico y el consecutivo."})
     */
    private $cursocodigo;

    /**
     * @var string
     *
     * @ORM\Column(name="CursoUsrCrea", type="string", length=300, nullable=false, options={"comment"="Registra la información del usuario que crea el Proceso Formativo."})
     */
    private $cursousrcrea;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CursoFecCrea", type="datetime", nullable=false, options={"comment"="Preserva la Fecha en que fue creado el Proceso Formativo."})
     */
    private $cursofeccrea;

    /**
     * @var string
     *
     * @ORM\Column(name="CursoUsrMod", type="string", length=300, nullable=false, options={"comment"="Almacena el usuario que realiza alguna modificación en el Proceso Formativo creado."})
     */
    private $cursousrmod;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CursoFecMod", type="datetime", nullable=false, options={"comment"="Guarda la Fecha de cuando se hizo la modificación de la información."})
     */
    private $cursofecmod;

    /**
     * @var \Centrocivico
     *
     * @ORM\ManyToOne(targetEntity="Centrocivico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CentroCivicoID", referencedColumnName="CentroCivicoID")
     * })
     */
    private $centrocivicoid;

    /**
     * @var \Institucion
     *
     * @ORM\ManyToOne(targetEntity="Institucion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="InstitucionID", referencedColumnName="InstitucionID")
     * })
     */
    private $institucionid;

    /**
     * @var \Tipoproceso
     *
     * @ORM\ManyToOne(targetEntity="Tipoproceso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="TipoProcesoID", referencedColumnName="TipoProcesoID")
     * })
     */
    private $tipoprocesoid;

    public function getCursoid(): ?int
    {
        return $this->cursoid;
    }

    public function getCursonombre(): ?string
    {
        return $this->cursonombre;
    }

    public function setCursonombre(string $cursonombre): self
    {
        $this->cursonombre = $cursonombre;

        return $this;
    }

    public function getCursoestado(): ?string
    {
        return $this->cursoestado;
    }

    public function setCursoestado(string $cursoestado): self
    {
        $this->cursoestado = $cursoestado;

        return $this;
    }

    public function getCursocodigo(): ?string
    {
        return $this->cursocodigo;
    }

    public function setCursocodigo(string $cursocodigo): self
    {
        $this->cursocodigo = $cursocodigo;

        return $this;
    }

    public function getCursousrcrea(): ?string
    {
        return $this->cursousrcrea;
    }

    public function setCursousrcrea(string $cursousrcrea): self
    {
        $this->cursousrcrea = $cursousrcrea;

        return $this;
    }

    public function getCursofeccrea(): ?\DateTimeInterface
    {
        return $this->cursofeccrea;
    }

    public function setCursofeccrea(\DateTimeInterface $cursofeccrea): self
    {
        $this->cursofeccrea = $cursofeccrea;

        return $this;
    }

    public function getCursousrmod(): ?string
    {
        return $this->cursousrmod;
    }

    public function setCursousrmod(string $cursousrmod): self
    {
        $this->cursousrmod = $cursousrmod;

        return $this;
    }

    public function getCursofecmod(): ?\DateTimeInterface
    {
        return $this->cursofecmod;
    }

    public function setCursofecmod(\DateTimeInterface $cursofecmod): self
    {
        $this->cursofecmod = $cursofecmod;

        return $this;
    }

    public function getCentrocivicoid(): ?Centrocivico
    {
        return $this->centrocivicoid;
    }

    public function setCentrocivicoid(?Centrocivico $centrocivicoid): self
    {
        $this->centrocivicoid = $centrocivicoid;

        return $this;
    }

    public function getInstitucionid(): ?Institucion
    {
        return $this->institucionid;
    }

    public function setInstitucionid(?Institucion $institucionid): self
    {
        $this->institucionid = $institucionid;

        return $this;
    }

    public function getTipoprocesoid(): ?Tipoproceso
    {
        return $this->tipoprocesoid;
    }

    public function setTipoprocesoid(?Tipoproceso $tipoprocesoid): self
    {
        $this->tipoprocesoid = $tipoprocesoid;

        return $this;
    }


}
