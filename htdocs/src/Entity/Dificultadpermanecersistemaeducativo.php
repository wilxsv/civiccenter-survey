<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dificultadpermanecersistemaeducativo
 *
 * @ORM\Table(name="DificultadPermanecerSistemaEducativo", uniqueConstraints={@ORM\UniqueConstraint(name="UN_PermaneceSEducativoDescripcion", columns={"PermaneceSEducativoDescripcion"})})
 * @ORM\Entity
 */
class Dificultadpermanecersistemaeducativo
{
    /**
     * @var int
     *
     * @ORM\Column(name="PermaneceSEducativoID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $permaneceseducativoid;

    /**
     * @var string
     *
     * @ORM\Column(name="PermaneceSEducativoDescripcion", type="string", length=120, nullable=false)
     */
    private $permaneceseducativodescripcion;

    public function getPermaneceseducativoid(): ?int
    {
        return $this->permaneceseducativoid;
    }

    public function getPermaneceseducativodescripcion(): ?string
    {
        return $this->permaneceseducativodescripcion;
    }

    public function setPermaneceseducativodescripcion(string $permaneceseducativodescripcion): self
    {
        $this->permaneceseducativodescripcion = $permaneceseducativodescripcion;

        return $this;
    }


}
