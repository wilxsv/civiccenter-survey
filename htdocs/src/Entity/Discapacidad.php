<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Discapacidad
 *
 * @ORM\Table(name="Discapacidad", uniqueConstraints={@ORM\UniqueConstraint(name="UN_DiscapacidadDescripcion", columns={"DiscapacidadDescripcion"})})
 * @ORM\Entity
 */
class Discapacidad
{
    /**
     * @var int
     *
     * @ORM\Column(name="DiscapacidadID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $discapacidadid;

    /**
     * @var string
     *
     * @ORM\Column(name="DiscapacidadDescripcion", type="string", length=120, nullable=false)
     */
    private $discapacidaddescripcion;

    public function getDiscapacidadid(): ?int
    {
        return $this->discapacidadid;
    }

    public function getDiscapacidaddescripcion(): ?string
    {
        return $this->discapacidaddescripcion;
    }

    public function setDiscapacidaddescripcion(string $discapacidaddescripcion): self
    {
        $this->discapacidaddescripcion = $discapacidaddescripcion;

        return $this;
    }


}
