<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Distrito
 *
 * @ORM\Table(name="Distrito", indexes={@ORM\Index(name="IDISTRITO1", columns={"CantonID"})})
 * @ORM\Entity
 */
class Distrito
{
    /**
     * @var int
     *
     * @ORM\Column(name="DistritoID", type="integer", nullable=false, options={"comment"="Indica la Llave Primaria que se va a utilizar para usarlo como relación con otra información."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $distritoid;

    /**
     * @var string
     *
     * @ORM\Column(name="DistritoDescripcion", type="string", length=300, nullable=false, options={"comment"="Almacena el nombre del Distrito."})
     */
    private $distritodescripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="DistritoGeoJson", type="text", length=16, nullable=false, options={"default"="{}"})
     */
    private $distritogeojson = '{}';

    /**
     * @var \Canton
     *
     * @ORM\ManyToOne(targetEntity="Canton")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CantonID", referencedColumnName="CantonID")
     * })
     */
    private $cantonid;

    public function getDistritoid(): ?int
    {
        return $this->distritoid;
    }

    public function getDistritodescripcion(): ?string
    {
        return $this->distritodescripcion;
    }

    public function setDistritodescripcion(string $distritodescripcion): self
    {
        $this->distritodescripcion = $distritodescripcion;

        return $this;
    }

    public function getDistritogeojson(): ?string
    {
        return $this->distritogeojson;
    }

    public function setDistritogeojson(string $distritogeojson): self
    {
        $this->distritogeojson = $distritogeojson;

        return $this;
    }

    public function getCantonid(): ?Canton
    {
        return $this->cantonid;
    }

    public function setCantonid(?Canton $cantonid): self
    {
        $this->cantonid = $cantonid;

        return $this;
    }


}
