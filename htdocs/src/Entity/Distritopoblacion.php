<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Distritopoblacion
 *
 * @ORM\Table(name="DistritoPoblacion", indexes={@ORM\Index(name="IDX_7C13F18A9BF7E9FD", columns={"DistritoID"})})
 * @ORM\Entity
 */
class Distritopoblacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="DistritoPoblacionAnio", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $distritopoblacionanio;

    /**
     * @var int
     *
     * @ORM\Column(name="DistritoPoblacionEdad", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $distritopoblacionedad;

    /**
     * @var float
     *
     * @ORM\Column(name="DistritoPoblacionTotal", type="float", precision=24, scale=0, nullable=false)
     */
    private $distritopoblaciontotal;

    /**
     * @var float
     *
     * @ORM\Column(name="DistritoPoblacionHombre", type="float", precision=24, scale=0, nullable=false)
     */
    private $distritopoblacionhombre;

    /**
     * @var float
     *
     * @ORM\Column(name="DistritoPoblacionMujer", type="float", precision=24, scale=0, nullable=false)
     */
    private $distritopoblacionmujer;

    /**
     * @var \Distrito
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Distrito")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="DistritoID", referencedColumnName="DistritoID")
     * })
     */
    private $distritoid;

    public function getDistritopoblacionanio(): ?int
    {
        return $this->distritopoblacionanio;
    }

    public function getDistritopoblacionedad(): ?int
    {
        return $this->distritopoblacionedad;
    }

    public function getDistritopoblaciontotal(): ?float
    {
        return $this->distritopoblaciontotal;
    }

    public function setDistritopoblaciontotal(float $distritopoblaciontotal): self
    {
        $this->distritopoblaciontotal = $distritopoblaciontotal;

        return $this;
    }

    public function getDistritopoblacionhombre(): ?float
    {
        return $this->distritopoblacionhombre;
    }

    public function setDistritopoblacionhombre(float $distritopoblacionhombre): self
    {
        $this->distritopoblacionhombre = $distritopoblacionhombre;

        return $this;
    }

    public function getDistritopoblacionmujer(): ?float
    {
        return $this->distritopoblacionmujer;
    }

    public function setDistritopoblacionmujer(float $distritopoblacionmujer): self
    {
        $this->distritopoblacionmujer = $distritopoblacionmujer;

        return $this;
    }

    public function getDistritoid(): ?Distrito
    {
        return $this->distritoid;
    }

    public function setDistritoid(?Distrito $distritoid): self
    {
        $this->distritoid = $distritoid;

        return $this;
    }


}
