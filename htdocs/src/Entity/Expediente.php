<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Expediente
 *
 * @ORM\Table(name="Expediente", indexes={@ORM\Index(name="IEXPEDIENTE1", columns={"SexoID"}), @ORM\Index(name="IEXPEDIENTE3", columns={"NacionalidadID"})})
 * @ORM\Entity
 */
class Expediente
{
    /**
     * @var string
     *
     * @ORM\Column(name="ExpedienteNumero", type="string", length=10, nullable=false, options={"comment"="Se define el ExpedienteNumero como la Llave Primaria."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $expedientenumero;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteTipoIdentif", type="string", length=30, nullable=true, options={"comment"="Preserva el Número Consecutivo del tipo de identificación que puede tener una persona que participa en un Centro Cívico."})
     */
    private $expedientetipoidentif;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteIndentif", type="string", length=50, nullable=true, options={"comment"="Guarda el Número de Documento de Identificación que posee una persona que participa en un Centro Cívico."})
     */
    private $expedienteindentif;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteApellido1", type="string", length=50, nullable=true, options={"comment"="Mantiene la información del Primer Apellido de la persona registrada en el Centro Cívico."})
     */
    private $expedienteapellido1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteApellido2", type="string", length=50, nullable=true, options={"comment"="Conserva los datos del Segundo Apellido que posee una persona que ingresa al Centro Cívico."})
     */
    private $expedienteapellido2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteNombre1", type="string", length=50, nullable=true, options={"comment"="Registra el Nombre de la persona que ingresa al Centro Cívico."})
     */
    private $expedientenombre1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteSexoObs", type="string", length=200, nullable=true, options={"comment"="Guarda las observaciones pertienentes de lo que observa un funcionario del Centro Cívico o lo que reporta la persona que ingresa al Centro Cívico."})
     */
    private $expedientesexoobs;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="ExpedienteFecNac", type="datetime", nullable=true, options={"comment"="Preserva la Fecha de Nacimiento de las personas del Centro Cívico."})
     */
    private $expedientefecnac;

    /**
     * @var int
     *
     * @ORM\Column(name="ExpedienteDependiente", type="integer", nullable=false, options={"comment"="Mantiene el nombre de la persona de la cual depende un usuario del Centro Cívico."})
     */
    private $expedientedependiente;

    /**
     * @var int
     *
     * @ORM\Column(name="ExpedienteMadreVive", type="integer", nullable=false, options={"comment"="Conserva la informacion del estado de la Madre según la prespuesta brindada por la persona del Centro Cívico."})
     */
    private $expedientemadrevive;

    /**
     * @var string
     *
     * @ORM\Column(name="ExpedienteMadreVive1", type="string", length=3, nullable=false)
     */
    private $expedientemadrevive1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteMadreApel1", type="string", length=50, nullable=true, options={"comment"="Registra el Primer Apellido de la Madre."})
     */
    private $expedientemadreapel1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteMadreApel2", type="string", length=50, nullable=true, options={"comment"="Guarda el Segundo Apellido de la Madre."})
     */
    private $expedientemadreapel2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteMadreNom1", type="string", length=50, nullable=true, options={"comment"="Almacena el Nombre de la Madre."})
     */
    private $expedientemadrenom1;

    /**
     * @var int
     *
     * @ORM\Column(name="ExpedientePadreVive", type="integer", nullable=false, options={"comment"="Preserva la informacion del estado del Padre según la prespuesta brindada por la persona del Centro Cívico."})
     */
    private $expedientepadrevive;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedientePadreApel1", type="string", length=50, nullable=true, options={"comment"="Mantiene el Primer Apellido del Padre."})
     */
    private $expedientepadreapel1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedientePadreApel2", type="string", length=50, nullable=true, options={"comment"="Conserva el Segundo Apellido del Padre."})
     */
    private $expedientepadreapel2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedientePadreNom1", type="string", length=50, nullable=true, options={"comment"="Registra el Nombre del Padre."})
     */
    private $expedientepadrenom1;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="ExpedienteFecCrea", type="datetime", nullable=true, options={"comment"="Almacena la Fecha en que se crea el Expediente."})
     */
    private $expedientefeccrea;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteUsrCrea", type="string", length=255, nullable=true, options={"comment"="Guarda el Usuario que crea realiza el ingreso del Expediente."})
     */
    private $expedienteusrcrea;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="ExpedienteFecMod", type="datetime", nullable=true, options={"comment"="Mantiene la Fecha de cuando se modifica un registro."})
     */
    private $expedientefecmod;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteUsrMod", type="string", length=255, nullable=true, options={"comment"="Preserva la Información del usuario que realiza modificaciones a un registro."})
     */
    private $expedienteusrmod;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteProfOfic", type="string", length=255, nullable=true, options={"comment"="Mantiene la informacion de la Profesión u Oficio de la persona que participa en el Centr Cívico."})
     */
    private $expedienteprofofic;

    /**
     * @var int
     *
     * @ORM\Column(name="ExpedienteEnferCronica", type="integer", nullable=false, options={"comment"="Guarda la respuesta proporcionada por el usuario acerca de si padece una enfermedad crónica."})
     */
    private $expedienteenfercronica;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteEnfermedades", type="string", length=255, nullable=true, options={"comment"="Almacena la informacíon acerca de las enfermedades crónicas que padece alguna persona que participa en un Centro Cívico."})
     */
    private $expedienteenfermedades;

    /**
     * @var int
     *
     * @ORM\Column(name="ExpedienteTipoSangre", type="integer", nullable=false, options={"comment"="Registra la información de la respuesta del tipo de sangre que posee la persona que ingresa al Centro Cívico."})
     */
    private $expedientetiposangre;

    /**
     * @var int
     *
     * @ORM\Column(name="ExpedienteAdecuacion", type="integer", nullable=false, options={"comment"="Preserva la respuesta que brinda la persona del Centro Cívico acera de si posee algún tipo de adecuación."})
     */
    private $expedienteadecuacion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteAdecTipo", type="string", length=255, nullable=true, options={"comment"="Guarda la información acerca del tipo de adecuación que necesita la persona."})
     */
    private $expedienteadectipo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteAdecComen", type="string", length=255, nullable=true, options={"comment"="Almacena los comentarios de la adecuación de la persona que participa en el Centro Cívico."})
     */
    private $expedienteadeccomen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteEducOtroNivel", type="string", length=50, nullable=true, options={"comment"="Mantiene la informacion del Nivel Educativo que posee la persona que se encuentra en el Centro Cívico."})
     */
    private $expedienteeducotronivel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteEducUltAnno", type="string", length=40, nullable=true, options={"comment"="Registra la respuesta acerca del útlimo año aprobado."})
     */
    private $expedienteeducultanno;

    /**
     * @var int
     *
     * @ORM\Column(name="ExpedienteEducActual", type="integer", nullable=false, options={"comment"="Preserva la información de la respuesta si la persona estudia actualmente."})
     */
    private $expedienteeducactual;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteEducEstudio", type="string", length=40, nullable=true, options={"comment"="Guarda la información de lo que esta estudiando la persona."})
     */
    private $expedienteeducestudio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteEducLugarEst", type="string", length=100, nullable=true, options={"comment"="Almacena la información del lugar de estudio de la persona que participa en el Centro Cívico."})
     */
    private $expedienteeduclugarest;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteEducProbEst", type="string", length=100, nullable=true, options={"comment"="Mantiene la información de la respuesta si la persona tiene algun problema para seguir estudiando."})
     */
    private $expedienteeducprobest;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteEducMotivo", type="string", length=100, nullable=true, options={"comment"="Preserva la información del Motivo por el cuál no puede seguir estudiando."})
     */
    private $expedienteeducmotivo;

    /**
     * @var int
     *
     * @ORM\Column(name="ExpedienteEducVolverEst", type="integer", nullable=false, options={"comment"="Registra la información de la respuesta si desea volver a Estudiar."})
     */
    private $expedienteeducvolverest;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteEducVolverEstObs", type="string", length=255, nullable=true, options={"comment"="Guarda las observaciones acerca de las respuesta si desea volver a estudiar la persona que ingresa al Centro Cívico."})
     */
    private $expedienteeducvolverestobs;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteEducNecesidad", type="string", length=255, nullable=true, options={"comment"="Almacena los datos acerca de las necesidades que tiene la persona para poder seguir estudiando."})
     */
    private $expedienteeducnecesidad;

    /**
     * @var int
     *
     * @ORM\Column(name="ExpedienteEducConcluirEst", type="integer", nullable=false, options={"comment"="Mantiene la información de la respuesta que brindan las personas, acerda si desean concluir sus estudios."})
     */
    private $expedienteeducconcluirest;

    /**
     * @var int
     *
     * @ORM\Column(name="ExpedienteNivelEducativo", type="integer", nullable=false, options={"comment"="Preserva la información acerca del Nivel Educativo que posee la persona."})
     */
    private $expedienteniveleducativo;

    /**
     * @var int
     *
     * @ORM\Column(name="ExpedienteHijos", type="integer", nullable=false, options={"comment"="Registra la respuesta de las personas si tiene hijos."})
     */
    private $expedientehijos;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ExpedienteHijosCant", type="smallint", nullable=true, options={"comment"="Guarda la cantidad de hijos que posee la persona que participa en los Centros Cívicos."})
     */
    private $expedientehijoscant;

    /**
     * @var int
     *
     * @ORM\Column(name="ExpedienteHijosTener", type="integer", nullable=false, options={"comment"="Almacena la cantidad de hijos que le gustaría tener a una persona que participa en los Centros Cívicos."})
     */
    private $expedientehijostener;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteHabilidades", type="string", length=255, nullable=true, options={"comment"="Mantiene la información de las habilidades de cada persona."})
     */
    private $expedientehabilidades;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedientePreocupaciones", type="string", length=255, nullable=true, options={"comment"="Registra las observaciones de las preocupaciones que tiene la persona en un Centro Cívico."})
     */
    private $expedientepreocupaciones;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteGustEstudiar", type="string", length=255, nullable=true, options={"comment"="Preserva la informacion de la respuesta de la persona si le gustaría estudiar."})
     */
    private $expedientegustestudiar;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteGustHacer1", type="string", length=255, nullable=true, options={"comment"="Guarda las respuesta acerca de lo que le gustaria hacer a la persona del Centro Cívico en un año."})
     */
    private $expedientegusthacer1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteGustHacer5", type="string", length=255, nullable=true, options={"comment"="Almacena respuesta acerca de lo que le gustaria hacer a la persona del Centro Cívico en cinco años."})
     */
    private $expedientegusthacer5;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteOpinionCC", type="string", length=255, nullable=true, options={"comment"="Mantiene la información de la opinión de las personas acerca del Centro Cívico."})
     */
    private $expedienteopinioncc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ExpedienteOtrasActiv", type="string", length=255, nullable=true, options={"comment"="Registra otras actividades que las personas desean hacer."})
     */
    private $expedienteotrasactiv;

    /**
     * @var \Nacionalidad
     *
     * @ORM\ManyToOne(targetEntity="Nacionalidad")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="NacionalidadID", referencedColumnName="NacionalidadID")
     * })
     */
    private $nacionalidadid;

    /**
     * @var \Sexo
     *
     * @ORM\ManyToOne(targetEntity="Sexo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SexoID", referencedColumnName="SexoID")
     * })
     */
    private $sexoid;

    public function getExpedientenumero(): ?string
    {
        return $this->expedientenumero;
    }

    public function getExpedientetipoidentif(): ?string
    {
        return $this->expedientetipoidentif;
    }

    public function setExpedientetipoidentif(?string $expedientetipoidentif): self
    {
        $this->expedientetipoidentif = $expedientetipoidentif;

        return $this;
    }

    public function getExpedienteindentif(): ?string
    {
        return $this->expedienteindentif;
    }

    public function setExpedienteindentif(?string $expedienteindentif): self
    {
        $this->expedienteindentif = $expedienteindentif;

        return $this;
    }

    public function getExpedienteapellido1(): ?string
    {
        return $this->expedienteapellido1;
    }

    public function setExpedienteapellido1(?string $expedienteapellido1): self
    {
        $this->expedienteapellido1 = $expedienteapellido1;

        return $this;
    }

    public function getExpedienteapellido2(): ?string
    {
        return $this->expedienteapellido2;
    }

    public function setExpedienteapellido2(?string $expedienteapellido2): self
    {
        $this->expedienteapellido2 = $expedienteapellido2;

        return $this;
    }

    public function getExpedientenombre1(): ?string
    {
        return $this->expedientenombre1;
    }

    public function setExpedientenombre1(?string $expedientenombre1): self
    {
        $this->expedientenombre1 = $expedientenombre1;

        return $this;
    }

    public function getExpedientesexoobs(): ?string
    {
        return $this->expedientesexoobs;
    }

    public function setExpedientesexoobs(?string $expedientesexoobs): self
    {
        $this->expedientesexoobs = $expedientesexoobs;

        return $this;
    }

    public function getExpedientefecnac(): ?\DateTimeInterface
    {
        return $this->expedientefecnac;
    }

    public function setExpedientefecnac(?\DateTimeInterface $expedientefecnac): self
    {
        $this->expedientefecnac = $expedientefecnac;

        return $this;
    }

    public function getExpedientedependiente(): ?int
    {
        return $this->expedientedependiente;
    }

    public function setExpedientedependiente(int $expedientedependiente): self
    {
        $this->expedientedependiente = $expedientedependiente;

        return $this;
    }

    public function getExpedientemadrevive(): ?int
    {
        return $this->expedientemadrevive;
    }

    public function setExpedientemadrevive(int $expedientemadrevive): self
    {
        $this->expedientemadrevive = $expedientemadrevive;

        return $this;
    }

    public function getExpedientemadrevive1(): ?string
    {
        return $this->expedientemadrevive1;
    }

    public function setExpedientemadrevive1(string $expedientemadrevive1): self
    {
        $this->expedientemadrevive1 = $expedientemadrevive1;

        return $this;
    }

    public function getExpedientemadreapel1(): ?string
    {
        return $this->expedientemadreapel1;
    }

    public function setExpedientemadreapel1(?string $expedientemadreapel1): self
    {
        $this->expedientemadreapel1 = $expedientemadreapel1;

        return $this;
    }

    public function getExpedientemadreapel2(): ?string
    {
        return $this->expedientemadreapel2;
    }

    public function setExpedientemadreapel2(?string $expedientemadreapel2): self
    {
        $this->expedientemadreapel2 = $expedientemadreapel2;

        return $this;
    }

    public function getExpedientemadrenom1(): ?string
    {
        return $this->expedientemadrenom1;
    }

    public function setExpedientemadrenom1(?string $expedientemadrenom1): self
    {
        $this->expedientemadrenom1 = $expedientemadrenom1;

        return $this;
    }

    public function getExpedientepadrevive(): ?int
    {
        return $this->expedientepadrevive;
    }

    public function setExpedientepadrevive(int $expedientepadrevive): self
    {
        $this->expedientepadrevive = $expedientepadrevive;

        return $this;
    }

    public function getExpedientepadreapel1(): ?string
    {
        return $this->expedientepadreapel1;
    }

    public function setExpedientepadreapel1(?string $expedientepadreapel1): self
    {
        $this->expedientepadreapel1 = $expedientepadreapel1;

        return $this;
    }

    public function getExpedientepadreapel2(): ?string
    {
        return $this->expedientepadreapel2;
    }

    public function setExpedientepadreapel2(?string $expedientepadreapel2): self
    {
        $this->expedientepadreapel2 = $expedientepadreapel2;

        return $this;
    }

    public function getExpedientepadrenom1(): ?string
    {
        return $this->expedientepadrenom1;
    }

    public function setExpedientepadrenom1(?string $expedientepadrenom1): self
    {
        $this->expedientepadrenom1 = $expedientepadrenom1;

        return $this;
    }

    public function getExpedientefeccrea(): ?\DateTimeInterface
    {
        return $this->expedientefeccrea;
    }

    public function setExpedientefeccrea(?\DateTimeInterface $expedientefeccrea): self
    {
        $this->expedientefeccrea = $expedientefeccrea;

        return $this;
    }

    public function getExpedienteusrcrea(): ?string
    {
        return $this->expedienteusrcrea;
    }

    public function setExpedienteusrcrea(?string $expedienteusrcrea): self
    {
        $this->expedienteusrcrea = $expedienteusrcrea;

        return $this;
    }

    public function getExpedientefecmod(): ?\DateTimeInterface
    {
        return $this->expedientefecmod;
    }

    public function setExpedientefecmod(?\DateTimeInterface $expedientefecmod): self
    {
        $this->expedientefecmod = $expedientefecmod;

        return $this;
    }

    public function getExpedienteusrmod(): ?string
    {
        return $this->expedienteusrmod;
    }

    public function setExpedienteusrmod(?string $expedienteusrmod): self
    {
        $this->expedienteusrmod = $expedienteusrmod;

        return $this;
    }

    public function getExpedienteprofofic(): ?string
    {
        return $this->expedienteprofofic;
    }

    public function setExpedienteprofofic(?string $expedienteprofofic): self
    {
        $this->expedienteprofofic = $expedienteprofofic;

        return $this;
    }

    public function getExpedienteenfercronica(): ?int
    {
        return $this->expedienteenfercronica;
    }

    public function setExpedienteenfercronica(int $expedienteenfercronica): self
    {
        $this->expedienteenfercronica = $expedienteenfercronica;

        return $this;
    }

    public function getExpedienteenfermedades(): ?string
    {
        return $this->expedienteenfermedades;
    }

    public function setExpedienteenfermedades(?string $expedienteenfermedades): self
    {
        $this->expedienteenfermedades = $expedienteenfermedades;

        return $this;
    }

    public function getExpedientetiposangre(): ?int
    {
        return $this->expedientetiposangre;
    }

    public function setExpedientetiposangre(int $expedientetiposangre): self
    {
        $this->expedientetiposangre = $expedientetiposangre;

        return $this;
    }

    public function getExpedienteadecuacion(): ?int
    {
        return $this->expedienteadecuacion;
    }

    public function setExpedienteadecuacion(int $expedienteadecuacion): self
    {
        $this->expedienteadecuacion = $expedienteadecuacion;

        return $this;
    }

    public function getExpedienteadectipo(): ?string
    {
        return $this->expedienteadectipo;
    }

    public function setExpedienteadectipo(?string $expedienteadectipo): self
    {
        $this->expedienteadectipo = $expedienteadectipo;

        return $this;
    }

    public function getExpedienteadeccomen(): ?string
    {
        return $this->expedienteadeccomen;
    }

    public function setExpedienteadeccomen(?string $expedienteadeccomen): self
    {
        $this->expedienteadeccomen = $expedienteadeccomen;

        return $this;
    }

    public function getExpedienteeducotronivel(): ?string
    {
        return $this->expedienteeducotronivel;
    }

    public function setExpedienteeducotronivel(?string $expedienteeducotronivel): self
    {
        $this->expedienteeducotronivel = $expedienteeducotronivel;

        return $this;
    }

    public function getExpedienteeducultanno(): ?string
    {
        return $this->expedienteeducultanno;
    }

    public function setExpedienteeducultanno(?string $expedienteeducultanno): self
    {
        $this->expedienteeducultanno = $expedienteeducultanno;

        return $this;
    }

    public function getExpedienteeducactual(): ?int
    {
        return $this->expedienteeducactual;
    }

    public function setExpedienteeducactual(int $expedienteeducactual): self
    {
        $this->expedienteeducactual = $expedienteeducactual;

        return $this;
    }

    public function getExpedienteeducestudio(): ?string
    {
        return $this->expedienteeducestudio;
    }

    public function setExpedienteeducestudio(?string $expedienteeducestudio): self
    {
        $this->expedienteeducestudio = $expedienteeducestudio;

        return $this;
    }

    public function getExpedienteeduclugarest(): ?string
    {
        return $this->expedienteeduclugarest;
    }

    public function setExpedienteeduclugarest(?string $expedienteeduclugarest): self
    {
        $this->expedienteeduclugarest = $expedienteeduclugarest;

        return $this;
    }

    public function getExpedienteeducprobest(): ?string
    {
        return $this->expedienteeducprobest;
    }

    public function setExpedienteeducprobest(?string $expedienteeducprobest): self
    {
        $this->expedienteeducprobest = $expedienteeducprobest;

        return $this;
    }

    public function getExpedienteeducmotivo(): ?string
    {
        return $this->expedienteeducmotivo;
    }

    public function setExpedienteeducmotivo(?string $expedienteeducmotivo): self
    {
        $this->expedienteeducmotivo = $expedienteeducmotivo;

        return $this;
    }

    public function getExpedienteeducvolverest(): ?int
    {
        return $this->expedienteeducvolverest;
    }

    public function setExpedienteeducvolverest(int $expedienteeducvolverest): self
    {
        $this->expedienteeducvolverest = $expedienteeducvolverest;

        return $this;
    }

    public function getExpedienteeducvolverestobs(): ?string
    {
        return $this->expedienteeducvolverestobs;
    }

    public function setExpedienteeducvolverestobs(?string $expedienteeducvolverestobs): self
    {
        $this->expedienteeducvolverestobs = $expedienteeducvolverestobs;

        return $this;
    }

    public function getExpedienteeducnecesidad(): ?string
    {
        return $this->expedienteeducnecesidad;
    }

    public function setExpedienteeducnecesidad(?string $expedienteeducnecesidad): self
    {
        $this->expedienteeducnecesidad = $expedienteeducnecesidad;

        return $this;
    }

    public function getExpedienteeducconcluirest(): ?int
    {
        return $this->expedienteeducconcluirest;
    }

    public function setExpedienteeducconcluirest(int $expedienteeducconcluirest): self
    {
        $this->expedienteeducconcluirest = $expedienteeducconcluirest;

        return $this;
    }

    public function getExpedienteniveleducativo(): ?int
    {
        return $this->expedienteniveleducativo;
    }

    public function setExpedienteniveleducativo(int $expedienteniveleducativo): self
    {
        $this->expedienteniveleducativo = $expedienteniveleducativo;

        return $this;
    }

    public function getExpedientehijos(): ?int
    {
        return $this->expedientehijos;
    }

    public function setExpedientehijos(int $expedientehijos): self
    {
        $this->expedientehijos = $expedientehijos;

        return $this;
    }

    public function getExpedientehijoscant(): ?int
    {
        return $this->expedientehijoscant;
    }

    public function setExpedientehijoscant(?int $expedientehijoscant): self
    {
        $this->expedientehijoscant = $expedientehijoscant;

        return $this;
    }

    public function getExpedientehijostener(): ?int
    {
        return $this->expedientehijostener;
    }

    public function setExpedientehijostener(int $expedientehijostener): self
    {
        $this->expedientehijostener = $expedientehijostener;

        return $this;
    }

    public function getExpedientehabilidades(): ?string
    {
        return $this->expedientehabilidades;
    }

    public function setExpedientehabilidades(?string $expedientehabilidades): self
    {
        $this->expedientehabilidades = $expedientehabilidades;

        return $this;
    }

    public function getExpedientepreocupaciones(): ?string
    {
        return $this->expedientepreocupaciones;
    }

    public function setExpedientepreocupaciones(?string $expedientepreocupaciones): self
    {
        $this->expedientepreocupaciones = $expedientepreocupaciones;

        return $this;
    }

    public function getExpedientegustestudiar(): ?string
    {
        return $this->expedientegustestudiar;
    }

    public function setExpedientegustestudiar(?string $expedientegustestudiar): self
    {
        $this->expedientegustestudiar = $expedientegustestudiar;

        return $this;
    }

    public function getExpedientegusthacer1(): ?string
    {
        return $this->expedientegusthacer1;
    }

    public function setExpedientegusthacer1(?string $expedientegusthacer1): self
    {
        $this->expedientegusthacer1 = $expedientegusthacer1;

        return $this;
    }

    public function getExpedientegusthacer5(): ?string
    {
        return $this->expedientegusthacer5;
    }

    public function setExpedientegusthacer5(?string $expedientegusthacer5): self
    {
        $this->expedientegusthacer5 = $expedientegusthacer5;

        return $this;
    }

    public function getExpedienteopinioncc(): ?string
    {
        return $this->expedienteopinioncc;
    }

    public function setExpedienteopinioncc(?string $expedienteopinioncc): self
    {
        $this->expedienteopinioncc = $expedienteopinioncc;

        return $this;
    }

    public function getExpedienteotrasactiv(): ?string
    {
        return $this->expedienteotrasactiv;
    }

    public function setExpedienteotrasactiv(?string $expedienteotrasactiv): self
    {
        $this->expedienteotrasactiv = $expedienteotrasactiv;

        return $this;
    }

    public function getNacionalidadid(): ?Nacionalidad
    {
        return $this->nacionalidadid;
    }

    public function setNacionalidadid(?Nacionalidad $nacionalidadid): self
    {
        $this->nacionalidadid = $nacionalidadid;

        return $this;
    }

    public function getSexoid(): ?Sexo
    {
        return $this->sexoid;
    }

    public function setSexoid(?Sexo $sexoid): self
    {
        $this->sexoid = $sexoid;

        return $this;
    }


}
