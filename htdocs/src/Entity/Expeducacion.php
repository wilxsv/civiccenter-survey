<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Expeducacion
 *
 * @ORM\Table(name="ExpEducacion", indexes={@ORM\Index(name="IEXPEDUCACION1", columns={"ExpedienteNumero"}), @ORM\Index(name="IEXPEDUCACION2", columns={"CentroEducId"})})
 * @ORM\Entity
 */
class Expeducacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="ExpEducacionID", type="integer", nullable=false, options={"comment"="Llave compuesta para relacionar y diferenciar la información para cada expediente."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $expeducacionid;

    /**
     * @var int
     *
     * @ORM\Column(name="ExpEducacionNivelEduc", type="integer", nullable=false, options={"comment"="Guarda el Nivel Educativo que la persona ha registrado."})
     */
    private $expeducacionniveleduc;

    /**
     * @var string
     *
     * @ORM\Column(name="ExpEducacionOtroNivel", type="string", length=50, nullable=false, options={"comment"="Mantiene la información de otro nivel de educación que no se tenga registrado."})
     */
    private $expeducacionotronivel;

    /**
     * @var int
     *
     * @ORM\Column(name="ExpEducacionActual", type="integer", nullable=false, options={"comment"="Conserva la Respuesta que brindo la persona si estudia actualmente."})
     */
    private $expeducacionactual;

    /**
     * @var string
     *
     * @ORM\Column(name="ExpEducacionEstudio", type="string", length=40, nullable=false, options={"comment"="Preserva la información de lo que esta estudiando la persona actualmente."})
     */
    private $expeducacionestudio;

    /**
     * @var string
     *
     * @ORM\Column(name="ExpEducacionLugarEst", type="string", length=100, nullable=false, options={"comment"="Registra el Lugar donde se encuentra el centro educativo donde estudia la persona."})
     */
    private $expeducacionlugarest;

    /**
     * @var string
     *
     * @ORM\Column(name="ExpEducacionProbEst", type="string", length=100, nullable=false, options={"comment"="Mantiene la información si la persona tiene algún problema para estudiar."})
     */
    private $expeducacionprobest;

    /**
     * @var string
     *
     * @ORM\Column(name="ExpEducacionMotivo", type="string", length=255, nullable=false, options={"comment"="Conserva la justificación del motivo del porque no estudia."})
     */
    private $expeducacionmotivo;

    /**
     * @var int
     *
     * @ORM\Column(name="ExpEducacionVolverEst", type="integer", nullable=false, options={"comment"="Preserva la respuesta de la persona si desea volver a estudiar."})
     */
    private $expeducacionvolverest;

    /**
     * @var string
     *
     * @ORM\Column(name="ExpEducacionVolverEstObs", type="string", length=255, nullable=false, options={"comment"="Guarda las observaciones acerca del estudio."})
     */
    private $expeducacionvolverestobs;

    /**
     * @var string
     *
     * @ORM\Column(name="ExpEducacionNecesidad", type="string", length=255, nullable=false, options={"comment"="Almacena cuales son las necesidades que tiene para volver a estudiar."})
     */
    private $expeducacionnecesidad;

    /**
     * @var int
     *
     * @ORM\Column(name="ExpEducacionConcluirEst", type="integer", nullable=false, options={"comment"="Registra la respuesta acerca si desea concluir los estudios."})
     */
    private $expeducacionconcluirest;

    /**
     * @var string
     *
     * @ORM\Column(name="ExpEducacionUltAnno", type="string", length=40, nullable=false, options={"comment"="Mantiene la respuesta del último año aprobado de la persona que se encuentra dentro del Centro Cívico."})
     */
    private $expeducacionultanno;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ExpEducacionFecCrea", type="datetime", nullable=false, options={"comment"="Conserva la fecha en la cual se realizó el registro de la información."})
     */
    private $expeducacionfeccrea;

    /**
     * @var string
     *
     * @ORM\Column(name="ExpEducacionUsrCrea", type="string", length=255, nullable=false, options={"comment"="Preserva el usuario que creó el regsitro de estudios."})
     */
    private $expeducacionusrcrea;

    /**
     * @var \Centroeduc
     *
     * @ORM\ManyToOne(targetEntity="Centroeduc")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CentroEducId", referencedColumnName="CentroEducId")
     * })
     */
    private $centroeducid;

    /**
     * @var \Expediente
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Expediente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ExpedienteNumero", referencedColumnName="ExpedienteNumero")
     * })
     */
    private $expedientenumero;

    public function getExpeducacionid(): ?int
    {
        return $this->expeducacionid;
    }

    public function getExpeducacionniveleduc(): ?int
    {
        return $this->expeducacionniveleduc;
    }

    public function setExpeducacionniveleduc(int $expeducacionniveleduc): self
    {
        $this->expeducacionniveleduc = $expeducacionniveleduc;

        return $this;
    }

    public function getExpeducacionotronivel(): ?string
    {
        return $this->expeducacionotronivel;
    }

    public function setExpeducacionotronivel(string $expeducacionotronivel): self
    {
        $this->expeducacionotronivel = $expeducacionotronivel;

        return $this;
    }

    public function getExpeducacionactual(): ?int
    {
        return $this->expeducacionactual;
    }

    public function setExpeducacionactual(int $expeducacionactual): self
    {
        $this->expeducacionactual = $expeducacionactual;

        return $this;
    }

    public function getExpeducacionestudio(): ?string
    {
        return $this->expeducacionestudio;
    }

    public function setExpeducacionestudio(string $expeducacionestudio): self
    {
        $this->expeducacionestudio = $expeducacionestudio;

        return $this;
    }

    public function getExpeducacionlugarest(): ?string
    {
        return $this->expeducacionlugarest;
    }

    public function setExpeducacionlugarest(string $expeducacionlugarest): self
    {
        $this->expeducacionlugarest = $expeducacionlugarest;

        return $this;
    }

    public function getExpeducacionprobest(): ?string
    {
        return $this->expeducacionprobest;
    }

    public function setExpeducacionprobest(string $expeducacionprobest): self
    {
        $this->expeducacionprobest = $expeducacionprobest;

        return $this;
    }

    public function getExpeducacionmotivo(): ?string
    {
        return $this->expeducacionmotivo;
    }

    public function setExpeducacionmotivo(string $expeducacionmotivo): self
    {
        $this->expeducacionmotivo = $expeducacionmotivo;

        return $this;
    }

    public function getExpeducacionvolverest(): ?int
    {
        return $this->expeducacionvolverest;
    }

    public function setExpeducacionvolverest(int $expeducacionvolverest): self
    {
        $this->expeducacionvolverest = $expeducacionvolverest;

        return $this;
    }

    public function getExpeducacionvolverestobs(): ?string
    {
        return $this->expeducacionvolverestobs;
    }

    public function setExpeducacionvolverestobs(string $expeducacionvolverestobs): self
    {
        $this->expeducacionvolverestobs = $expeducacionvolverestobs;

        return $this;
    }

    public function getExpeducacionnecesidad(): ?string
    {
        return $this->expeducacionnecesidad;
    }

    public function setExpeducacionnecesidad(string $expeducacionnecesidad): self
    {
        $this->expeducacionnecesidad = $expeducacionnecesidad;

        return $this;
    }

    public function getExpeducacionconcluirest(): ?int
    {
        return $this->expeducacionconcluirest;
    }

    public function setExpeducacionconcluirest(int $expeducacionconcluirest): self
    {
        $this->expeducacionconcluirest = $expeducacionconcluirest;

        return $this;
    }

    public function getExpeducacionultanno(): ?string
    {
        return $this->expeducacionultanno;
    }

    public function setExpeducacionultanno(string $expeducacionultanno): self
    {
        $this->expeducacionultanno = $expeducacionultanno;

        return $this;
    }

    public function getExpeducacionfeccrea(): ?\DateTimeInterface
    {
        return $this->expeducacionfeccrea;
    }

    public function setExpeducacionfeccrea(\DateTimeInterface $expeducacionfeccrea): self
    {
        $this->expeducacionfeccrea = $expeducacionfeccrea;

        return $this;
    }

    public function getExpeducacionusrcrea(): ?string
    {
        return $this->expeducacionusrcrea;
    }

    public function setExpeducacionusrcrea(string $expeducacionusrcrea): self
    {
        $this->expeducacionusrcrea = $expeducacionusrcrea;

        return $this;
    }

    public function getCentroeducid(): ?Centroeduc
    {
        return $this->centroeducid;
    }

    public function setCentroeducid(?Centroeduc $centroeducid): self
    {
        $this->centroeducid = $centroeducid;

        return $this;
    }

    public function getExpedientenumero(): ?Expediente
    {
        return $this->expedientenumero;
    }

    public function setExpedientenumero(?Expediente $expedientenumero): self
    {
        $this->expedientenumero = $expedientenumero;

        return $this;
    }


}
