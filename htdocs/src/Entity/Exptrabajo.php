<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Exptrabajo
 *
 * @ORM\Table(name="ExpTrabajo", indexes={@ORM\Index(name="IEXPTRABAJO1", columns={"ExpedienteNumero"}), @ORM\Index(name="IEXPTRABAJO2", columns={"AreaTrabajoID"}), @ORM\Index(name="IEXPTRABAJO3", columns={"JornadaTrabajoID"})})
 * @ORM\Entity
 */
class Exptrabajo
{
    /**
     * @var int
     *
     * @ORM\Column(name="ExpTrabajoID", type="integer", nullable=false, options={"comment"="Llave Compuesta formada por el ID y el Número de Expediente."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $exptrabajoid;

    /**
     * @var int
     *
     * @ORM\Column(name="ExpTrabajoSiNo", type="integer", nullable=false, options={"comment"="Guarda la respuesta de la persona si trabaja o no."})
     */
    private $exptrabajosino;

    /**
     * @var string
     *
     * @ORM\Column(name="ExpTrabajoAreaTrabOtro", type="string", length=50, nullable=false, options={"comment"="Conserva los datos de otra área de trabajo en la que se desenvuelve la persona."})
     */
    private $exptrabajoareatrabotro;

    /**
     * @var int
     *
     * @ORM\Column(name="ExpTrabajoDiurno", type="integer", nullable=false, options={"comment"="Guarda la Respuesta de la persona si el trabajo es Diurno."})
     */
    private $exptrabajodiurno;

    /**
     * @var string
     *
     * @ORM\Column(name="ExpTrabajoLugar", type="string", length=50, nullable=false, options={"comment"="Registra la información del lugar donde trabaja la persona."})
     */
    private $exptrabajolugar;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ExpTrabajoFecCrea", type="datetime", nullable=false, options={"comment"="Almacena la Fecha en que se creó el registro."})
     */
    private $exptrabajofeccrea;

    /**
     * @var string
     *
     * @ORM\Column(name="ExpTrabajoUsrCrea", type="string", length=255, nullable=false, options={"comment"="Mantiene el dato del usuario que creó el registro"})
     */
    private $exptrabajousrcrea;

    /**
     * @var \Areatrabajo
     *
     * @ORM\ManyToOne(targetEntity="Areatrabajo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="AreaTrabajoID", referencedColumnName="AreaTrabajoID")
     * })
     */
    private $areatrabajoid;

    /**
     * @var \Expediente
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Expediente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ExpedienteNumero", referencedColumnName="ExpedienteNumero")
     * })
     */
    private $expedientenumero;

    /**
     * @var \Jornadatrabajo
     *
     * @ORM\ManyToOne(targetEntity="Jornadatrabajo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="JornadaTrabajoID", referencedColumnName="JornadaTrabajoID")
     * })
     */
    private $jornadatrabajoid;

    public function getExptrabajoid(): ?int
    {
        return $this->exptrabajoid;
    }

    public function getExptrabajosino(): ?int
    {
        return $this->exptrabajosino;
    }

    public function setExptrabajosino(int $exptrabajosino): self
    {
        $this->exptrabajosino = $exptrabajosino;

        return $this;
    }

    public function getExptrabajoareatrabotro(): ?string
    {
        return $this->exptrabajoareatrabotro;
    }

    public function setExptrabajoareatrabotro(string $exptrabajoareatrabotro): self
    {
        $this->exptrabajoareatrabotro = $exptrabajoareatrabotro;

        return $this;
    }

    public function getExptrabajodiurno(): ?int
    {
        return $this->exptrabajodiurno;
    }

    public function setExptrabajodiurno(int $exptrabajodiurno): self
    {
        $this->exptrabajodiurno = $exptrabajodiurno;

        return $this;
    }

    public function getExptrabajolugar(): ?string
    {
        return $this->exptrabajolugar;
    }

    public function setExptrabajolugar(string $exptrabajolugar): self
    {
        $this->exptrabajolugar = $exptrabajolugar;

        return $this;
    }

    public function getExptrabajofeccrea(): ?\DateTimeInterface
    {
        return $this->exptrabajofeccrea;
    }

    public function setExptrabajofeccrea(\DateTimeInterface $exptrabajofeccrea): self
    {
        $this->exptrabajofeccrea = $exptrabajofeccrea;

        return $this;
    }

    public function getExptrabajousrcrea(): ?string
    {
        return $this->exptrabajousrcrea;
    }

    public function setExptrabajousrcrea(string $exptrabajousrcrea): self
    {
        $this->exptrabajousrcrea = $exptrabajousrcrea;

        return $this;
    }

    public function getAreatrabajoid(): ?Areatrabajo
    {
        return $this->areatrabajoid;
    }

    public function setAreatrabajoid(?Areatrabajo $areatrabajoid): self
    {
        $this->areatrabajoid = $areatrabajoid;

        return $this;
    }

    public function getExpedientenumero(): ?Expediente
    {
        return $this->expedientenumero;
    }

    public function setExpedientenumero(?Expediente $expedientenumero): self
    {
        $this->expedientenumero = $expedientenumero;

        return $this;
    }

    public function getJornadatrabajoid(): ?Jornadatrabajo
    {
        return $this->jornadatrabajoid;
    }

    public function setJornadatrabajoid(?Jornadatrabajo $jornadatrabajoid): self
    {
        $this->jornadatrabajoid = $jornadatrabajoid;

        return $this;
    }


}
