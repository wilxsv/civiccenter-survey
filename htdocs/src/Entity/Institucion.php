<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Institucion
 *
 * @ORM\Table(name="Institucion")
 * @ORM\Entity
 */
class Institucion
{
    /**
     * @var int
     *
     * @ORM\Column(name="InstitucionID", type="integer", nullable=false, options={"comment"="Llave primaria para identificar cada registro"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $institucionid;

    /**
     * @var string
     *
     * @ORM\Column(name="InstitucionDescrip", type="string", length=300, nullable=false, options={"comment"="Registra el Nombre de cada una de las Instituciones."})
     */
    private $instituciondescrip;

    /**
     * @var string
     *
     * @ORM\Column(name="InstitucionSiglas", type="string", length=200, nullable=false, options={"comment"="Almacena las Siglas de cada Institución."})
     */
    private $institucionsiglas;

    public function getInstitucionid(): ?int
    {
        return $this->institucionid;
    }

    public function getInstituciondescrip(): ?string
    {
        return $this->instituciondescrip;
    }

    public function setInstituciondescrip(string $instituciondescrip): self
    {
        $this->instituciondescrip = $instituciondescrip;

        return $this;
    }

    public function getInstitucionsiglas(): ?string
    {
        return $this->institucionsiglas;
    }

    public function setInstitucionsiglas(string $institucionsiglas): self
    {
        $this->institucionsiglas = $institucionsiglas;

        return $this;
    }


}
