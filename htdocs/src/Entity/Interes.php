<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Interes
 *
 * @ORM\Table(name="Interes", indexes={@ORM\Index(name="IINTERES1", columns={"AreaInteresID"}), @ORM\Index(name="IINTERES2", columns={"ExpedienteNumero"})})
 * @ORM\Entity
 */
class Interes
{
    /**
     * @var int
     *
     * @ORM\Column(name="InteresID", type="integer", nullable=false, options={"comment"="InteresID se utiliza como llave Primaria"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $interesid;

    /**
     * @var string
     *
     * @ORM\Column(name="InteresTema", type="string", length=50, nullable=false, options={"comment"="Almacena el tema que la persona desearía que se brindara en el Centro Cívico."})
     */
    private $interestema;

    /**
     * @var int
     *
     * @ORM\Column(name="InteresPrioridad", type="smallint", nullable=false, options={"comment"="Mantiene la información de la prioridad que la persona le brindó al tema en específico, que puede ser de 1 a 3."})
     */
    private $interesprioridad;

    /**
     * @var bool
     *
     * @ORM\Column(name="InteresActivo", type="boolean", nullable=false, options={"comment"="Preserva el estado de cada uno de los intereses seleccionados."})
     */
    private $interesactivo;

    /**
     * @var string
     *
     * @ORM\Column(name="InteresUsrCrea", type="string", length=40, nullable=false, options={"comment"="Contiene el usuario que creo el registro."})
     */
    private $interesusrcrea;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="InteresFecCrea", type="datetime", nullable=false, options={"comment"="Guarda la fecha en la que se creó el registro."})
     */
    private $interesfeccrea;

    /**
     * @var string|null
     *
     * @ORM\Column(name="InteresUsrMod", type="string", length=40, nullable=true, options={"comment"="Almacena el usuario que modifica los registros."})
     */
    private $interesusrmod;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="InteresFecMod", type="datetime", nullable=true, options={"comment"="Registra la fecha en las que se realizaron modificaciones."})
     */
    private $interesfecmod;

    /**
     * @var \Areainteres
     *
     * @ORM\ManyToOne(targetEntity="Areainteres")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="AreaInteresID", referencedColumnName="AreaInteresID")
     * })
     */
    private $areainteresid;

    /**
     * @var \Expediente
     *
     * @ORM\ManyToOne(targetEntity="Expediente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ExpedienteNumero", referencedColumnName="ExpedienteNumero")
     * })
     */
    private $expedientenumero;

    public function getInteresid(): ?int
    {
        return $this->interesid;
    }

    public function getInterestema(): ?string
    {
        return $this->interestema;
    }

    public function setInterestema(string $interestema): self
    {
        $this->interestema = $interestema;

        return $this;
    }

    public function getInteresprioridad(): ?int
    {
        return $this->interesprioridad;
    }

    public function setInteresprioridad(int $interesprioridad): self
    {
        $this->interesprioridad = $interesprioridad;

        return $this;
    }

    public function getInteresactivo(): ?bool
    {
        return $this->interesactivo;
    }

    public function setInteresactivo(bool $interesactivo): self
    {
        $this->interesactivo = $interesactivo;

        return $this;
    }

    public function getInteresusrcrea(): ?string
    {
        return $this->interesusrcrea;
    }

    public function setInteresusrcrea(string $interesusrcrea): self
    {
        $this->interesusrcrea = $interesusrcrea;

        return $this;
    }

    public function getInteresfeccrea(): ?\DateTimeInterface
    {
        return $this->interesfeccrea;
    }

    public function setInteresfeccrea(\DateTimeInterface $interesfeccrea): self
    {
        $this->interesfeccrea = $interesfeccrea;

        return $this;
    }

    public function getInteresusrmod(): ?string
    {
        return $this->interesusrmod;
    }

    public function setInteresusrmod(?string $interesusrmod): self
    {
        $this->interesusrmod = $interesusrmod;

        return $this;
    }

    public function getInteresfecmod(): ?\DateTimeInterface
    {
        return $this->interesfecmod;
    }

    public function setInteresfecmod(?\DateTimeInterface $interesfecmod): self
    {
        $this->interesfecmod = $interesfecmod;

        return $this;
    }

    public function getAreainteresid(): ?Areainteres
    {
        return $this->areainteresid;
    }

    public function setAreainteresid(?Areainteres $areainteresid): self
    {
        $this->areainteresid = $areainteresid;

        return $this;
    }

    public function getExpedientenumero(): ?Expediente
    {
        return $this->expedientenumero;
    }

    public function setExpedientenumero(?Expediente $expedientenumero): self
    {
        $this->expedientenumero = $expedientenumero;

        return $this;
    }


}
