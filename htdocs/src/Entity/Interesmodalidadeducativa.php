<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Interesmodalidadeducativa
 *
 * @ORM\Table(name="InteresModalidadEducativa", uniqueConstraints={@ORM\UniqueConstraint(name="UN_IModalidadEducativaDescripcion", columns={"IModalidadEducativaDescripcion"})})
 * @ORM\Entity
 */
class Interesmodalidadeducativa
{
    /**
     * @var int
     *
     * @ORM\Column(name="IModalidadEducativaID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $imodalidadeducativaid;

    /**
     * @var string
     *
     * @ORM\Column(name="IModalidadEducativaDescripcion", type="string", length=120, nullable=false)
     */
    private $imodalidadeducativadescripcion;

    public function getImodalidadeducativaid(): ?int
    {
        return $this->imodalidadeducativaid;
    }

    public function getImodalidadeducativadescripcion(): ?string
    {
        return $this->imodalidadeducativadescripcion;
    }

    public function setImodalidadeducativadescripcion(string $imodalidadeducativadescripcion): self
    {
        $this->imodalidadeducativadescripcion = $imodalidadeducativadescripcion;

        return $this;
    }


}
