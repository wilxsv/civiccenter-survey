<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Jornadatrabajo
 *
 * @ORM\Table(name="JornadaTrabajo")
 * @ORM\Entity
 */
class Jornadatrabajo
{
    /**
     * @var int
     *
     * @ORM\Column(name="JornadaTrabajoID", type="integer", nullable=false, options={"comment"="Llave primaria para la Jornada de Trabajo por medio del campo JornadaTrabajoID"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $jornadatrabajoid;

    /**
     * @var string
     *
     * @ORM\Column(name="JornadaTrabajoDescripcion", type="string", length=50, nullable=false, options={"comment"="Registra la descripción de cada Jornada de Trabajo."})
     */
    private $jornadatrabajodescripcion;

    public function getJornadatrabajoid(): ?int
    {
        return $this->jornadatrabajoid;
    }

    public function getJornadatrabajodescripcion(): ?string
    {
        return $this->jornadatrabajodescripcion;
    }

    public function setJornadatrabajodescripcion(string $jornadatrabajodescripcion): self
    {
        $this->jornadatrabajodescripcion = $jornadatrabajodescripcion;

        return $this;
    }


}
