<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Nacionalidad
 *
 * @ORM\Table(name="Nacionalidad")
 * @ORM\Entity
 */
class Nacionalidad
{
    /**
     * @var int
     *
     * @ORM\Column(name="NacionalidadID", type="integer", nullable=false, options={"comment"="Llave Primaria utilizando la NacionalidadID"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $nacionalidadid;

    /**
     * @var string
     *
     * @ORM\Column(name="NacionalidadDescripcion", type="string", length=50, nullable=false, options={"comment"="Almacena la descripción de la Nacionalidad."})
     */
    private $nacionalidaddescripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="NacionalidadCodigo", type="string", length=5, nullable=false, options={"comment"="Registra el Código de cada Nacionalidad."})
     */
    private $nacionalidadcodigo;

    public function getNacionalidadid(): ?int
    {
        return $this->nacionalidadid;
    }

    public function getNacionalidaddescripcion(): ?string
    {
        return $this->nacionalidaddescripcion;
    }

    public function setNacionalidaddescripcion(string $nacionalidaddescripcion): self
    {
        $this->nacionalidaddescripcion = $nacionalidaddescripcion;

        return $this;
    }

    public function getNacionalidadcodigo(): ?string
    {
        return $this->nacionalidadcodigo;
    }

    public function setNacionalidadcodigo(string $nacionalidadcodigo): self
    {
        $this->nacionalidadcodigo = $nacionalidadcodigo;

        return $this;
    }


}
