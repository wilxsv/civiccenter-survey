<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Necesitavolveraestudiar
 *
 * @ORM\Table(name="NecesitaVolverAEstudiar", uniqueConstraints={@ORM\UniqueConstraint(name="UN_NecesitaVolverAEstudiarDescripcion", columns={"NecesitaVolverAEstudiarDescripcion"})})
 * @ORM\Entity
 */
class Necesitavolveraestudiar
{
    /**
     * @var int
     *
     * @ORM\Column(name="NecesitaVolverAEstudiarID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $necesitavolveraestudiarid;

    /**
     * @var string
     *
     * @ORM\Column(name="NecesitaVolverAEstudiarDescripcion", type="string", length=120, nullable=false)
     */
    private $necesitavolveraestudiardescripcion;

    public function getNecesitavolveraestudiarid(): ?int
    {
        return $this->necesitavolveraestudiarid;
    }

    public function getNecesitavolveraestudiardescripcion(): ?string
    {
        return $this->necesitavolveraestudiardescripcion;
    }

    public function setNecesitavolveraestudiardescripcion(string $necesitavolveraestudiardescripcion): self
    {
        $this->necesitavolveraestudiardescripcion = $necesitavolveraestudiardescripcion;

        return $this;
    }


}
