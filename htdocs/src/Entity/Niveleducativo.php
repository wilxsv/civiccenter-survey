<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Niveleducativo
 *
 * @ORM\Table(name="NivelEducativo")
 * @ORM\Entity
 */
class Niveleducativo
{
    /**
     * @var int
     *
     * @ORM\Column(name="NivelEducativoID", type="integer", nullable=false, options={"comment"="Llave Primaria donde se utiliza el campo NivelEducativoID."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $niveleducativoid;

    /**
     * @var string
     *
     * @ORM\Column(name="NivelEducativoDescripcion", type="string", length=40, nullable=false, options={"comment"="Almacena la descripción del Nivel Educativo."})
     */
    private $niveleducativodescripcion;

    public function getNiveleducativoid(): ?int
    {
        return $this->niveleducativoid;
    }

    public function getNiveleducativodescripcion(): ?string
    {
        return $this->niveleducativodescripcion;
    }

    public function setNiveleducativodescripcion(string $niveleducativodescripcion): self
    {
        $this->niveleducativodescripcion = $niveleducativodescripcion;

        return $this;
    }


}
