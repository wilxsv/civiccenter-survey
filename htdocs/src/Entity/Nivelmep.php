<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Nivelmep
 *
 * @ORM\Table(name="NivelMEP")
 * @ORM\Entity
 */
class Nivelmep
{
    /**
     * @var int
     *
     * @ORM\Column(name="NivelMEPId", type="integer", nullable=false, options={"comment"="Llave Primaria que se identifica con el campo NivelMEPId."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $nivelmepid;

    /**
     * @var string
     *
     * @ORM\Column(name="NivelMEPDescripcion", type="string", length=300, nullable=false, options={"comment"="Registra la información de la descripción del Nivel Educativo del MEP."})
     */
    private $nivelmepdescripcion;

    public function getNivelmepid(): ?int
    {
        return $this->nivelmepid;
    }

    public function getNivelmepdescripcion(): ?string
    {
        return $this->nivelmepdescripcion;
    }

    public function setNivelmepdescripcion(string $nivelmepdescripcion): self
    {
        $this->nivelmepdescripcion = $nivelmepdescripcion;

        return $this;
    }


}
