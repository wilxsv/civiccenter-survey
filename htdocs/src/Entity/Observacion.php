<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Observacion
 *
 * @ORM\Table(name="Observacion", indexes={@ORM\Index(name="IOBSERVACION1", columns={"ExpedienteNumero"})})
 * @ORM\Entity
 */
class Observacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="ObservacionNum", type="integer", nullable=false, options={"comment"="Llave Primaria compuesta por ObservacionNum y ExpedienteNumero."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $observacionnum;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ObservacionFecha", type="datetime", nullable=false, options={"comment"="Guarda la fecha en que se hizo el registro de la observación."})
     */
    private $observacionfecha;

    /**
     * @var string
     *
     * @ORM\Column(name="ObservacionTexto", type="string", length=255, nullable=false, options={"comment"="Mantiene la Observación registrada por el personal de los Centros Cívicos."})
     */
    private $observaciontexto;

    /**
     * @var string
     *
     * @ORM\Column(name="ObservacionUsuarioNombre", type="string", length=255, nullable=false, options={"comment"="Conserva el nombre del funcionario que realiza el registro de la información."})
     */
    private $observacionusuarionombre;

    /**
     * @var \Expediente
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Expediente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ExpedienteNumero", referencedColumnName="ExpedienteNumero")
     * })
     */
    private $expedientenumero;

    public function getObservacionnum(): ?int
    {
        return $this->observacionnum;
    }

    public function getObservacionfecha(): ?\DateTimeInterface
    {
        return $this->observacionfecha;
    }

    public function setObservacionfecha(\DateTimeInterface $observacionfecha): self
    {
        $this->observacionfecha = $observacionfecha;

        return $this;
    }

    public function getObservaciontexto(): ?string
    {
        return $this->observaciontexto;
    }

    public function setObservaciontexto(string $observaciontexto): self
    {
        $this->observaciontexto = $observaciontexto;

        return $this;
    }

    public function getObservacionusuarionombre(): ?string
    {
        return $this->observacionusuarionombre;
    }

    public function setObservacionusuarionombre(string $observacionusuarionombre): self
    {
        $this->observacionusuarionombre = $observacionusuarionombre;

        return $this;
    }

    public function getExpedientenumero(): ?Expediente
    {
        return $this->expedientenumero;
    }

    public function setExpedientenumero(?Expediente $expedientenumero): self
    {
        $this->expedientenumero = $expedientenumero;

        return $this;
    }


}
