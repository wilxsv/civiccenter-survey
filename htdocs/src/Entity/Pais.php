<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pais
 *
 * @ORM\Table(name="pais")
 * @ORM\Entity
 */
class Pais
{
    /**
     * @var int
     *
     * @ORM\Column(name="paisCodigo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $paiscodigo;

    /**
     * @var string
     *
     * @ORM\Column(name="paisDescripcion", type="string", length=300, nullable=false)
     */
    private $paisdescripcion;

    /**
     * @var bool
     *
     * @ORM\Column(name="paisActivo", type="boolean", nullable=false)
     */
    private $paisactivo;

    public function getPaiscodigo(): ?int
    {
        return $this->paiscodigo;
    }

    public function getPaisdescripcion(): ?string
    {
        return $this->paisdescripcion;
    }

    public function setPaisdescripcion(string $paisdescripcion): self
    {
        $this->paisdescripcion = $paisdescripcion;

        return $this;
    }

    public function getPaisactivo(): ?bool
    {
        return $this->paisactivo;
    }

    public function setPaisactivo(bool $paisactivo): self
    {
        $this->paisactivo = $paisactivo;

        return $this;
    }


}
