<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Pregunta
 *
 * @ORM\Table(name="Pregunta")
 * @ORM\Entity
 */
class Pregunta
{
    /**
     * @var int
     *
     * @ORM\Column(name="PreguntaID", type="integer", nullable=false, options={"comment"="Llave Primaria establecida por el campo PreguntaID."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $preguntaid;

    /**
     * @var string
     *
     * @ORM\Column(name="PreguntaDescripcion", type="string", length=300, nullable=false, options={"comment"="Registra la descripción de cada categoria de pregunta."})
     */
    private $preguntadescripcion;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Respuesta", inversedBy="preguntaid")
     * @ORM\JoinTable(name="preguntarespuesta",
     *   joinColumns={
     *     @ORM\JoinColumn(name="PreguntaID", referencedColumnName="PreguntaID")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="RespuestaID", referencedColumnName="RespuestaID")
     *   }
     * )
     */
    private $respuestaid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->respuestaid = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getPreguntaid(): ?int
    {
        return $this->preguntaid;
    }

    public function getPreguntadescripcion(): ?string
    {
        return $this->preguntadescripcion;
    }

    public function setPreguntadescripcion(string $preguntadescripcion): self
    {
        $this->preguntadescripcion = $preguntadescripcion;

        return $this;
    }

    /**
     * @return Collection|Respuesta[]
     */
    public function getRespuestaid(): Collection
    {
        return $this->respuestaid;
    }

    public function addRespuestaid(Respuesta $respuestaid): self
    {
        if (!$this->respuestaid->contains($respuestaid)) {
            $this->respuestaid[] = $respuestaid;
        }

        return $this;
    }

    public function removeRespuestaid(Respuesta $respuestaid): self
    {
        if ($this->respuestaid->contains($respuestaid)) {
            $this->respuestaid->removeElement($respuestaid);
        }

        return $this;
    }

}
