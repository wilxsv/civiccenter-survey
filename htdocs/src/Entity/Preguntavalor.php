<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Preguntavalor
 *
 * @ORM\Table(name="PreguntaValor", indexes={@ORM\Index(name="IDX_5187A80ED27E6B9E", columns={"PreguntaID"})})
 * @ORM\Entity
 */
class Preguntavalor
{
    /**
     * @var string
     *
     * @ORM\Column(name="PreguntaValor", type="string", length=3, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $preguntavalor;

    /**
     * @var string
     *
     * @ORM\Column(name="PreguntaValorDescripcion", type="string", length=300, nullable=false)
     */
    private $preguntavalordescripcion;

    /**
     * @var \Pregunta
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Pregunta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PreguntaID", referencedColumnName="PreguntaID")
     * })
     */
    private $preguntaid;

    public function getPreguntavalor(): ?string
    {
        return $this->preguntavalor;
    }

    public function getPreguntavalordescripcion(): ?string
    {
        return $this->preguntavalordescripcion;
    }

    public function setPreguntavalordescripcion(string $preguntavalordescripcion): self
    {
        $this->preguntavalordescripcion = $preguntavalordescripcion;

        return $this;
    }

    public function getPreguntaid(): ?Pregunta
    {
        return $this->preguntaid;
    }

    public function setPreguntaid(?Pregunta $preguntaid): self
    {
        $this->preguntaid = $preguntaid;

        return $this;
    }


}
