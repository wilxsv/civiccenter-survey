<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Procesoformativo
 *
 * @ORM\Table(name="ProcesoFormativo", indexes={@ORM\Index(name="IPROCESOFORMATIVO1", columns={"ExpedienteNumero"})})
 * @ORM\Entity
 */
class Procesoformativo
{
    /**
     * @var int
     *
     * @ORM\Column(name="ProcesoFormativoNum", type="integer", nullable=false, options={"comment"="Llave Primaria compuesta por el Número de Proceso Formativo y el Número de Expediente."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $procesoformativonum;

    /**
     * @var string
     *
     * @ORM\Column(name="ProcesoFormativoNombre", type="string", length=100, nullable=false, options={"comment"="Registra el Nombre del Proceso Formativo en el cual participa."})
     */
    private $procesoformativonombre;

    /**
     * @var string
     *
     * @ORM\Column(name="ProcesoFormativoCodigoCurso", type="string", length=40, nullable=false, options={"comment"="Guarda el Código del Proceso Formativo."})
     */
    private $procesoformativocodigocurso;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="ProcesoFormativoFecInscripcion", type="datetime", nullable=true, options={"comment"="Mantiene la Fecha de Incio de un Proceso Formativo."})
     */
    private $procesoformativofecinscripcion;

    /**
     * @var int
     *
     * @ORM\Column(name="ProcesoFormativoDuracion", type="smallint", nullable=false, options={"comment"="Preserva la información de la duración del proceso formativo en semanas."})
     */
    private $procesoformativoduracion;

    /**
     * @var string
     *
     * @ORM\Column(name="ProcesoFormativoUsrCrea", type="string", length=255, nullable=false, options={"comment"="Conserva el registro del usuario que crea el registro."})
     */
    private $procesoformativousrcrea;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ProcesoFormativoFecCrea", type="datetime", nullable=false, options={"comment"="Registra la fecha en la cual se hizo el registro de los datos."})
     */
    private $procesoformativofeccrea;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ProcesoFormativoUsrMod", type="string", length=255, nullable=true, options={"comment"="Guarda el usuario que realiza modificaciones en el sistema."})
     */
    private $procesoformativousrmod;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="ProcesoFormativoFecMod", type="datetime", nullable=true, options={"comment"="Almacena la fecha en la cual se realiza la modificación."})
     */
    private $procesoformativofecmod;

    /**
     * @var int
     *
     * @ORM\Column(name="ProcesoFormativoEstadoProceso", type="integer", nullable=false, options={"comment"="Mantiene el estado en que se encuentra el Proceso Formativo."})
     */
    private $procesoformativoestadoproceso;

    /**
     * @var string
     *
     * @ORM\Column(name="ProcesoFormativoInstitucion", type="string", length=255, nullable=false, options={"comment"="Contiene la información de la Institución que brindó la capacitación."})
     */
    private $procesoformativoinstitucion;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="ProcesoFormativoFecInscripFina", type="datetime", nullable=true, options={"comment"="Conserca la fecha en que finaliza el Proceso Formativo."})
     */
    private $procesoformativofecinscripfina;

    /**
     * @var \Expediente
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Expediente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ExpedienteNumero", referencedColumnName="ExpedienteNumero")
     * })
     */
    private $expedientenumero;

    public function getProcesoformativonum(): ?int
    {
        return $this->procesoformativonum;
    }

    public function getProcesoformativonombre(): ?string
    {
        return $this->procesoformativonombre;
    }

    public function setProcesoformativonombre(string $procesoformativonombre): self
    {
        $this->procesoformativonombre = $procesoformativonombre;

        return $this;
    }

    public function getProcesoformativocodigocurso(): ?string
    {
        return $this->procesoformativocodigocurso;
    }

    public function setProcesoformativocodigocurso(string $procesoformativocodigocurso): self
    {
        $this->procesoformativocodigocurso = $procesoformativocodigocurso;

        return $this;
    }

    public function getProcesoformativofecinscripcion(): ?\DateTimeInterface
    {
        return $this->procesoformativofecinscripcion;
    }

    public function setProcesoformativofecinscripcion(?\DateTimeInterface $procesoformativofecinscripcion): self
    {
        $this->procesoformativofecinscripcion = $procesoformativofecinscripcion;

        return $this;
    }

    public function getProcesoformativoduracion(): ?int
    {
        return $this->procesoformativoduracion;
    }

    public function setProcesoformativoduracion(int $procesoformativoduracion): self
    {
        $this->procesoformativoduracion = $procesoformativoduracion;

        return $this;
    }

    public function getProcesoformativousrcrea(): ?string
    {
        return $this->procesoformativousrcrea;
    }

    public function setProcesoformativousrcrea(string $procesoformativousrcrea): self
    {
        $this->procesoformativousrcrea = $procesoformativousrcrea;

        return $this;
    }

    public function getProcesoformativofeccrea(): ?\DateTimeInterface
    {
        return $this->procesoformativofeccrea;
    }

    public function setProcesoformativofeccrea(\DateTimeInterface $procesoformativofeccrea): self
    {
        $this->procesoformativofeccrea = $procesoformativofeccrea;

        return $this;
    }

    public function getProcesoformativousrmod(): ?string
    {
        return $this->procesoformativousrmod;
    }

    public function setProcesoformativousrmod(?string $procesoformativousrmod): self
    {
        $this->procesoformativousrmod = $procesoformativousrmod;

        return $this;
    }

    public function getProcesoformativofecmod(): ?\DateTimeInterface
    {
        return $this->procesoformativofecmod;
    }

    public function setProcesoformativofecmod(?\DateTimeInterface $procesoformativofecmod): self
    {
        $this->procesoformativofecmod = $procesoformativofecmod;

        return $this;
    }

    public function getProcesoformativoestadoproceso(): ?int
    {
        return $this->procesoformativoestadoproceso;
    }

    public function setProcesoformativoestadoproceso(int $procesoformativoestadoproceso): self
    {
        $this->procesoformativoestadoproceso = $procesoformativoestadoproceso;

        return $this;
    }

    public function getProcesoformativoinstitucion(): ?string
    {
        return $this->procesoformativoinstitucion;
    }

    public function setProcesoformativoinstitucion(string $procesoformativoinstitucion): self
    {
        $this->procesoformativoinstitucion = $procesoformativoinstitucion;

        return $this;
    }

    public function getProcesoformativofecinscripfina(): ?\DateTimeInterface
    {
        return $this->procesoformativofecinscripfina;
    }

    public function setProcesoformativofecinscripfina(?\DateTimeInterface $procesoformativofecinscripfina): self
    {
        $this->procesoformativofecinscripfina = $procesoformativofecinscripfina;

        return $this;
    }

    public function getExpedientenumero(): ?Expediente
    {
        return $this->expedientenumero;
    }

    public function setExpedientenumero(?Expediente $expedientenumero): self
    {
        $this->expedientenumero = $expedientenumero;

        return $this;
    }


}
