<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Provincia
 *
 * @ORM\Table(name="Provincia")
 * @ORM\Entity
 */
class Provincia
{
    /**
     * @var int
     *
     * @ORM\Column(name="ProvinciaID", type="integer", nullable=false, options={"comment"="Llave Primaria que es representada por el campo ProvinciaID"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $provinciaid;

    /**
     * @var string
     *
     * @ORM\Column(name="ProvinciaDescripcion", type="string", length=300, nullable=false, options={"comment"="Registra la Descripción de cada Provincia."})
     */
    private $provinciadescripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="ProvinciaGeoJson", type="text", length=16, nullable=false, options={"default"="{}"})
     */
    private $provinciageojson = '{}';

    public function getProvinciaid(): ?int
    {
        return $this->provinciaid;
    }

    public function getProvinciadescripcion(): ?string
    {
        return $this->provinciadescripcion;
    }

    public function setProvinciadescripcion(string $provinciadescripcion): self
    {
        $this->provinciadescripcion = $provinciadescripcion;

        return $this;
    }

    public function getProvinciageojson(): ?string
    {
        return $this->provinciageojson;
    }

    public function setProvinciageojson(string $provinciageojson): self
    {
        $this->provinciageojson = $provinciageojson;

        return $this;
    }


}
