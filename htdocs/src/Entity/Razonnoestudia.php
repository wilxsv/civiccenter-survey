<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Razonnoestudia
 *
 * @ORM\Table(name="RazonNoEstudia", uniqueConstraints={@ORM\UniqueConstraint(name="UN_RazonNoEstudiaDescripcion", columns={"RazonNoEstudiaDescripcion"})})
 * @ORM\Entity
 */
class Razonnoestudia
{
    /**
     * @var int
     *
     * @ORM\Column(name="RazonNoEstudiaID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $razonnoestudiaid;

    /**
     * @var string
     *
     * @ORM\Column(name="RazonNoEstudiaDescripcion", type="string", length=120, nullable=false)
     */
    private $razonnoestudiadescripcion;

    public function getRazonnoestudiaid(): ?int
    {
        return $this->razonnoestudiaid;
    }

    public function getRazonnoestudiadescripcion(): ?string
    {
        return $this->razonnoestudiadescripcion;
    }

    public function setRazonnoestudiadescripcion(string $razonnoestudiadescripcion): self
    {
        $this->razonnoestudiadescripcion = $razonnoestudiadescripcion;

        return $this;
    }


}
