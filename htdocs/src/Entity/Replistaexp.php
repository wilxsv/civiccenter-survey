<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Replistaexp
 *
 * @ORM\Table(name="RepListaExp")
 * @ORM\Entity
 */
class Replistaexp
{
    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpID", type="integer", nullable=false, options={"comment"="Número Consecutivo para identificar cada registro generado en la consulta."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $replistaexpid;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpNumExp", type="string", length=10, nullable=false, options={"comment"="Registra el Número de Expediente."})
     */
    private $replistaexpnumexp;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoIdentif", type="string", length=30, nullable=false, options={"comment"="Almacena el tipo de identificación de la persona."})
     */
    private $replistaexptipoidentif;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpIndentif", type="string", length=50, nullable=false, options={"comment"="Guarda el Número de Identificación de la persona."})
     */
    private $replistaexpindentif;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpApel1", type="string", length=50, nullable=false, options={"comment"="Mantiene el Primer Apellido de la Persona."})
     */
    private $replistaexpapel1;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpApel2", type="string", length=50, nullable=false, options={"comment"="Conserva el Segundo Apellido de la Persona."})
     */
    private $replistaexpapel2;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpNombre", type="string", length=50, nullable=false, options={"comment"="Contiene el Nombre de la Persona."})
     */
    private $replistaexpnombre;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpSexoID", type="smallint", nullable=false, options={"comment"="Preserva el identifcador del tipo de género de la persona."})
     */
    private $replistaexpsexoid;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpSexoDesc", type="string", length=50, nullable=false, options={"comment"="Registra la descripción del sexo."})
     */
    private $replistaexpsexodesc;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpFecNac", type="datetime", nullable=false, options={"comment"="Almacena la Fecha de Nacimiento de la Persona."})
     */
    private $replistaexpfecnac;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpTelefono", type="integer", nullable=false, options={"comment"="Guarda el Número de Teléfono de la Persona."})
     */
    private $replistaexptelefono;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpCelular", type="integer", nullable=false, options={"comment"="Mantiene el Número de Celular de la Persona."})
     */
    private $replistaexpcelular;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpCorreo", type="string", length=200, nullable=false, options={"comment"="Contiene el Correo Electrónico de la Persona."})
     */
    private $replistaexpcorreo;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpCentroCivicoID", type="smallint", nullable=false, options={"comment"="Conserva el identifcador del Centro Cívico."})
     */
    private $replistaexpcentrocivicoid;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpCentroCivicoSiglas", type="string", length=3, nullable=false, options={"comment"="Preserva las Siglas del Centro Cívico."})
     */
    private $replistaexpcentrocivicosiglas;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpCentroCivicoDesc", type="string", length=200, nullable=false, options={"comment"="Registra la descripción del Centro Cívico."})
     */
    private $replistaexpcentrocivicodesc;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpProvinciaID", type="smallint", nullable=false, options={"comment"="Almacena el identificador de la Provincia."})
     */
    private $replistaexpprovinciaid;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpProvincia", type="string", length=100, nullable=false, options={"comment"="Guarda la descripción de la Provincia."})
     */
    private $replistaexpprovincia;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpCantonID", type="smallint", nullable=false, options={"comment"="Mantiene el identificador del Cantón."})
     */
    private $replistaexpcantonid;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpCanton", type="string", length=100, nullable=false, options={"comment"="Contiene la descripción del Cantón."})
     */
    private $replistaexpcanton;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpDistritoID", type="smallint", nullable=false, options={"comment"="Conserva el identificador del Distrito."})
     */
    private $replistaexpdistritoid;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpDistrito", type="string", length=100, nullable=false, options={"comment"="Preserva la descripción del Distrito."})
     */
    private $replistaexpdistrito;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpBarrio", type="string", length=100, nullable=false, options={"comment"="Registra el Barrio donde vive la persona."})
     */
    private $replistaexpbarrio;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpOtraSenas", type="string", length=500, nullable=false, options={"comment"="Almacena las señas de la dirección de residencia."})
     */
    private $replistaexpotrasenas;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpEnferCronica", type="integer", nullable=false, options={"comment"="Guarda la información de una enfermedad crónica."})
     */
    private $replistaexpenfercronica;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpEnfermedades", type="string", length=300, nullable=false, options={"comment"="Mantiene la información de enfermedades de la persona."})
     */
    private $replistaexpenfermedades;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpNivelEducNum", type="integer", nullable=false, options={"comment"="Conserva el identificador del Nivel Educativo."})
     */
    private $replistaexpniveleducnum;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpNivelEducDescrip", type="string", length=300, nullable=false, options={"comment"="Contiene la descripción del Nivel Educativo."})
     */
    private $replistaexpniveleducdescrip;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpEducacionActualNum", type="smallint", nullable=false, options={"comment"="Preserva el identificador del Nivel Educativo Actual."})
     */
    private $replistaexpeducacionactualnum;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpEducacionActualDesc", type="string", length=300, nullable=false, options={"comment"="Registra la descripción del Nivel Educativo actual."})
     */
    private $replistaexpeducacionactualdesc;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpNivelEduc2Num", type="smallint", nullable=false, options={"comment"="Almacena un segundo identificador de Nivel Educativo Registrado."})
     */
    private $replistaexpniveleduc2num;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpNivelEduc2Descrip", type="string", length=300, nullable=false, options={"comment"="Guarda una segunda descripción de Nivel Educativo Registrado."})
     */
    private $replistaexpniveleduc2descrip;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpEducacionActual2Num", type="smallint", nullable=false, options={"comment"="Mantiene el identifcador del segundo Nivel Estudio Actual."})
     */
    private $replistaexpeducacionactual2num;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpEducacionActual2Des", type="string", length=300, nullable=false, options={"comment"="Conserva la Descripción del Segundo Nivel educativo Actual."})
     */
    private $replistaexpeducacionactual2des;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpCentroEducId", type="smallint", nullable=false, options={"comment"="Contiene el identificador del Centro Educativo"})
     */
    private $replistaexpcentroeducid;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpCentroEducNombre", type="string", length=150, nullable=false, options={"comment"="Preserva el Nombre del Centro Educativo."})
     */
    private $replistaexpcentroeducnombre;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpTrabajoSiNoNum", type="smallint", nullable=false, options={"comment"="Registra el identificador de la respuesta si trabaja o no."})
     */
    private $replistaexptrabajosinonum;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTrabajoSiNoDescrip", type="string", length=40, nullable=false, options={"comment"="Almacena la descripción de la respuesta."})
     */
    private $replistaexptrabajosinodescrip;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpInteresPrioridad", type="smallint", nullable=false, options={"comment"="Guarda la priorida del primer interés."})
     */
    private $replistaexpinteresprioridad;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpTipoInteresID", type="smallint", nullable=false, options={"comment"="Contiene el identificador del tipo de interes."})
     */
    private $replistaexptipointeresid;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoInteresDescrip", type="string", length=300, nullable=false, options={"comment"="Conserva la descripción del tipo de interés."})
     */
    private $replistaexptipointeresdescrip;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpAreaInteresID", type="smallint", nullable=false, options={"comment"="Preserva el identificador del área de interés."})
     */
    private $replistaexpareainteresid;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpAreaInteresDescrip", type="string", length=300, nullable=false, options={"comment"="Registra la descripción del área de interés."})
     */
    private $replistaexpareainteresdescrip;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpCodigoCurso", type="string", length=40, nullable=false, options={"comment"="Almacena el código del curso."})
     */
    private $replistaexpcodigocurso;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpNombCurso", type="string", length=200, nullable=false, options={"comment"="Guarda el nombre dle curso."})
     */
    private $replistaexpnombcurso;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpProceFormEstProcNum", type="integer", nullable=false, options={"comment"="Mantiene el identificador del estado del proceso formativo."})
     */
    private $replistaexpproceformestprocnum;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpProceFormEstProcDes", type="string", length=300, nullable=false, options={"comment"="Contiene la descripción del estado del proceso formativo."})
     */
    private $replistaexpproceformestprocdes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecInscrip", type="datetime", nullable=false, options={"comment"="Conserva la fecha de inicio del proceso formativo."})
     */
    private $replistaexpproceformfecinscrip;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecFinal", type="datetime", nullable=false, options={"comment"="Preserva la fecha de finalización del proceso formativo."})
     */
    private $replistaexpproceformfecfinal;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpTipoProcesoID", type="smallint", nullable=false, options={"comment"="Registra el identificador del tipo de proceso formativo."})
     */
    private $replistaexptipoprocesoid;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoNomb", type="string", length=300, nullable=false, options={"comment"="Almacena el nombre del tipo de proceso formativo."})
     */
    private $replistaexptipoprocesonomb;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoDescrip", type="string", length=300, nullable=false, options={"comment"="Guarda la descripción del tipo de proceso formativo."})
     */
    private $replistaexptipoprocesodescrip;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpCodigoCurso2", type="string", length=40, nullable=false)
     */
    private $replistaexpcodigocurso2;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpNombCurso2", type="string", length=200, nullable=false)
     */
    private $replistaexpnombcurso2;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpProForEstProNum2", type="integer", nullable=false)
     */
    private $replistaexpproforestpronum2;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpProForEstProDesc2", type="string", length=300, nullable=false)
     */
    private $replistaexpproforestprodesc2;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecIns2", type="datetime", nullable=false)
     */
    private $replistaexpproceformfecins2;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecFinal2", type="datetime", nullable=false)
     */
    private $replistaexpproceformfecfinal2;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpTipoProcesoID2", type="smallint", nullable=false)
     */
    private $replistaexptipoprocesoid2;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoNomb2", type="string", length=300, nullable=false)
     */
    private $replistaexptipoprocesonomb2;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoDescrip2", type="string", length=300, nullable=false)
     */
    private $replistaexptipoprocesodescrip2;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpCodigoCurso3", type="string", length=40, nullable=false)
     */
    private $replistaexpcodigocurso3;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpNombCurso3", type="string", length=200, nullable=false)
     */
    private $replistaexpnombcurso3;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpProForEstProNum3", type="integer", nullable=false)
     */
    private $replistaexpproforestpronum3;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpProForEstProDesc3", type="string", length=300, nullable=false)
     */
    private $replistaexpproforestprodesc3;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecIns3", type="datetime", nullable=false)
     */
    private $replistaexpproceformfecins3;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecFinal3", type="datetime", nullable=false)
     */
    private $replistaexpproceformfecfinal3;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpTipoProcesoID3", type="smallint", nullable=false)
     */
    private $replistaexptipoprocesoid3;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoNomb3", type="string", length=300, nullable=false)
     */
    private $replistaexptipoprocesonomb3;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoDescrip3", type="string", length=300, nullable=false)
     */
    private $replistaexptipoprocesodescrip3;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpCodigoCurso4", type="string", length=40, nullable=false)
     */
    private $replistaexpcodigocurso4;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpNombCurso4", type="string", length=200, nullable=false)
     */
    private $replistaexpnombcurso4;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpProForEstProNum4", type="integer", nullable=false)
     */
    private $replistaexpproforestpronum4;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpProForEstProDesc4", type="string", length=300, nullable=false)
     */
    private $replistaexpproforestprodesc4;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecIns4", type="datetime", nullable=false)
     */
    private $replistaexpproceformfecins4;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecFinal4", type="datetime", nullable=false)
     */
    private $replistaexpproceformfecfinal4;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpTipoProcesoID4", type="smallint", nullable=false)
     */
    private $replistaexptipoprocesoid4;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoNomb4", type="string", length=300, nullable=false)
     */
    private $replistaexptipoprocesonomb4;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoDescrip4", type="string", length=300, nullable=false)
     */
    private $replistaexptipoprocesodescrip4;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpCodigoCurso5", type="string", length=40, nullable=false)
     */
    private $replistaexpcodigocurso5;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpNombCurso5", type="string", length=200, nullable=false)
     */
    private $replistaexpnombcurso5;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpProForEstProNum5", type="integer", nullable=false)
     */
    private $replistaexpproforestpronum5;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpProForEstProDesc5", type="string", length=300, nullable=false)
     */
    private $replistaexpproforestprodesc5;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecIns5", type="datetime", nullable=false)
     */
    private $replistaexpproceformfecins5;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecFinal5", type="datetime", nullable=false)
     */
    private $replistaexpproceformfecfinal5;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpTipoProcesoID5", type="smallint", nullable=false)
     */
    private $replistaexptipoprocesoid5;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoNomb5", type="string", length=300, nullable=false)
     */
    private $replistaexptipoprocesonomb5;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoDescrip5", type="string", length=300, nullable=false)
     */
    private $replistaexptipoprocesodescrip5;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpCodigoCurso6", type="string", length=40, nullable=false)
     */
    private $replistaexpcodigocurso6;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpNombCurso6", type="string", length=200, nullable=false)
     */
    private $replistaexpnombcurso6;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpProForEstProNum6", type="integer", nullable=false)
     */
    private $replistaexpproforestpronum6;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpProForEstProDesc6", type="string", length=300, nullable=false)
     */
    private $replistaexpproforestprodesc6;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecIns6", type="datetime", nullable=false)
     */
    private $replistaexpproceformfecins6;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecFinal6", type="datetime", nullable=false)
     */
    private $replistaexpproceformfecfinal6;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpTipoProcesoID6", type="smallint", nullable=false)
     */
    private $replistaexptipoprocesoid6;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoNomb6", type="string", length=300, nullable=false)
     */
    private $replistaexptipoprocesonomb6;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoDescrip6", type="string", length=300, nullable=false)
     */
    private $replistaexptipoprocesodescrip6;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpCodigoCurso7", type="string", length=40, nullable=false)
     */
    private $replistaexpcodigocurso7;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpNombCurso7", type="string", length=200, nullable=false)
     */
    private $replistaexpnombcurso7;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpProForEstProNum7", type="integer", nullable=false)
     */
    private $replistaexpproforestpronum7;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpProForEstProDesc7", type="string", length=300, nullable=false)
     */
    private $replistaexpproforestprodesc7;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecIns7", type="datetime", nullable=false)
     */
    private $replistaexpproceformfecins7;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecFinal7", type="datetime", nullable=false)
     */
    private $replistaexpproceformfecfinal7;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpTipoProcesoID7", type="smallint", nullable=false)
     */
    private $replistaexptipoprocesoid7;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoNomb7", type="string", length=300, nullable=false)
     */
    private $replistaexptipoprocesonomb7;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoDescrip7", type="string", length=300, nullable=false)
     */
    private $replistaexptipoprocesodescrip7;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpCodigoCurso8", type="string", length=40, nullable=false)
     */
    private $replistaexpcodigocurso8;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpNombCurso8", type="string", length=200, nullable=false)
     */
    private $replistaexpnombcurso8;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpProForEstProNum8", type="integer", nullable=false)
     */
    private $replistaexpproforestpronum8;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpProForEstProDesc8", type="string", length=300, nullable=false)
     */
    private $replistaexpproforestprodesc8;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecIns8", type="datetime", nullable=false)
     */
    private $replistaexpproceformfecins8;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecFinal8", type="datetime", nullable=false)
     */
    private $replistaexpproceformfecfinal8;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpTipoProcesoID8", type="smallint", nullable=false)
     */
    private $replistaexptipoprocesoid8;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoNomb8", type="string", length=40, nullable=false)
     */
    private $replistaexptipoprocesonomb8;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoDescrip8", type="string", length=40, nullable=false)
     */
    private $replistaexptipoprocesodescrip8;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpCodigoCurso9", type="string", length=40, nullable=false)
     */
    private $replistaexpcodigocurso9;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpNombCurso9", type="string", length=200, nullable=false)
     */
    private $replistaexpnombcurso9;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpProForEstProNum9", type="integer", nullable=false)
     */
    private $replistaexpproforestpronum9;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpProForEstProDesc9", type="string", length=300, nullable=false)
     */
    private $replistaexpproforestprodesc9;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecIns9", type="datetime", nullable=false)
     */
    private $replistaexpproceformfecins9;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecFinal9", type="datetime", nullable=false)
     */
    private $replistaexpproceformfecfinal9;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpTipoProcesoID9", type="smallint", nullable=false)
     */
    private $replistaexptipoprocesoid9;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoNomb9", type="string", length=300, nullable=false)
     */
    private $replistaexptipoprocesonomb9;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoDescrip9", type="string", length=300, nullable=false)
     */
    private $replistaexptipoprocesodescrip9;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpCodigoCurso10", type="string", length=40, nullable=false)
     */
    private $replistaexpcodigocurso10;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpNombCurso10", type="string", length=200, nullable=false)
     */
    private $replistaexpnombcurso10;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpProForEstProNum10", type="integer", nullable=false)
     */
    private $replistaexpproforestpronum10;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpProForEstProDesc10", type="string", length=300, nullable=false)
     */
    private $replistaexpproforestprodesc10;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecIns10", type="datetime", nullable=false)
     */
    private $replistaexpproceformfecins10;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RepListaExpProceFormFecFinal10", type="datetime", nullable=false)
     */
    private $replistaexpproceformfecfinal10;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpTipoProcesoID10", type="smallint", nullable=false)
     */
    private $replistaexptipoprocesoid10;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoNomb10", type="string", length=300, nullable=false)
     */
    private $replistaexptipoprocesonomb10;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipoProcesoDescrip1", type="string", length=300, nullable=false)
     */
    private $replistaexptipoprocesodescrip1;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpTipIntDepRecrID", type="integer", nullable=false)
     */
    private $replistaexptipintdeprecrid;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipIntDepRecrDesc", type="string", length=300, nullable=false)
     */
    private $replistaexptipintdeprecrdesc;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpAreaIntDepRecrID", type="integer", nullable=false)
     */
    private $replistaexpareaintdeprecrid;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpAreaIntDepRecrDesc", type="string", length=300, nullable=false)
     */
    private $replistaexpareaintdeprecrdesc;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpTipIntMunLabID", type="integer", nullable=false)
     */
    private $replistaexptipintmunlabid;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipIntMunLabDesc", type="string", length=300, nullable=false)
     */
    private $replistaexptipintmunlabdesc;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpAreaIntMunLabID", type="integer", nullable=false)
     */
    private $replistaexpareaintmunlabid;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpAreaIntMunLabDesc", type="string", length=300, nullable=false)
     */
    private $replistaexpareaintmunlabdesc;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpTipIntOtroTemID", type="integer", nullable=false)
     */
    private $replistaexptipintotrotemid;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpTipIntOtroTemDesc", type="string", length=300, nullable=false)
     */
    private $replistaexptipintotrotemdesc;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpAreaIntOtroTemID", type="integer", nullable=false)
     */
    private $replistaexpareaintotrotemid;

    /**
     * @var string
     *
     * @ORM\Column(name="RepListaExpAreaIntOtroTemDesc", type="string", length=300, nullable=false)
     */
    private $replistaexpareaintotrotemdesc;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpIntPrioDepRecr", type="smallint", nullable=false)
     */
    private $replistaexpintpriodeprecr;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpIntPrioMunLab", type="smallint", nullable=false)
     */
    private $replistaexpintpriomunlab;

    /**
     * @var int
     *
     * @ORM\Column(name="RepListaExpIntPrioOtroTem", type="smallint", nullable=false)
     */
    private $replistaexpintpriootrotem;

    public function getReplistaexpid(): ?int
    {
        return $this->replistaexpid;
    }

    public function getReplistaexpnumexp(): ?string
    {
        return $this->replistaexpnumexp;
    }

    public function setReplistaexpnumexp(string $replistaexpnumexp): self
    {
        $this->replistaexpnumexp = $replistaexpnumexp;

        return $this;
    }

    public function getReplistaexptipoidentif(): ?string
    {
        return $this->replistaexptipoidentif;
    }

    public function setReplistaexptipoidentif(string $replistaexptipoidentif): self
    {
        $this->replistaexptipoidentif = $replistaexptipoidentif;

        return $this;
    }

    public function getReplistaexpindentif(): ?string
    {
        return $this->replistaexpindentif;
    }

    public function setReplistaexpindentif(string $replistaexpindentif): self
    {
        $this->replistaexpindentif = $replistaexpindentif;

        return $this;
    }

    public function getReplistaexpapel1(): ?string
    {
        return $this->replistaexpapel1;
    }

    public function setReplistaexpapel1(string $replistaexpapel1): self
    {
        $this->replistaexpapel1 = $replistaexpapel1;

        return $this;
    }

    public function getReplistaexpapel2(): ?string
    {
        return $this->replistaexpapel2;
    }

    public function setReplistaexpapel2(string $replistaexpapel2): self
    {
        $this->replistaexpapel2 = $replistaexpapel2;

        return $this;
    }

    public function getReplistaexpnombre(): ?string
    {
        return $this->replistaexpnombre;
    }

    public function setReplistaexpnombre(string $replistaexpnombre): self
    {
        $this->replistaexpnombre = $replistaexpnombre;

        return $this;
    }

    public function getReplistaexpsexoid(): ?int
    {
        return $this->replistaexpsexoid;
    }

    public function setReplistaexpsexoid(int $replistaexpsexoid): self
    {
        $this->replistaexpsexoid = $replistaexpsexoid;

        return $this;
    }

    public function getReplistaexpsexodesc(): ?string
    {
        return $this->replistaexpsexodesc;
    }

    public function setReplistaexpsexodesc(string $replistaexpsexodesc): self
    {
        $this->replistaexpsexodesc = $replistaexpsexodesc;

        return $this;
    }

    public function getReplistaexpfecnac(): ?\DateTimeInterface
    {
        return $this->replistaexpfecnac;
    }

    public function setReplistaexpfecnac(\DateTimeInterface $replistaexpfecnac): self
    {
        $this->replistaexpfecnac = $replistaexpfecnac;

        return $this;
    }

    public function getReplistaexptelefono(): ?int
    {
        return $this->replistaexptelefono;
    }

    public function setReplistaexptelefono(int $replistaexptelefono): self
    {
        $this->replistaexptelefono = $replistaexptelefono;

        return $this;
    }

    public function getReplistaexpcelular(): ?int
    {
        return $this->replistaexpcelular;
    }

    public function setReplistaexpcelular(int $replistaexpcelular): self
    {
        $this->replistaexpcelular = $replistaexpcelular;

        return $this;
    }

    public function getReplistaexpcorreo(): ?string
    {
        return $this->replistaexpcorreo;
    }

    public function setReplistaexpcorreo(string $replistaexpcorreo): self
    {
        $this->replistaexpcorreo = $replistaexpcorreo;

        return $this;
    }

    public function getReplistaexpcentrocivicoid(): ?int
    {
        return $this->replistaexpcentrocivicoid;
    }

    public function setReplistaexpcentrocivicoid(int $replistaexpcentrocivicoid): self
    {
        $this->replistaexpcentrocivicoid = $replistaexpcentrocivicoid;

        return $this;
    }

    public function getReplistaexpcentrocivicosiglas(): ?string
    {
        return $this->replistaexpcentrocivicosiglas;
    }

    public function setReplistaexpcentrocivicosiglas(string $replistaexpcentrocivicosiglas): self
    {
        $this->replistaexpcentrocivicosiglas = $replistaexpcentrocivicosiglas;

        return $this;
    }

    public function getReplistaexpcentrocivicodesc(): ?string
    {
        return $this->replistaexpcentrocivicodesc;
    }

    public function setReplistaexpcentrocivicodesc(string $replistaexpcentrocivicodesc): self
    {
        $this->replistaexpcentrocivicodesc = $replistaexpcentrocivicodesc;

        return $this;
    }

    public function getReplistaexpprovinciaid(): ?int
    {
        return $this->replistaexpprovinciaid;
    }

    public function setReplistaexpprovinciaid(int $replistaexpprovinciaid): self
    {
        $this->replistaexpprovinciaid = $replistaexpprovinciaid;

        return $this;
    }

    public function getReplistaexpprovincia(): ?string
    {
        return $this->replistaexpprovincia;
    }

    public function setReplistaexpprovincia(string $replistaexpprovincia): self
    {
        $this->replistaexpprovincia = $replistaexpprovincia;

        return $this;
    }

    public function getReplistaexpcantonid(): ?int
    {
        return $this->replistaexpcantonid;
    }

    public function setReplistaexpcantonid(int $replistaexpcantonid): self
    {
        $this->replistaexpcantonid = $replistaexpcantonid;

        return $this;
    }

    public function getReplistaexpcanton(): ?string
    {
        return $this->replistaexpcanton;
    }

    public function setReplistaexpcanton(string $replistaexpcanton): self
    {
        $this->replistaexpcanton = $replistaexpcanton;

        return $this;
    }

    public function getReplistaexpdistritoid(): ?int
    {
        return $this->replistaexpdistritoid;
    }

    public function setReplistaexpdistritoid(int $replistaexpdistritoid): self
    {
        $this->replistaexpdistritoid = $replistaexpdistritoid;

        return $this;
    }

    public function getReplistaexpdistrito(): ?string
    {
        return $this->replistaexpdistrito;
    }

    public function setReplistaexpdistrito(string $replistaexpdistrito): self
    {
        $this->replistaexpdistrito = $replistaexpdistrito;

        return $this;
    }

    public function getReplistaexpbarrio(): ?string
    {
        return $this->replistaexpbarrio;
    }

    public function setReplistaexpbarrio(string $replistaexpbarrio): self
    {
        $this->replistaexpbarrio = $replistaexpbarrio;

        return $this;
    }

    public function getReplistaexpotrasenas(): ?string
    {
        return $this->replistaexpotrasenas;
    }

    public function setReplistaexpotrasenas(string $replistaexpotrasenas): self
    {
        $this->replistaexpotrasenas = $replistaexpotrasenas;

        return $this;
    }

    public function getReplistaexpenfercronica(): ?int
    {
        return $this->replistaexpenfercronica;
    }

    public function setReplistaexpenfercronica(int $replistaexpenfercronica): self
    {
        $this->replistaexpenfercronica = $replistaexpenfercronica;

        return $this;
    }

    public function getReplistaexpenfermedades(): ?string
    {
        return $this->replistaexpenfermedades;
    }

    public function setReplistaexpenfermedades(string $replistaexpenfermedades): self
    {
        $this->replistaexpenfermedades = $replistaexpenfermedades;

        return $this;
    }

    public function getReplistaexpniveleducnum(): ?int
    {
        return $this->replistaexpniveleducnum;
    }

    public function setReplistaexpniveleducnum(int $replistaexpniveleducnum): self
    {
        $this->replistaexpniveleducnum = $replistaexpniveleducnum;

        return $this;
    }

    public function getReplistaexpniveleducdescrip(): ?string
    {
        return $this->replistaexpniveleducdescrip;
    }

    public function setReplistaexpniveleducdescrip(string $replistaexpniveleducdescrip): self
    {
        $this->replistaexpniveleducdescrip = $replistaexpniveleducdescrip;

        return $this;
    }

    public function getReplistaexpeducacionactualnum(): ?int
    {
        return $this->replistaexpeducacionactualnum;
    }

    public function setReplistaexpeducacionactualnum(int $replistaexpeducacionactualnum): self
    {
        $this->replistaexpeducacionactualnum = $replistaexpeducacionactualnum;

        return $this;
    }

    public function getReplistaexpeducacionactualdesc(): ?string
    {
        return $this->replistaexpeducacionactualdesc;
    }

    public function setReplistaexpeducacionactualdesc(string $replistaexpeducacionactualdesc): self
    {
        $this->replistaexpeducacionactualdesc = $replistaexpeducacionactualdesc;

        return $this;
    }

    public function getReplistaexpniveleduc2num(): ?int
    {
        return $this->replistaexpniveleduc2num;
    }

    public function setReplistaexpniveleduc2num(int $replistaexpniveleduc2num): self
    {
        $this->replistaexpniveleduc2num = $replistaexpniveleduc2num;

        return $this;
    }

    public function getReplistaexpniveleduc2descrip(): ?string
    {
        return $this->replistaexpniveleduc2descrip;
    }

    public function setReplistaexpniveleduc2descrip(string $replistaexpniveleduc2descrip): self
    {
        $this->replistaexpniveleduc2descrip = $replistaexpniveleduc2descrip;

        return $this;
    }

    public function getReplistaexpeducacionactual2num(): ?int
    {
        return $this->replistaexpeducacionactual2num;
    }

    public function setReplistaexpeducacionactual2num(int $replistaexpeducacionactual2num): self
    {
        $this->replistaexpeducacionactual2num = $replistaexpeducacionactual2num;

        return $this;
    }

    public function getReplistaexpeducacionactual2des(): ?string
    {
        return $this->replistaexpeducacionactual2des;
    }

    public function setReplistaexpeducacionactual2des(string $replistaexpeducacionactual2des): self
    {
        $this->replistaexpeducacionactual2des = $replistaexpeducacionactual2des;

        return $this;
    }

    public function getReplistaexpcentroeducid(): ?int
    {
        return $this->replistaexpcentroeducid;
    }

    public function setReplistaexpcentroeducid(int $replistaexpcentroeducid): self
    {
        $this->replistaexpcentroeducid = $replistaexpcentroeducid;

        return $this;
    }

    public function getReplistaexpcentroeducnombre(): ?string
    {
        return $this->replistaexpcentroeducnombre;
    }

    public function setReplistaexpcentroeducnombre(string $replistaexpcentroeducnombre): self
    {
        $this->replistaexpcentroeducnombre = $replistaexpcentroeducnombre;

        return $this;
    }

    public function getReplistaexptrabajosinonum(): ?int
    {
        return $this->replistaexptrabajosinonum;
    }

    public function setReplistaexptrabajosinonum(int $replistaexptrabajosinonum): self
    {
        $this->replistaexptrabajosinonum = $replistaexptrabajosinonum;

        return $this;
    }

    public function getReplistaexptrabajosinodescrip(): ?string
    {
        return $this->replistaexptrabajosinodescrip;
    }

    public function setReplistaexptrabajosinodescrip(string $replistaexptrabajosinodescrip): self
    {
        $this->replistaexptrabajosinodescrip = $replistaexptrabajosinodescrip;

        return $this;
    }

    public function getReplistaexpinteresprioridad(): ?int
    {
        return $this->replistaexpinteresprioridad;
    }

    public function setReplistaexpinteresprioridad(int $replistaexpinteresprioridad): self
    {
        $this->replistaexpinteresprioridad = $replistaexpinteresprioridad;

        return $this;
    }

    public function getReplistaexptipointeresid(): ?int
    {
        return $this->replistaexptipointeresid;
    }

    public function setReplistaexptipointeresid(int $replistaexptipointeresid): self
    {
        $this->replistaexptipointeresid = $replistaexptipointeresid;

        return $this;
    }

    public function getReplistaexptipointeresdescrip(): ?string
    {
        return $this->replistaexptipointeresdescrip;
    }

    public function setReplistaexptipointeresdescrip(string $replistaexptipointeresdescrip): self
    {
        $this->replistaexptipointeresdescrip = $replistaexptipointeresdescrip;

        return $this;
    }

    public function getReplistaexpareainteresid(): ?int
    {
        return $this->replistaexpareainteresid;
    }

    public function setReplistaexpareainteresid(int $replistaexpareainteresid): self
    {
        $this->replistaexpareainteresid = $replistaexpareainteresid;

        return $this;
    }

    public function getReplistaexpareainteresdescrip(): ?string
    {
        return $this->replistaexpareainteresdescrip;
    }

    public function setReplistaexpareainteresdescrip(string $replistaexpareainteresdescrip): self
    {
        $this->replistaexpareainteresdescrip = $replistaexpareainteresdescrip;

        return $this;
    }

    public function getReplistaexpcodigocurso(): ?string
    {
        return $this->replistaexpcodigocurso;
    }

    public function setReplistaexpcodigocurso(string $replistaexpcodigocurso): self
    {
        $this->replistaexpcodigocurso = $replistaexpcodigocurso;

        return $this;
    }

    public function getReplistaexpnombcurso(): ?string
    {
        return $this->replistaexpnombcurso;
    }

    public function setReplistaexpnombcurso(string $replistaexpnombcurso): self
    {
        $this->replistaexpnombcurso = $replistaexpnombcurso;

        return $this;
    }

    public function getReplistaexpproceformestprocnum(): ?int
    {
        return $this->replistaexpproceformestprocnum;
    }

    public function setReplistaexpproceformestprocnum(int $replistaexpproceformestprocnum): self
    {
        $this->replistaexpproceformestprocnum = $replistaexpproceformestprocnum;

        return $this;
    }

    public function getReplistaexpproceformestprocdes(): ?string
    {
        return $this->replistaexpproceformestprocdes;
    }

    public function setReplistaexpproceformestprocdes(string $replistaexpproceformestprocdes): self
    {
        $this->replistaexpproceformestprocdes = $replistaexpproceformestprocdes;

        return $this;
    }

    public function getReplistaexpproceformfecinscrip(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecinscrip;
    }

    public function setReplistaexpproceformfecinscrip(\DateTimeInterface $replistaexpproceformfecinscrip): self
    {
        $this->replistaexpproceformfecinscrip = $replistaexpproceformfecinscrip;

        return $this;
    }

    public function getReplistaexpproceformfecfinal(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecfinal;
    }

    public function setReplistaexpproceformfecfinal(\DateTimeInterface $replistaexpproceformfecfinal): self
    {
        $this->replistaexpproceformfecfinal = $replistaexpproceformfecfinal;

        return $this;
    }

    public function getReplistaexptipoprocesoid(): ?int
    {
        return $this->replistaexptipoprocesoid;
    }

    public function setReplistaexptipoprocesoid(int $replistaexptipoprocesoid): self
    {
        $this->replistaexptipoprocesoid = $replistaexptipoprocesoid;

        return $this;
    }

    public function getReplistaexptipoprocesonomb(): ?string
    {
        return $this->replistaexptipoprocesonomb;
    }

    public function setReplistaexptipoprocesonomb(string $replistaexptipoprocesonomb): self
    {
        $this->replistaexptipoprocesonomb = $replistaexptipoprocesonomb;

        return $this;
    }

    public function getReplistaexptipoprocesodescrip(): ?string
    {
        return $this->replistaexptipoprocesodescrip;
    }

    public function setReplistaexptipoprocesodescrip(string $replistaexptipoprocesodescrip): self
    {
        $this->replistaexptipoprocesodescrip = $replistaexptipoprocesodescrip;

        return $this;
    }

    public function getReplistaexpcodigocurso2(): ?string
    {
        return $this->replistaexpcodigocurso2;
    }

    public function setReplistaexpcodigocurso2(string $replistaexpcodigocurso2): self
    {
        $this->replistaexpcodigocurso2 = $replistaexpcodigocurso2;

        return $this;
    }

    public function getReplistaexpnombcurso2(): ?string
    {
        return $this->replistaexpnombcurso2;
    }

    public function setReplistaexpnombcurso2(string $replistaexpnombcurso2): self
    {
        $this->replistaexpnombcurso2 = $replistaexpnombcurso2;

        return $this;
    }

    public function getReplistaexpproforestpronum2(): ?int
    {
        return $this->replistaexpproforestpronum2;
    }

    public function setReplistaexpproforestpronum2(int $replistaexpproforestpronum2): self
    {
        $this->replistaexpproforestpronum2 = $replistaexpproforestpronum2;

        return $this;
    }

    public function getReplistaexpproforestprodesc2(): ?string
    {
        return $this->replistaexpproforestprodesc2;
    }

    public function setReplistaexpproforestprodesc2(string $replistaexpproforestprodesc2): self
    {
        $this->replistaexpproforestprodesc2 = $replistaexpproforestprodesc2;

        return $this;
    }

    public function getReplistaexpproceformfecins2(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecins2;
    }

    public function setReplistaexpproceformfecins2(\DateTimeInterface $replistaexpproceformfecins2): self
    {
        $this->replistaexpproceformfecins2 = $replistaexpproceformfecins2;

        return $this;
    }

    public function getReplistaexpproceformfecfinal2(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecfinal2;
    }

    public function setReplistaexpproceformfecfinal2(\DateTimeInterface $replistaexpproceformfecfinal2): self
    {
        $this->replistaexpproceformfecfinal2 = $replistaexpproceformfecfinal2;

        return $this;
    }

    public function getReplistaexptipoprocesoid2(): ?int
    {
        return $this->replistaexptipoprocesoid2;
    }

    public function setReplistaexptipoprocesoid2(int $replistaexptipoprocesoid2): self
    {
        $this->replistaexptipoprocesoid2 = $replistaexptipoprocesoid2;

        return $this;
    }

    public function getReplistaexptipoprocesonomb2(): ?string
    {
        return $this->replistaexptipoprocesonomb2;
    }

    public function setReplistaexptipoprocesonomb2(string $replistaexptipoprocesonomb2): self
    {
        $this->replistaexptipoprocesonomb2 = $replistaexptipoprocesonomb2;

        return $this;
    }

    public function getReplistaexptipoprocesodescrip2(): ?string
    {
        return $this->replistaexptipoprocesodescrip2;
    }

    public function setReplistaexptipoprocesodescrip2(string $replistaexptipoprocesodescrip2): self
    {
        $this->replistaexptipoprocesodescrip2 = $replistaexptipoprocesodescrip2;

        return $this;
    }

    public function getReplistaexpcodigocurso3(): ?string
    {
        return $this->replistaexpcodigocurso3;
    }

    public function setReplistaexpcodigocurso3(string $replistaexpcodigocurso3): self
    {
        $this->replistaexpcodigocurso3 = $replistaexpcodigocurso3;

        return $this;
    }

    public function getReplistaexpnombcurso3(): ?string
    {
        return $this->replistaexpnombcurso3;
    }

    public function setReplistaexpnombcurso3(string $replistaexpnombcurso3): self
    {
        $this->replistaexpnombcurso3 = $replistaexpnombcurso3;

        return $this;
    }

    public function getReplistaexpproforestpronum3(): ?int
    {
        return $this->replistaexpproforestpronum3;
    }

    public function setReplistaexpproforestpronum3(int $replistaexpproforestpronum3): self
    {
        $this->replistaexpproforestpronum3 = $replistaexpproforestpronum3;

        return $this;
    }

    public function getReplistaexpproforestprodesc3(): ?string
    {
        return $this->replistaexpproforestprodesc3;
    }

    public function setReplistaexpproforestprodesc3(string $replistaexpproforestprodesc3): self
    {
        $this->replistaexpproforestprodesc3 = $replistaexpproforestprodesc3;

        return $this;
    }

    public function getReplistaexpproceformfecins3(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecins3;
    }

    public function setReplistaexpproceformfecins3(\DateTimeInterface $replistaexpproceformfecins3): self
    {
        $this->replistaexpproceformfecins3 = $replistaexpproceformfecins3;

        return $this;
    }

    public function getReplistaexpproceformfecfinal3(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecfinal3;
    }

    public function setReplistaexpproceformfecfinal3(\DateTimeInterface $replistaexpproceformfecfinal3): self
    {
        $this->replistaexpproceformfecfinal3 = $replistaexpproceformfecfinal3;

        return $this;
    }

    public function getReplistaexptipoprocesoid3(): ?int
    {
        return $this->replistaexptipoprocesoid3;
    }

    public function setReplistaexptipoprocesoid3(int $replistaexptipoprocesoid3): self
    {
        $this->replistaexptipoprocesoid3 = $replistaexptipoprocesoid3;

        return $this;
    }

    public function getReplistaexptipoprocesonomb3(): ?string
    {
        return $this->replistaexptipoprocesonomb3;
    }

    public function setReplistaexptipoprocesonomb3(string $replistaexptipoprocesonomb3): self
    {
        $this->replistaexptipoprocesonomb3 = $replistaexptipoprocesonomb3;

        return $this;
    }

    public function getReplistaexptipoprocesodescrip3(): ?string
    {
        return $this->replistaexptipoprocesodescrip3;
    }

    public function setReplistaexptipoprocesodescrip3(string $replistaexptipoprocesodescrip3): self
    {
        $this->replistaexptipoprocesodescrip3 = $replistaexptipoprocesodescrip3;

        return $this;
    }

    public function getReplistaexpcodigocurso4(): ?string
    {
        return $this->replistaexpcodigocurso4;
    }

    public function setReplistaexpcodigocurso4(string $replistaexpcodigocurso4): self
    {
        $this->replistaexpcodigocurso4 = $replistaexpcodigocurso4;

        return $this;
    }

    public function getReplistaexpnombcurso4(): ?string
    {
        return $this->replistaexpnombcurso4;
    }

    public function setReplistaexpnombcurso4(string $replistaexpnombcurso4): self
    {
        $this->replistaexpnombcurso4 = $replistaexpnombcurso4;

        return $this;
    }

    public function getReplistaexpproforestpronum4(): ?int
    {
        return $this->replistaexpproforestpronum4;
    }

    public function setReplistaexpproforestpronum4(int $replistaexpproforestpronum4): self
    {
        $this->replistaexpproforestpronum4 = $replistaexpproforestpronum4;

        return $this;
    }

    public function getReplistaexpproforestprodesc4(): ?string
    {
        return $this->replistaexpproforestprodesc4;
    }

    public function setReplistaexpproforestprodesc4(string $replistaexpproforestprodesc4): self
    {
        $this->replistaexpproforestprodesc4 = $replistaexpproforestprodesc4;

        return $this;
    }

    public function getReplistaexpproceformfecins4(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecins4;
    }

    public function setReplistaexpproceformfecins4(\DateTimeInterface $replistaexpproceformfecins4): self
    {
        $this->replistaexpproceformfecins4 = $replistaexpproceformfecins4;

        return $this;
    }

    public function getReplistaexpproceformfecfinal4(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecfinal4;
    }

    public function setReplistaexpproceformfecfinal4(\DateTimeInterface $replistaexpproceformfecfinal4): self
    {
        $this->replistaexpproceformfecfinal4 = $replistaexpproceformfecfinal4;

        return $this;
    }

    public function getReplistaexptipoprocesoid4(): ?int
    {
        return $this->replistaexptipoprocesoid4;
    }

    public function setReplistaexptipoprocesoid4(int $replistaexptipoprocesoid4): self
    {
        $this->replistaexptipoprocesoid4 = $replistaexptipoprocesoid4;

        return $this;
    }

    public function getReplistaexptipoprocesonomb4(): ?string
    {
        return $this->replistaexptipoprocesonomb4;
    }

    public function setReplistaexptipoprocesonomb4(string $replistaexptipoprocesonomb4): self
    {
        $this->replistaexptipoprocesonomb4 = $replistaexptipoprocesonomb4;

        return $this;
    }

    public function getReplistaexptipoprocesodescrip4(): ?string
    {
        return $this->replistaexptipoprocesodescrip4;
    }

    public function setReplistaexptipoprocesodescrip4(string $replistaexptipoprocesodescrip4): self
    {
        $this->replistaexptipoprocesodescrip4 = $replistaexptipoprocesodescrip4;

        return $this;
    }

    public function getReplistaexpcodigocurso5(): ?string
    {
        return $this->replistaexpcodigocurso5;
    }

    public function setReplistaexpcodigocurso5(string $replistaexpcodigocurso5): self
    {
        $this->replistaexpcodigocurso5 = $replistaexpcodigocurso5;

        return $this;
    }

    public function getReplistaexpnombcurso5(): ?string
    {
        return $this->replistaexpnombcurso5;
    }

    public function setReplistaexpnombcurso5(string $replistaexpnombcurso5): self
    {
        $this->replistaexpnombcurso5 = $replistaexpnombcurso5;

        return $this;
    }

    public function getReplistaexpproforestpronum5(): ?int
    {
        return $this->replistaexpproforestpronum5;
    }

    public function setReplistaexpproforestpronum5(int $replistaexpproforestpronum5): self
    {
        $this->replistaexpproforestpronum5 = $replistaexpproforestpronum5;

        return $this;
    }

    public function getReplistaexpproforestprodesc5(): ?string
    {
        return $this->replistaexpproforestprodesc5;
    }

    public function setReplistaexpproforestprodesc5(string $replistaexpproforestprodesc5): self
    {
        $this->replistaexpproforestprodesc5 = $replistaexpproforestprodesc5;

        return $this;
    }

    public function getReplistaexpproceformfecins5(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecins5;
    }

    public function setReplistaexpproceformfecins5(\DateTimeInterface $replistaexpproceformfecins5): self
    {
        $this->replistaexpproceformfecins5 = $replistaexpproceformfecins5;

        return $this;
    }

    public function getReplistaexpproceformfecfinal5(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecfinal5;
    }

    public function setReplistaexpproceformfecfinal5(\DateTimeInterface $replistaexpproceformfecfinal5): self
    {
        $this->replistaexpproceformfecfinal5 = $replistaexpproceformfecfinal5;

        return $this;
    }

    public function getReplistaexptipoprocesoid5(): ?int
    {
        return $this->replistaexptipoprocesoid5;
    }

    public function setReplistaexptipoprocesoid5(int $replistaexptipoprocesoid5): self
    {
        $this->replistaexptipoprocesoid5 = $replistaexptipoprocesoid5;

        return $this;
    }

    public function getReplistaexptipoprocesonomb5(): ?string
    {
        return $this->replistaexptipoprocesonomb5;
    }

    public function setReplistaexptipoprocesonomb5(string $replistaexptipoprocesonomb5): self
    {
        $this->replistaexptipoprocesonomb5 = $replistaexptipoprocesonomb5;

        return $this;
    }

    public function getReplistaexptipoprocesodescrip5(): ?string
    {
        return $this->replistaexptipoprocesodescrip5;
    }

    public function setReplistaexptipoprocesodescrip5(string $replistaexptipoprocesodescrip5): self
    {
        $this->replistaexptipoprocesodescrip5 = $replistaexptipoprocesodescrip5;

        return $this;
    }

    public function getReplistaexpcodigocurso6(): ?string
    {
        return $this->replistaexpcodigocurso6;
    }

    public function setReplistaexpcodigocurso6(string $replistaexpcodigocurso6): self
    {
        $this->replistaexpcodigocurso6 = $replistaexpcodigocurso6;

        return $this;
    }

    public function getReplistaexpnombcurso6(): ?string
    {
        return $this->replistaexpnombcurso6;
    }

    public function setReplistaexpnombcurso6(string $replistaexpnombcurso6): self
    {
        $this->replistaexpnombcurso6 = $replistaexpnombcurso6;

        return $this;
    }

    public function getReplistaexpproforestpronum6(): ?int
    {
        return $this->replistaexpproforestpronum6;
    }

    public function setReplistaexpproforestpronum6(int $replistaexpproforestpronum6): self
    {
        $this->replistaexpproforestpronum6 = $replistaexpproforestpronum6;

        return $this;
    }

    public function getReplistaexpproforestprodesc6(): ?string
    {
        return $this->replistaexpproforestprodesc6;
    }

    public function setReplistaexpproforestprodesc6(string $replistaexpproforestprodesc6): self
    {
        $this->replistaexpproforestprodesc6 = $replistaexpproforestprodesc6;

        return $this;
    }

    public function getReplistaexpproceformfecins6(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecins6;
    }

    public function setReplistaexpproceformfecins6(\DateTimeInterface $replistaexpproceformfecins6): self
    {
        $this->replistaexpproceformfecins6 = $replistaexpproceformfecins6;

        return $this;
    }

    public function getReplistaexpproceformfecfinal6(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecfinal6;
    }

    public function setReplistaexpproceformfecfinal6(\DateTimeInterface $replistaexpproceformfecfinal6): self
    {
        $this->replistaexpproceformfecfinal6 = $replistaexpproceformfecfinal6;

        return $this;
    }

    public function getReplistaexptipoprocesoid6(): ?int
    {
        return $this->replistaexptipoprocesoid6;
    }

    public function setReplistaexptipoprocesoid6(int $replistaexptipoprocesoid6): self
    {
        $this->replistaexptipoprocesoid6 = $replistaexptipoprocesoid6;

        return $this;
    }

    public function getReplistaexptipoprocesonomb6(): ?string
    {
        return $this->replistaexptipoprocesonomb6;
    }

    public function setReplistaexptipoprocesonomb6(string $replistaexptipoprocesonomb6): self
    {
        $this->replistaexptipoprocesonomb6 = $replistaexptipoprocesonomb6;

        return $this;
    }

    public function getReplistaexptipoprocesodescrip6(): ?string
    {
        return $this->replistaexptipoprocesodescrip6;
    }

    public function setReplistaexptipoprocesodescrip6(string $replistaexptipoprocesodescrip6): self
    {
        $this->replistaexptipoprocesodescrip6 = $replistaexptipoprocesodescrip6;

        return $this;
    }

    public function getReplistaexpcodigocurso7(): ?string
    {
        return $this->replistaexpcodigocurso7;
    }

    public function setReplistaexpcodigocurso7(string $replistaexpcodigocurso7): self
    {
        $this->replistaexpcodigocurso7 = $replistaexpcodigocurso7;

        return $this;
    }

    public function getReplistaexpnombcurso7(): ?string
    {
        return $this->replistaexpnombcurso7;
    }

    public function setReplistaexpnombcurso7(string $replistaexpnombcurso7): self
    {
        $this->replistaexpnombcurso7 = $replistaexpnombcurso7;

        return $this;
    }

    public function getReplistaexpproforestpronum7(): ?int
    {
        return $this->replistaexpproforestpronum7;
    }

    public function setReplistaexpproforestpronum7(int $replistaexpproforestpronum7): self
    {
        $this->replistaexpproforestpronum7 = $replistaexpproforestpronum7;

        return $this;
    }

    public function getReplistaexpproforestprodesc7(): ?string
    {
        return $this->replistaexpproforestprodesc7;
    }

    public function setReplistaexpproforestprodesc7(string $replistaexpproforestprodesc7): self
    {
        $this->replistaexpproforestprodesc7 = $replistaexpproforestprodesc7;

        return $this;
    }

    public function getReplistaexpproceformfecins7(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecins7;
    }

    public function setReplistaexpproceformfecins7(\DateTimeInterface $replistaexpproceformfecins7): self
    {
        $this->replistaexpproceformfecins7 = $replistaexpproceformfecins7;

        return $this;
    }

    public function getReplistaexpproceformfecfinal7(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecfinal7;
    }

    public function setReplistaexpproceformfecfinal7(\DateTimeInterface $replistaexpproceformfecfinal7): self
    {
        $this->replistaexpproceformfecfinal7 = $replistaexpproceformfecfinal7;

        return $this;
    }

    public function getReplistaexptipoprocesoid7(): ?int
    {
        return $this->replistaexptipoprocesoid7;
    }

    public function setReplistaexptipoprocesoid7(int $replistaexptipoprocesoid7): self
    {
        $this->replistaexptipoprocesoid7 = $replistaexptipoprocesoid7;

        return $this;
    }

    public function getReplistaexptipoprocesonomb7(): ?string
    {
        return $this->replistaexptipoprocesonomb7;
    }

    public function setReplistaexptipoprocesonomb7(string $replistaexptipoprocesonomb7): self
    {
        $this->replistaexptipoprocesonomb7 = $replistaexptipoprocesonomb7;

        return $this;
    }

    public function getReplistaexptipoprocesodescrip7(): ?string
    {
        return $this->replistaexptipoprocesodescrip7;
    }

    public function setReplistaexptipoprocesodescrip7(string $replistaexptipoprocesodescrip7): self
    {
        $this->replistaexptipoprocesodescrip7 = $replistaexptipoprocesodescrip7;

        return $this;
    }

    public function getReplistaexpcodigocurso8(): ?string
    {
        return $this->replistaexpcodigocurso8;
    }

    public function setReplistaexpcodigocurso8(string $replistaexpcodigocurso8): self
    {
        $this->replistaexpcodigocurso8 = $replistaexpcodigocurso8;

        return $this;
    }

    public function getReplistaexpnombcurso8(): ?string
    {
        return $this->replistaexpnombcurso8;
    }

    public function setReplistaexpnombcurso8(string $replistaexpnombcurso8): self
    {
        $this->replistaexpnombcurso8 = $replistaexpnombcurso8;

        return $this;
    }

    public function getReplistaexpproforestpronum8(): ?int
    {
        return $this->replistaexpproforestpronum8;
    }

    public function setReplistaexpproforestpronum8(int $replistaexpproforestpronum8): self
    {
        $this->replistaexpproforestpronum8 = $replistaexpproforestpronum8;

        return $this;
    }

    public function getReplistaexpproforestprodesc8(): ?string
    {
        return $this->replistaexpproforestprodesc8;
    }

    public function setReplistaexpproforestprodesc8(string $replistaexpproforestprodesc8): self
    {
        $this->replistaexpproforestprodesc8 = $replistaexpproforestprodesc8;

        return $this;
    }

    public function getReplistaexpproceformfecins8(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecins8;
    }

    public function setReplistaexpproceformfecins8(\DateTimeInterface $replistaexpproceformfecins8): self
    {
        $this->replistaexpproceformfecins8 = $replistaexpproceformfecins8;

        return $this;
    }

    public function getReplistaexpproceformfecfinal8(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecfinal8;
    }

    public function setReplistaexpproceformfecfinal8(\DateTimeInterface $replistaexpproceformfecfinal8): self
    {
        $this->replistaexpproceformfecfinal8 = $replistaexpproceformfecfinal8;

        return $this;
    }

    public function getReplistaexptipoprocesoid8(): ?int
    {
        return $this->replistaexptipoprocesoid8;
    }

    public function setReplistaexptipoprocesoid8(int $replistaexptipoprocesoid8): self
    {
        $this->replistaexptipoprocesoid8 = $replistaexptipoprocesoid8;

        return $this;
    }

    public function getReplistaexptipoprocesonomb8(): ?string
    {
        return $this->replistaexptipoprocesonomb8;
    }

    public function setReplistaexptipoprocesonomb8(string $replistaexptipoprocesonomb8): self
    {
        $this->replistaexptipoprocesonomb8 = $replistaexptipoprocesonomb8;

        return $this;
    }

    public function getReplistaexptipoprocesodescrip8(): ?string
    {
        return $this->replistaexptipoprocesodescrip8;
    }

    public function setReplistaexptipoprocesodescrip8(string $replistaexptipoprocesodescrip8): self
    {
        $this->replistaexptipoprocesodescrip8 = $replistaexptipoprocesodescrip8;

        return $this;
    }

    public function getReplistaexpcodigocurso9(): ?string
    {
        return $this->replistaexpcodigocurso9;
    }

    public function setReplistaexpcodigocurso9(string $replistaexpcodigocurso9): self
    {
        $this->replistaexpcodigocurso9 = $replistaexpcodigocurso9;

        return $this;
    }

    public function getReplistaexpnombcurso9(): ?string
    {
        return $this->replistaexpnombcurso9;
    }

    public function setReplistaexpnombcurso9(string $replistaexpnombcurso9): self
    {
        $this->replistaexpnombcurso9 = $replistaexpnombcurso9;

        return $this;
    }

    public function getReplistaexpproforestpronum9(): ?int
    {
        return $this->replistaexpproforestpronum9;
    }

    public function setReplistaexpproforestpronum9(int $replistaexpproforestpronum9): self
    {
        $this->replistaexpproforestpronum9 = $replistaexpproforestpronum9;

        return $this;
    }

    public function getReplistaexpproforestprodesc9(): ?string
    {
        return $this->replistaexpproforestprodesc9;
    }

    public function setReplistaexpproforestprodesc9(string $replistaexpproforestprodesc9): self
    {
        $this->replistaexpproforestprodesc9 = $replistaexpproforestprodesc9;

        return $this;
    }

    public function getReplistaexpproceformfecins9(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecins9;
    }

    public function setReplistaexpproceformfecins9(\DateTimeInterface $replistaexpproceformfecins9): self
    {
        $this->replistaexpproceformfecins9 = $replistaexpproceformfecins9;

        return $this;
    }

    public function getReplistaexpproceformfecfinal9(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecfinal9;
    }

    public function setReplistaexpproceformfecfinal9(\DateTimeInterface $replistaexpproceformfecfinal9): self
    {
        $this->replistaexpproceformfecfinal9 = $replistaexpproceformfecfinal9;

        return $this;
    }

    public function getReplistaexptipoprocesoid9(): ?int
    {
        return $this->replistaexptipoprocesoid9;
    }

    public function setReplistaexptipoprocesoid9(int $replistaexptipoprocesoid9): self
    {
        $this->replistaexptipoprocesoid9 = $replistaexptipoprocesoid9;

        return $this;
    }

    public function getReplistaexptipoprocesonomb9(): ?string
    {
        return $this->replistaexptipoprocesonomb9;
    }

    public function setReplistaexptipoprocesonomb9(string $replistaexptipoprocesonomb9): self
    {
        $this->replistaexptipoprocesonomb9 = $replistaexptipoprocesonomb9;

        return $this;
    }

    public function getReplistaexptipoprocesodescrip9(): ?string
    {
        return $this->replistaexptipoprocesodescrip9;
    }

    public function setReplistaexptipoprocesodescrip9(string $replistaexptipoprocesodescrip9): self
    {
        $this->replistaexptipoprocesodescrip9 = $replistaexptipoprocesodescrip9;

        return $this;
    }

    public function getReplistaexpcodigocurso10(): ?string
    {
        return $this->replistaexpcodigocurso10;
    }

    public function setReplistaexpcodigocurso10(string $replistaexpcodigocurso10): self
    {
        $this->replistaexpcodigocurso10 = $replistaexpcodigocurso10;

        return $this;
    }

    public function getReplistaexpnombcurso10(): ?string
    {
        return $this->replistaexpnombcurso10;
    }

    public function setReplistaexpnombcurso10(string $replistaexpnombcurso10): self
    {
        $this->replistaexpnombcurso10 = $replistaexpnombcurso10;

        return $this;
    }

    public function getReplistaexpproforestpronum10(): ?int
    {
        return $this->replistaexpproforestpronum10;
    }

    public function setReplistaexpproforestpronum10(int $replistaexpproforestpronum10): self
    {
        $this->replistaexpproforestpronum10 = $replistaexpproforestpronum10;

        return $this;
    }

    public function getReplistaexpproforestprodesc10(): ?string
    {
        return $this->replistaexpproforestprodesc10;
    }

    public function setReplistaexpproforestprodesc10(string $replistaexpproforestprodesc10): self
    {
        $this->replistaexpproforestprodesc10 = $replistaexpproforestprodesc10;

        return $this;
    }

    public function getReplistaexpproceformfecins10(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecins10;
    }

    public function setReplistaexpproceformfecins10(\DateTimeInterface $replistaexpproceformfecins10): self
    {
        $this->replistaexpproceformfecins10 = $replistaexpproceformfecins10;

        return $this;
    }

    public function getReplistaexpproceformfecfinal10(): ?\DateTimeInterface
    {
        return $this->replistaexpproceformfecfinal10;
    }

    public function setReplistaexpproceformfecfinal10(\DateTimeInterface $replistaexpproceformfecfinal10): self
    {
        $this->replistaexpproceformfecfinal10 = $replistaexpproceformfecfinal10;

        return $this;
    }

    public function getReplistaexptipoprocesoid10(): ?int
    {
        return $this->replistaexptipoprocesoid10;
    }

    public function setReplistaexptipoprocesoid10(int $replistaexptipoprocesoid10): self
    {
        $this->replistaexptipoprocesoid10 = $replistaexptipoprocesoid10;

        return $this;
    }

    public function getReplistaexptipoprocesonomb10(): ?string
    {
        return $this->replistaexptipoprocesonomb10;
    }

    public function setReplistaexptipoprocesonomb10(string $replistaexptipoprocesonomb10): self
    {
        $this->replistaexptipoprocesonomb10 = $replistaexptipoprocesonomb10;

        return $this;
    }

    public function getReplistaexptipoprocesodescrip1(): ?string
    {
        return $this->replistaexptipoprocesodescrip1;
    }

    public function setReplistaexptipoprocesodescrip1(string $replistaexptipoprocesodescrip1): self
    {
        $this->replistaexptipoprocesodescrip1 = $replistaexptipoprocesodescrip1;

        return $this;
    }

    public function getReplistaexptipintdeprecrid(): ?int
    {
        return $this->replistaexptipintdeprecrid;
    }

    public function setReplistaexptipintdeprecrid(int $replistaexptipintdeprecrid): self
    {
        $this->replistaexptipintdeprecrid = $replistaexptipintdeprecrid;

        return $this;
    }

    public function getReplistaexptipintdeprecrdesc(): ?string
    {
        return $this->replistaexptipintdeprecrdesc;
    }

    public function setReplistaexptipintdeprecrdesc(string $replistaexptipintdeprecrdesc): self
    {
        $this->replistaexptipintdeprecrdesc = $replistaexptipintdeprecrdesc;

        return $this;
    }

    public function getReplistaexpareaintdeprecrid(): ?int
    {
        return $this->replistaexpareaintdeprecrid;
    }

    public function setReplistaexpareaintdeprecrid(int $replistaexpareaintdeprecrid): self
    {
        $this->replistaexpareaintdeprecrid = $replistaexpareaintdeprecrid;

        return $this;
    }

    public function getReplistaexpareaintdeprecrdesc(): ?string
    {
        return $this->replistaexpareaintdeprecrdesc;
    }

    public function setReplistaexpareaintdeprecrdesc(string $replistaexpareaintdeprecrdesc): self
    {
        $this->replistaexpareaintdeprecrdesc = $replistaexpareaintdeprecrdesc;

        return $this;
    }

    public function getReplistaexptipintmunlabid(): ?int
    {
        return $this->replistaexptipintmunlabid;
    }

    public function setReplistaexptipintmunlabid(int $replistaexptipintmunlabid): self
    {
        $this->replistaexptipintmunlabid = $replistaexptipintmunlabid;

        return $this;
    }

    public function getReplistaexptipintmunlabdesc(): ?string
    {
        return $this->replistaexptipintmunlabdesc;
    }

    public function setReplistaexptipintmunlabdesc(string $replistaexptipintmunlabdesc): self
    {
        $this->replistaexptipintmunlabdesc = $replistaexptipintmunlabdesc;

        return $this;
    }

    public function getReplistaexpareaintmunlabid(): ?int
    {
        return $this->replistaexpareaintmunlabid;
    }

    public function setReplistaexpareaintmunlabid(int $replistaexpareaintmunlabid): self
    {
        $this->replistaexpareaintmunlabid = $replistaexpareaintmunlabid;

        return $this;
    }

    public function getReplistaexpareaintmunlabdesc(): ?string
    {
        return $this->replistaexpareaintmunlabdesc;
    }

    public function setReplistaexpareaintmunlabdesc(string $replistaexpareaintmunlabdesc): self
    {
        $this->replistaexpareaintmunlabdesc = $replistaexpareaintmunlabdesc;

        return $this;
    }

    public function getReplistaexptipintotrotemid(): ?int
    {
        return $this->replistaexptipintotrotemid;
    }

    public function setReplistaexptipintotrotemid(int $replistaexptipintotrotemid): self
    {
        $this->replistaexptipintotrotemid = $replistaexptipintotrotemid;

        return $this;
    }

    public function getReplistaexptipintotrotemdesc(): ?string
    {
        return $this->replistaexptipintotrotemdesc;
    }

    public function setReplistaexptipintotrotemdesc(string $replistaexptipintotrotemdesc): self
    {
        $this->replistaexptipintotrotemdesc = $replistaexptipintotrotemdesc;

        return $this;
    }

    public function getReplistaexpareaintotrotemid(): ?int
    {
        return $this->replistaexpareaintotrotemid;
    }

    public function setReplistaexpareaintotrotemid(int $replistaexpareaintotrotemid): self
    {
        $this->replistaexpareaintotrotemid = $replistaexpareaintotrotemid;

        return $this;
    }

    public function getReplistaexpareaintotrotemdesc(): ?string
    {
        return $this->replistaexpareaintotrotemdesc;
    }

    public function setReplistaexpareaintotrotemdesc(string $replistaexpareaintotrotemdesc): self
    {
        $this->replistaexpareaintotrotemdesc = $replistaexpareaintotrotemdesc;

        return $this;
    }

    public function getReplistaexpintpriodeprecr(): ?int
    {
        return $this->replistaexpintpriodeprecr;
    }

    public function setReplistaexpintpriodeprecr(int $replistaexpintpriodeprecr): self
    {
        $this->replistaexpintpriodeprecr = $replistaexpintpriodeprecr;

        return $this;
    }

    public function getReplistaexpintpriomunlab(): ?int
    {
        return $this->replistaexpintpriomunlab;
    }

    public function setReplistaexpintpriomunlab(int $replistaexpintpriomunlab): self
    {
        $this->replistaexpintpriomunlab = $replistaexpintpriomunlab;

        return $this;
    }

    public function getReplistaexpintpriootrotem(): ?int
    {
        return $this->replistaexpintpriootrotem;
    }

    public function setReplistaexpintpriootrotem(int $replistaexpintpriootrotem): self
    {
        $this->replistaexpintpriootrotem = $replistaexpintpriootrotem;

        return $this;
    }


}
