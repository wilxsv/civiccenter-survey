<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reporteprocform
 *
 * @ORM\Table(name="ReporteProcForm")
 * @ORM\Entity
 */
class Reporteprocform
{
    /**
     * @var int
     *
     * @ORM\Column(name="ReporteProcFormID", type="integer", nullable=false, options={"comment"="Llave Primaria representada por el campo ReporteProcFormID"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $reporteprocformid;

    /**
     * @var string
     *
     * @ORM\Column(name="ReporteProcFormCCP", type="string", length=100, nullable=false, options={"comment"="Registra las Siglas del Centro Cívico a la cual pertenece el proceso formativo."})
     */
    private $reporteprocformccp;

    /**
     * @var string
     *
     * @ORM\Column(name="ReporteProcFormCPPNombre", type="string", length=100, nullable=false, options={"comment"="Almacena el Nombre del Centro Cívico a la cual pertenece el proceso formativo."})
     */
    private $reporteprocformcppnombre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ReporteProcFormFecInicioInsc", type="datetime", nullable=false, options={"comment"="Guarda la fecha de inicio del proceso formativo."})
     */
    private $reporteprocformfecinicioinsc;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ReporteProcFormFecFinalInsc", type="datetime", nullable=false, options={"comment"="Mantiene la fecha de finalización del proceso formativo."})
     */
    private $reporteprocformfecfinalinsc;

    /**
     * @var string
     *
     * @ORM\Column(name="ReporteProcFormCodCurso", type="string", length=100, nullable=false, options={"comment"="Conserva el Codigo del Curso del Proceso Formativo."})
     */
    private $reporteprocformcodcurso;

    /**
     * @var string
     *
     * @ORM\Column(name="ReporteProcFormNomCurso", type="string", length=100, nullable=false, options={"comment"="Contiene el nombre del Proceso Formativo."})
     */
    private $reporteprocformnomcurso;

    /**
     * @var int
     *
     * @ORM\Column(name="ReporteProcFormTipoProcID", type="smallint", nullable=false, options={"comment"="Preserva el Tipo de Proceso Formativo."})
     */
    private $reporteprocformtipoprocid;

    /**
     * @var string
     *
     * @ORM\Column(name="ReporteProcFormTipoProcNombre", type="string", length=100, nullable=false, options={"comment"="Registra el Nombre del Tipo de Proceso Formativo."})
     */
    private $reporteprocformtipoprocnombre;

    /**
     * @var string
     *
     * @ORM\Column(name="ReporteProcFormTipoProcDescrip", type="string", length=100, nullable=false, options={"comment"="Almacena la Descripción del Tipo de Proceso Formativo. "})
     */
    private $reporteprocformtipoprocdescrip;

    /**
     * @var int
     *
     * @ORM\Column(name="ReporteProcFormInstitucID", type="smallint", nullable=false, options={"comment"="Guarda el Número que identifica la Insitución que brindo el Proceso Formativo."})
     */
    private $reporteprocforminstitucid;

    /**
     * @var string
     *
     * @ORM\Column(name="ReporteProcFormInstitucDescrip", type="string", length=100, nullable=false, options={"comment"="Contiene el Nombre de la Institución."})
     */
    private $reporteprocforminstitucdescrip;

    /**
     * @var string
     *
     * @ORM\Column(name="ReporteProcFormInstitucSiglas", type="string", length=100, nullable=false, options={"comment"="Conserva las Siglas de la Institución."})
     */
    private $reporteprocforminstitucsiglas;

    /**
     * @var int
     *
     * @ORM\Column(name="ReporteProcFormEdad", type="smallint", nullable=false, options={"comment"="Preserva la información de la edad de las personas que se encuentran en el proceso formativo."})
     */
    private $reporteprocformedad;

    /**
     * @var string
     *
     * @ORM\Column(name="ReporteProcFormRangoEdad", type="string", length=100, nullable=false, options={"comment"="Mantiene el rango de edad de las personas que se encuentran en un proceso formativo."})
     */
    private $reporteprocformrangoedad;

    /**
     * @var int
     *
     * @ORM\Column(name="ReporteProcFormSexoID", type="smallint", nullable=false, options={"comment"="Registra el número de idetificación para el género de la persona."})
     */
    private $reporteprocformsexoid;

    /**
     * @var string
     *
     * @ORM\Column(name="ReporteProcFormSexo", type="string", length=100, nullable=false, options={"comment"="Almacena la descripción del género."})
     */
    private $reporteprocformsexo;

    /**
     * @var string
     *
     * @ORM\Column(name="ReporteProcFormEstado", type="string", length=100, nullable=false, options={"comment"="Guarda el estado en el cuál se encuentra dentro del proceso formativo."})
     */
    private $reporteprocformestado;

    public function getReporteprocformid(): ?int
    {
        return $this->reporteprocformid;
    }

    public function getReporteprocformccp(): ?string
    {
        return $this->reporteprocformccp;
    }

    public function setReporteprocformccp(string $reporteprocformccp): self
    {
        $this->reporteprocformccp = $reporteprocformccp;

        return $this;
    }

    public function getReporteprocformcppnombre(): ?string
    {
        return $this->reporteprocformcppnombre;
    }

    public function setReporteprocformcppnombre(string $reporteprocformcppnombre): self
    {
        $this->reporteprocformcppnombre = $reporteprocformcppnombre;

        return $this;
    }

    public function getReporteprocformfecinicioinsc(): ?\DateTimeInterface
    {
        return $this->reporteprocformfecinicioinsc;
    }

    public function setReporteprocformfecinicioinsc(\DateTimeInterface $reporteprocformfecinicioinsc): self
    {
        $this->reporteprocformfecinicioinsc = $reporteprocformfecinicioinsc;

        return $this;
    }

    public function getReporteprocformfecfinalinsc(): ?\DateTimeInterface
    {
        return $this->reporteprocformfecfinalinsc;
    }

    public function setReporteprocformfecfinalinsc(\DateTimeInterface $reporteprocformfecfinalinsc): self
    {
        $this->reporteprocformfecfinalinsc = $reporteprocformfecfinalinsc;

        return $this;
    }

    public function getReporteprocformcodcurso(): ?string
    {
        return $this->reporteprocformcodcurso;
    }

    public function setReporteprocformcodcurso(string $reporteprocformcodcurso): self
    {
        $this->reporteprocformcodcurso = $reporteprocformcodcurso;

        return $this;
    }

    public function getReporteprocformnomcurso(): ?string
    {
        return $this->reporteprocformnomcurso;
    }

    public function setReporteprocformnomcurso(string $reporteprocformnomcurso): self
    {
        $this->reporteprocformnomcurso = $reporteprocformnomcurso;

        return $this;
    }

    public function getReporteprocformtipoprocid(): ?int
    {
        return $this->reporteprocformtipoprocid;
    }

    public function setReporteprocformtipoprocid(int $reporteprocformtipoprocid): self
    {
        $this->reporteprocformtipoprocid = $reporteprocformtipoprocid;

        return $this;
    }

    public function getReporteprocformtipoprocnombre(): ?string
    {
        return $this->reporteprocformtipoprocnombre;
    }

    public function setReporteprocformtipoprocnombre(string $reporteprocformtipoprocnombre): self
    {
        $this->reporteprocformtipoprocnombre = $reporteprocformtipoprocnombre;

        return $this;
    }

    public function getReporteprocformtipoprocdescrip(): ?string
    {
        return $this->reporteprocformtipoprocdescrip;
    }

    public function setReporteprocformtipoprocdescrip(string $reporteprocformtipoprocdescrip): self
    {
        $this->reporteprocformtipoprocdescrip = $reporteprocformtipoprocdescrip;

        return $this;
    }

    public function getReporteprocforminstitucid(): ?int
    {
        return $this->reporteprocforminstitucid;
    }

    public function setReporteprocforminstitucid(int $reporteprocforminstitucid): self
    {
        $this->reporteprocforminstitucid = $reporteprocforminstitucid;

        return $this;
    }

    public function getReporteprocforminstitucdescrip(): ?string
    {
        return $this->reporteprocforminstitucdescrip;
    }

    public function setReporteprocforminstitucdescrip(string $reporteprocforminstitucdescrip): self
    {
        $this->reporteprocforminstitucdescrip = $reporteprocforminstitucdescrip;

        return $this;
    }

    public function getReporteprocforminstitucsiglas(): ?string
    {
        return $this->reporteprocforminstitucsiglas;
    }

    public function setReporteprocforminstitucsiglas(string $reporteprocforminstitucsiglas): self
    {
        $this->reporteprocforminstitucsiglas = $reporteprocforminstitucsiglas;

        return $this;
    }

    public function getReporteprocformedad(): ?int
    {
        return $this->reporteprocformedad;
    }

    public function setReporteprocformedad(int $reporteprocformedad): self
    {
        $this->reporteprocformedad = $reporteprocformedad;

        return $this;
    }

    public function getReporteprocformrangoedad(): ?string
    {
        return $this->reporteprocformrangoedad;
    }

    public function setReporteprocformrangoedad(string $reporteprocformrangoedad): self
    {
        $this->reporteprocformrangoedad = $reporteprocformrangoedad;

        return $this;
    }

    public function getReporteprocformsexoid(): ?int
    {
        return $this->reporteprocformsexoid;
    }

    public function setReporteprocformsexoid(int $reporteprocformsexoid): self
    {
        $this->reporteprocformsexoid = $reporteprocformsexoid;

        return $this;
    }

    public function getReporteprocformsexo(): ?string
    {
        return $this->reporteprocformsexo;
    }

    public function setReporteprocformsexo(string $reporteprocformsexo): self
    {
        $this->reporteprocformsexo = $reporteprocformsexo;

        return $this;
    }

    public function getReporteprocformestado(): ?string
    {
        return $this->reporteprocformestado;
    }

    public function setReporteprocformestado(string $reporteprocformestado): self
    {
        $this->reporteprocformestado = $reporteprocformestado;

        return $this;
    }


}
