<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Residencia
 *
 * @ORM\Table(name="Residencia", indexes={@ORM\Index(name="IRESIDENCIA1", columns={"DistritoID"}), @ORM\Index(name="IRESIDENCIA2", columns={"ExpedienteNumero"})})
 * @ORM\Entity
 */
class Residencia
{
    /**
     * @var int
     *
     * @ORM\Column(name="ResidenciaID", type="integer", nullable=false, options={"comment"="Llave Primaria representada por el campo de ResidenciaID."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $residenciaid;

    /**
     * @var string
     *
     * @ORM\Column(name="ResidenciaBarrio", type="string", length=50, nullable=false, options={"comment"="Guarda el barrio donde vive la persona."})
     */
    private $residenciabarrio;

    /**
     * @var int
     *
     * @ORM\Column(name="ResidenciaCantPersonas", type="smallint", nullable=false, options={"comment"="Contiene la cantidad de personas que habitan en el lugar donde residen."})
     */
    private $residenciacantpersonas;

    /**
     * @var string
     *
     * @ORM\Column(name="ResidenciaParentesco", type="string", length=255, nullable=false, options={"comment"="Mantiene la información del parentesco de las personas que viven en la residencia."})
     */
    private $residenciaparentesco;

    /**
     * @var string
     *
     * @ORM\Column(name="ResidenciaOtraSenas", type="string", length=255, nullable=false, options={"comment"="Conserva la información de otras señas de la dirección donde vive."})
     */
    private $residenciaotrasenas;

    /**
     * @var bool
     *
     * @ORM\Column(name="ResidenciaEstado", type="boolean", nullable=false, options={"comment"="Preserva el estado del lugar de residencia de la persona, para indicar cual es el actual."})
     */
    private $residenciaestado;

    /**
     * @var int
     *
     * @ORM\Column(name="ResidenciaAreaInfluencia", type="integer", nullable=false, options={"comment"="Almacena la respuesta si el lugar donde vive se encuentra dentro del área de influencia del Centro Cívico."})
     */
    private $residenciaareainfluencia;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ResidenciaFecCrea", type="datetime", nullable=false, options={"comment"="Registra la fecha en que se crea el registro."})
     */
    private $residenciafeccrea;

    /**
     * @var string
     *
     * @ORM\Column(name="ResidenciaUsrCrea", type="string", length=255, nullable=false, options={"comment"="Guarda el usuario que crea el registro."})
     */
    private $residenciausrcrea;

    /**
     * @var \Distrito
     *
     * @ORM\ManyToOne(targetEntity="Distrito")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="DistritoID", referencedColumnName="DistritoID")
     * })
     */
    private $distritoid;

    /**
     * @var \Expediente
     *
     * @ORM\ManyToOne(targetEntity="Expediente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ExpedienteNumero", referencedColumnName="ExpedienteNumero")
     * })
     */
    private $expedientenumero;

    public function getResidenciaid(): ?int
    {
        return $this->residenciaid;
    }

    public function getResidenciabarrio(): ?string
    {
        return $this->residenciabarrio;
    }

    public function setResidenciabarrio(string $residenciabarrio): self
    {
        $this->residenciabarrio = $residenciabarrio;

        return $this;
    }

    public function getResidenciacantpersonas(): ?int
    {
        return $this->residenciacantpersonas;
    }

    public function setResidenciacantpersonas(int $residenciacantpersonas): self
    {
        $this->residenciacantpersonas = $residenciacantpersonas;

        return $this;
    }

    public function getResidenciaparentesco(): ?string
    {
        return $this->residenciaparentesco;
    }

    public function setResidenciaparentesco(string $residenciaparentesco): self
    {
        $this->residenciaparentesco = $residenciaparentesco;

        return $this;
    }

    public function getResidenciaotrasenas(): ?string
    {
        return $this->residenciaotrasenas;
    }

    public function setResidenciaotrasenas(string $residenciaotrasenas): self
    {
        $this->residenciaotrasenas = $residenciaotrasenas;

        return $this;
    }

    public function getResidenciaestado(): ?bool
    {
        return $this->residenciaestado;
    }

    public function setResidenciaestado(bool $residenciaestado): self
    {
        $this->residenciaestado = $residenciaestado;

        return $this;
    }

    public function getResidenciaareainfluencia(): ?int
    {
        return $this->residenciaareainfluencia;
    }

    public function setResidenciaareainfluencia(int $residenciaareainfluencia): self
    {
        $this->residenciaareainfluencia = $residenciaareainfluencia;

        return $this;
    }

    public function getResidenciafeccrea(): ?\DateTimeInterface
    {
        return $this->residenciafeccrea;
    }

    public function setResidenciafeccrea(\DateTimeInterface $residenciafeccrea): self
    {
        $this->residenciafeccrea = $residenciafeccrea;

        return $this;
    }

    public function getResidenciausrcrea(): ?string
    {
        return $this->residenciausrcrea;
    }

    public function setResidenciausrcrea(string $residenciausrcrea): self
    {
        $this->residenciausrcrea = $residenciausrcrea;

        return $this;
    }

    public function getDistritoid(): ?Distrito
    {
        return $this->distritoid;
    }

    public function setDistritoid(?Distrito $distritoid): self
    {
        $this->distritoid = $distritoid;

        return $this;
    }

    public function getExpedientenumero(): ?Expediente
    {
        return $this->expedientenumero;
    }

    public function setExpedientenumero(?Expediente $expedientenumero): self
    {
        $this->expedientenumero = $expedientenumero;

        return $this;
    }


}
