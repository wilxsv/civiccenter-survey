<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Responsable
 *
 * @ORM\Table(name="Responsable", indexes={@ORM\Index(name="IRESPONSABLE1", columns={"ExpedienteNumero"})})
 * @ORM\Entity
 */
class Responsable
{
    /**
     * @var int
     *
     * @ORM\Column(name="ResponsableID", type="integer", nullable=false, options={"comment"="Llave Primaria compuesta por el ResponsableID y ExpedienteNumero."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $responsableid;

    /**
     * @var string
     *
     * @ORM\Column(name="ResponsableNombre1", type="string", length=50, nullable=false, options={"comment"="Guarda el Nombre del Responsable."})
     */
    private $responsablenombre1;

    /**
     * @var string
     *
     * @ORM\Column(name="ResponsableApellido1", type="string", length=50, nullable=false, options={"comment"="Mantiene el Primer Apellido del Responsable."})
     */
    private $responsableapellido1;

    /**
     * @var string
     *
     * @ORM\Column(name="ResponsableApellido2", type="string", length=50, nullable=false, options={"comment"="Conserva el Segundo Apellido del Responsable."})
     */
    private $responsableapellido2;

    /**
     * @var string
     *
     * @ORM\Column(name="ResponsableParentesco", type="string", length=100, nullable=false, options={"comment"="Preserva el Parentezco de la persona Responsable."})
     */
    private $responsableparentesco;

    /**
     * @var string
     *
     * @ORM\Column(name="ResponsableEMail", type="string", length=40, nullable=false, options={"comment"="Contiene el correo de la persona responsable."})
     */
    private $responsableemail;

    /**
     * @var string
     *
     * @ORM\Column(name="ResponsableIdentif", type="string", length=15, nullable=false, options={"comment"="Registra el número de identificación de la persona Responsable."})
     */
    private $responsableidentif;

    /**
     * @var string
     *
     * @ORM\Column(name="ResponsableTelCasa", type="string", length=8, nullable=false, options={"comment"="Almacena el teléfono de hogar del Responsable."})
     */
    private $responsabletelcasa;

    /**
     * @var string
     *
     * @ORM\Column(name="ResponsableTelCel", type="string", length=8, nullable=false, options={"comment"="Guarda el Número de Celular de la persona Responsable."})
     */
    private $responsabletelcel;

    /**
     * @var string
     *
     * @ORM\Column(name="ResponsableTelTrab", type="string", length=8, nullable=false, options={"comment"="Mantiene el Número de Trabajo de la persona Responsable."})
     */
    private $responsableteltrab;

    /**
     * @var bool
     *
     * @ORM\Column(name="ResponsableActivo", type="boolean", nullable=false, options={"comment"="Conserva el estado del Responsable para identifcar cual es el que está activo."})
     */
    private $responsableactivo;

    /**
     * @var string
     *
     * @ORM\Column(name="ResponsableUsrCrea", type="string", length=255, nullable=false, options={"comment"="Contiene el usuario que crea el registro."})
     */
    private $responsableusrcrea;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ResponsableFecCrea", type="datetime", nullable=false, options={"comment"="Preserva la fecha de cuando se crea el registro."})
     */
    private $responsablefeccrea;

    /**
     * @var string
     *
     * @ORM\Column(name="ResponsableUsrMod", type="string", length=255, nullable=false, options={"comment"="Registra el usuario que realiza modificaciones en cada registro."})
     */
    private $responsableusrmod;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ResponsableFecMod", type="datetime", nullable=false, options={"comment"="Almacena la fecha en que se realiza la modificación."})
     */
    private $responsablefecmod;

    /**
     * @var \Expediente
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Expediente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ExpedienteNumero", referencedColumnName="ExpedienteNumero")
     * })
     */
    private $expedientenumero;

    public function getResponsableid(): ?int
    {
        return $this->responsableid;
    }

    public function getResponsablenombre1(): ?string
    {
        return $this->responsablenombre1;
    }

    public function setResponsablenombre1(string $responsablenombre1): self
    {
        $this->responsablenombre1 = $responsablenombre1;

        return $this;
    }

    public function getResponsableapellido1(): ?string
    {
        return $this->responsableapellido1;
    }

    public function setResponsableapellido1(string $responsableapellido1): self
    {
        $this->responsableapellido1 = $responsableapellido1;

        return $this;
    }

    public function getResponsableapellido2(): ?string
    {
        return $this->responsableapellido2;
    }

    public function setResponsableapellido2(string $responsableapellido2): self
    {
        $this->responsableapellido2 = $responsableapellido2;

        return $this;
    }

    public function getResponsableparentesco(): ?string
    {
        return $this->responsableparentesco;
    }

    public function setResponsableparentesco(string $responsableparentesco): self
    {
        $this->responsableparentesco = $responsableparentesco;

        return $this;
    }

    public function getResponsableemail(): ?string
    {
        return $this->responsableemail;
    }

    public function setResponsableemail(string $responsableemail): self
    {
        $this->responsableemail = $responsableemail;

        return $this;
    }

    public function getResponsableidentif(): ?string
    {
        return $this->responsableidentif;
    }

    public function setResponsableidentif(string $responsableidentif): self
    {
        $this->responsableidentif = $responsableidentif;

        return $this;
    }

    public function getResponsabletelcasa(): ?string
    {
        return $this->responsabletelcasa;
    }

    public function setResponsabletelcasa(string $responsabletelcasa): self
    {
        $this->responsabletelcasa = $responsabletelcasa;

        return $this;
    }

    public function getResponsabletelcel(): ?string
    {
        return $this->responsabletelcel;
    }

    public function setResponsabletelcel(string $responsabletelcel): self
    {
        $this->responsabletelcel = $responsabletelcel;

        return $this;
    }

    public function getResponsableteltrab(): ?string
    {
        return $this->responsableteltrab;
    }

    public function setResponsableteltrab(string $responsableteltrab): self
    {
        $this->responsableteltrab = $responsableteltrab;

        return $this;
    }

    public function getResponsableactivo(): ?bool
    {
        return $this->responsableactivo;
    }

    public function setResponsableactivo(bool $responsableactivo): self
    {
        $this->responsableactivo = $responsableactivo;

        return $this;
    }

    public function getResponsableusrcrea(): ?string
    {
        return $this->responsableusrcrea;
    }

    public function setResponsableusrcrea(string $responsableusrcrea): self
    {
        $this->responsableusrcrea = $responsableusrcrea;

        return $this;
    }

    public function getResponsablefeccrea(): ?\DateTimeInterface
    {
        return $this->responsablefeccrea;
    }

    public function setResponsablefeccrea(\DateTimeInterface $responsablefeccrea): self
    {
        $this->responsablefeccrea = $responsablefeccrea;

        return $this;
    }

    public function getResponsableusrmod(): ?string
    {
        return $this->responsableusrmod;
    }

    public function setResponsableusrmod(string $responsableusrmod): self
    {
        $this->responsableusrmod = $responsableusrmod;

        return $this;
    }

    public function getResponsablefecmod(): ?\DateTimeInterface
    {
        return $this->responsablefecmod;
    }

    public function setResponsablefecmod(\DateTimeInterface $responsablefecmod): self
    {
        $this->responsablefecmod = $responsablefecmod;

        return $this;
    }

    public function getExpedientenumero(): ?Expediente
    {
        return $this->expedientenumero;
    }

    public function setExpedientenumero(?Expediente $expedientenumero): self
    {
        $this->expedientenumero = $expedientenumero;

        return $this;
    }


}
