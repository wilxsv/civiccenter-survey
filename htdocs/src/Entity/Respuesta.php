<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Respuesta
 *
 * @ORM\Table(name="Respuesta")
 * @ORM\Entity
 */
class Respuesta
{
    /**
     * @var int
     *
     * @ORM\Column(name="RespuestaID", type="integer", nullable=false, options={"comment"="Llave Primaria que se representa con el campo RespuestaID."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $respuestaid;

    /**
     * @var string
     *
     * @ORM\Column(name="RespuestaDescripcion", type="string", length=300, nullable=false, options={"comment"="Registra la descripción de las respuestas."})
     */
    private $respuestadescripcion;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Pregunta", mappedBy="respuestaid")
     */
    private $preguntaid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->preguntaid = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getRespuestaid(): ?int
    {
        return $this->respuestaid;
    }

    public function getRespuestadescripcion(): ?string
    {
        return $this->respuestadescripcion;
    }

    public function setRespuestadescripcion(string $respuestadescripcion): self
    {
        $this->respuestadescripcion = $respuestadescripcion;

        return $this;
    }

    /**
     * @return Collection|Pregunta[]
     */
    public function getPreguntaid(): Collection
    {
        return $this->preguntaid;
    }

    public function addPreguntaid(Pregunta $preguntaid): self
    {
        if (!$this->preguntaid->contains($preguntaid)) {
            $this->preguntaid[] = $preguntaid;
            $preguntaid->addRespuestaid($this);
        }

        return $this;
    }

    public function removePreguntaid(Pregunta $preguntaid): self
    {
        if ($this->preguntaid->contains($preguntaid)) {
            $this->preguntaid->removeElement($preguntaid);
            $preguntaid->removeRespuestaid($this);
        }

        return $this;
    }

}
