<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rol
 *
 * @ORM\Table(name="Rol", indexes={@ORM\Index(name="IROL1", columns={"UsuarioID"})})
 * @ORM\Entity
 */
class Rol
{
    /**
     * @var int
     *
     * @ORM\Column(name="RolID", type="integer", nullable=false, options={"comment"="Llave Primaria representada por el campo RolID"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $rolid;

    /**
     * @var int
     *
     * @ORM\Column(name="RolCatRolID", type="smallint", nullable=false, options={"comment"="Almacena el Número Consecutivo que identifca el rol asociado al usuario."})
     */
    private $rolcatrolid;

    /**
     * @var string
     *
     * @ORM\Column(name="RolDescripcion", type="string", length=150, nullable=false, options={"comment"="Guarda la descripción del rol asignado."})
     */
    private $roldescripcion;

    /**
     * @var \Usuario
     *
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="UsuarioID", referencedColumnName="UsuarioID")
     * })
     */
    private $usuarioid;

    public function getRolid(): ?int
    {
        return $this->rolid;
    }

    public function getRolcatrolid(): ?int
    {
        return $this->rolcatrolid;
    }

    public function setRolcatrolid(int $rolcatrolid): self
    {
        $this->rolcatrolid = $rolcatrolid;

        return $this;
    }

    public function getRoldescripcion(): ?string
    {
        return $this->roldescripcion;
    }

    public function setRoldescripcion(string $roldescripcion): self
    {
        $this->roldescripcion = $roldescripcion;

        return $this;
    }

    public function getUsuarioid(): ?Usuario
    {
        return $this->usuarioid;
    }

    public function setUsuarioid(?Usuario $usuarioid): self
    {
        $this->usuarioid = $usuarioid;

        return $this;
    }


}
