<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sectormep
 *
 * @ORM\Table(name="SectorMEP")
 * @ORM\Entity
 */
class Sectormep
{
    /**
     * @var int
     *
     * @ORM\Column(name="SectorMEPId", type="integer", nullable=false, options={"comment"="Llave Primaria representada por el campo SectorMEPId."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $sectormepid;

    /**
     * @var string
     *
     * @ORM\Column(name="SectorMEPDescripcion", type="string", length=300, nullable=false, options={"comment"="Almacena la descripción del Sector del MEP."})
     */
    private $sectormepdescripcion;

    public function getSectormepid(): ?int
    {
        return $this->sectormepid;
    }

    public function getSectormepdescripcion(): ?string
    {
        return $this->sectormepdescripcion;
    }

    public function setSectormepdescripcion(string $sectormepdescripcion): self
    {
        $this->sectormepdescripcion = $sectormepdescripcion;

        return $this;
    }


}
