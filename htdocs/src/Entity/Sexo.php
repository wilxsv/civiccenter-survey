<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sexo
 *
 * @ORM\Table(name="Sexo")
 * @ORM\Entity
 */
class Sexo
{
    /**
     * @var int
     *
     * @ORM\Column(name="SexoID", type="integer", nullable=false, options={"comment"="Llave Primaria representada por el campo de SexoID."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $sexoid;

    /**
     * @var string
     *
     * @ORM\Column(name="SexoDescripcion", type="string", length=300, nullable=false, options={"comment"="Almacena la descripción de cada registro."})
     */
    private $sexodescripcion;

    /**
     * @var bool
     *
     * @ORM\Column(name="SexoValido", type="boolean", nullable=false, options={"comment"="Registra el estado del registro, si esta activo se utiliza un 1, en otro caso un 0."})
     */
    private $sexovalido;

    public function getSexoid(): ?int
    {
        return $this->sexoid;
    }

    public function getSexodescripcion(): ?string
    {
        return $this->sexodescripcion;
    }

    public function setSexodescripcion(string $sexodescripcion): self
    {
        $this->sexodescripcion = $sexodescripcion;

        return $this;
    }

    public function getSexovalido(): ?bool
    {
        return $this->sexovalido;
    }

    public function setSexovalido(bool $sexovalido): self
    {
        $this->sexovalido = $sexovalido;

        return $this;
    }


}
