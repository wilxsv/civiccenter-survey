<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tipointeres
 *
 * @ORM\Table(name="TipoInteres")
 * @ORM\Entity
 */
class Tipointeres
{
    /**
     * @var int
     *
     * @ORM\Column(name="TipoInteresID", type="integer", nullable=false, options={"comment"="Llave Primaria representada por el campo TipoInteresID."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tipointeresid;

    /**
     * @var string
     *
     * @ORM\Column(name="TipoInteresDescripcion", type="string", length=300, nullable=false, options={"comment"="Registra la descripción del tipo de interés."})
     */
    private $tipointeresdescripcion;

    public function getTipointeresid(): ?int
    {
        return $this->tipointeresid;
    }

    public function getTipointeresdescripcion(): ?string
    {
        return $this->tipointeresdescripcion;
    }

    public function setTipointeresdescripcion(string $tipointeresdescripcion): self
    {
        $this->tipointeresdescripcion = $tipointeresdescripcion;

        return $this;
    }


}
