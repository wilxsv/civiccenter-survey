<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tipoproceso
 *
 * @ORM\Table(name="TipoProceso")
 * @ORM\Entity
 */
class Tipoproceso
{
    /**
     * @var int
     *
     * @ORM\Column(name="TipoProcesoID", type="integer", nullable=false, options={"comment"="Llave Primaria representada por el campo TipoProcesoID."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tipoprocesoid;

    /**
     * @var string
     *
     * @ORM\Column(name="TipoProcesoNombre", type="string", length=300, nullable=false, options={"comment"="Registra el Nombre del Proceso Formativo."})
     */
    private $tipoprocesonombre;

    /**
     * @var string
     *
     * @ORM\Column(name="TipoProcesoDescrip", type="string", length=300, nullable=false, options={"comment"="Almacena la descripción del Proceso Formativo."})
     */
    private $tipoprocesodescrip;

    public function getTipoprocesoid(): ?int
    {
        return $this->tipoprocesoid;
    }

    public function getTipoprocesonombre(): ?string
    {
        return $this->tipoprocesonombre;
    }

    public function setTipoprocesonombre(string $tipoprocesonombre): self
    {
        $this->tipoprocesonombre = $tipoprocesonombre;

        return $this;
    }

    public function getTipoprocesodescrip(): ?string
    {
        return $this->tipoprocesodescrip;
    }

    public function setTipoprocesodescrip(string $tipoprocesodescrip): self
    {
        $this->tipoprocesodescrip = $tipoprocesodescrip;

        return $this;
    }


}
