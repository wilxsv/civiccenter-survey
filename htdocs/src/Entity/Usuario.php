<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usuario
 *
 * @ORM\Table(name="Usuario", indexes={@ORM\Index(name="IUSUARIO1", columns={"CentroCivicoID"})})
 * @ORM\Entity
 */
class Usuario
{
    /**
     * @var int
     *
     * @ORM\Column(name="UsuarioID", type="integer", nullable=false, options={"comment"="Llave Primaria que es representada por el campo de UsuarioID."})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $usuarioid;

    /**
     * @var string
     *
     * @ORM\Column(name="UsuarioLogin", type="string", length=15, nullable=false, options={"comment"="Registra el nombre de usuario con el cual va a ingresar al sistema."})
     */
    private $usuariologin;

    /**
     * @var string
     *
     * @ORM\Column(name="UsuarioPass", type="string", length=15, nullable=false, options={"comment"="Guarda la contraseña con la cual van a ingresar al sistema."})
     */
    private $usuariopass;

    /**
     * @var string
     *
     * @ORM\Column(name="UsuarioNombre", type="string", length=255, nullable=false, options={"comment"="Almacena el nombre de la persona que va a ingresar al sistema."})
     */
    private $usuarionombre;

    /**
     * @var bool
     *
     * @ORM\Column(name="UsuarioActivo", type="boolean", nullable=false, options={"comment"="Mantiene el dato si la persona esta activa dentro del sistema."})
     */
    private $usuarioactivo;

    /**
     * @var \Centrocivico
     *
     * @ORM\ManyToOne(targetEntity="Centrocivico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CentroCivicoID", referencedColumnName="CentroCivicoID")
     * })
     */
    private $centrocivicoid;

    public function getUsuarioid(): ?int
    {
        return $this->usuarioid;
    }

    public function getUsuariologin(): ?string
    {
        return $this->usuariologin;
    }

    public function setUsuariologin(string $usuariologin): self
    {
        $this->usuariologin = $usuariologin;

        return $this;
    }

    public function getUsuariopass(): ?string
    {
        return $this->usuariopass;
    }

    public function setUsuariopass(string $usuariopass): self
    {
        $this->usuariopass = $usuariopass;

        return $this;
    }

    public function getUsuarionombre(): ?string
    {
        return $this->usuarionombre;
    }

    public function setUsuarionombre(string $usuarionombre): self
    {
        $this->usuarionombre = $usuarionombre;

        return $this;
    }

    public function getUsuarioactivo(): ?bool
    {
        return $this->usuarioactivo;
    }

    public function setUsuarioactivo(bool $usuarioactivo): self
    {
        $this->usuarioactivo = $usuarioactivo;

        return $this;
    }

    public function getCentrocivicoid(): ?Centrocivico
    {
        return $this->centrocivicoid;
    }

    public function setCentrocivicoid(?Centrocivico $centrocivicoid): self
    {
        $this->centrocivicoid = $centrocivicoid;

        return $this;
    }


}
