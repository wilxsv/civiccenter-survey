USE CCP GO

CREATE OR ALTER VIEW VW_procesos_formacion AS
SELECT 
	1 'Total',
	DATEDIFF(YEAR, e.ExpedienteFecNac, GETDATE()) 'Edad actual',
	dbo.fnc_get_rango_edad( e.ExpedienteFecNac ) 'Rango de edad',
	s.SexoDescripcion 'Sexo', 
	n.NacionalidadDescripcion 'Nacionalidad',
	ne.NivelEducativoDescripcion 'Educaci�n del estudiante',
	c.CentroCivicoDescripcion 'Centro c�vico',
	(CASE WHEN e.ExpedienteHijos = 3 THEN 'Si' ELSE 'No' END) 'Tiene hijos/as',
	(CASE WHEN e.ExpedienteHijos = 3 THEN e.ExpedienteHijosTener ELSE 0 END) 'Cantidad de hijos/as',
	p.ProcesoFormativoNombre 'Curso',
	re.RespuestaDescripcion 'Estado',
	-- Construidas a partir de los historicos
	ISNULL ( ( SELECT TOP 1 (CASE WHEN ee.ExpEducacionActual = 3 THEN 'Si' ELSE 'No' END) FROM ExpEducacion ee WHERE ee.ExpedienteNumero = e.ExpedienteNumero ORDER BY ee.ExpEducacionUsrCrea ASC ), 'ND' ) 'Estudia actualmente',
	ISNULL ( ( SELECT TOP 1 (CASE WHEN et.ExpTrabajoSiNo = 3 THEN 'Si' ELSE 'No' END) FROM ExpTrabajo et WHERE et.ExpedienteNumero = e.ExpedienteNumero ORDER BY et.ExpTrabajoFecCrea ASC ), 'ND' ) 'Trabaja',
	ISNULL ( ( SELECT TOP 1 jt.JornadaTrabajoDescripcion FROM ExpTrabajo et INNER JOIN JornadaTrabajo jt ON jt.JornadaTrabajoID = et.JornadaTrabajoID WHERE et.ExpedienteNumero = e.ExpedienteNumero ORDER BY et.ExpTrabajoFecCrea ASC ), 'ND' ) 'Jornada laboral',
	ISNULL ( ( SELECT TOP 1 jt.AreaTrabajoDescripcion FROM ExpTrabajo et INNER JOIN AreaTrabajo jt ON jt.AreaTrabajoID = et.AreaTrabajoID WHERE et.ExpedienteNumero = e.ExpedienteNumero ORDER BY et.ExpTrabajoFecCrea ASC ), 'ND' ) 'Sector de trabajo',
	ISNULL ( ( SELECT TOP 1 ce.CentroEducNombre FROM ExpEducacion ee INNER JOIN CentroEduc ce on ce.CentroEducId = ee.CentroEducId WHERE ee.ExpedienteNumero = e.ExpedienteNumero ORDER BY ee.ExpEducacionUsrCrea ASC ), 'ND' ) 'Centro de estudios',
	ISNULL ( ( SELECT TOP 1 nm.NivelMEPDescripcion FROM ExpEducacion ee INNER JOIN CentroEduc ce on ce.CentroEducId = ee.CentroEducId INNER JOIN NivelMEP nm on nm.NivelMEPId = ce.NivelMEPId WHERE ee.ExpedienteNumero = e.ExpedienteNumero ORDER BY ee.ExpEducacionUsrCrea ASC ), 'ND' ) 'Modalidad',
	ISNULL ( ( SELECT TOP 1 d.DistritoDescripcion FROM Residencia r INNER JOIN Distrito d ON d.DistritoID = r.DistritoID WHERE  r.ExpedienteNumero = e.ExpedienteNumero ORDER BY r.ResidenciaFecCrea ASC ), 'ND' ) 'Distrito residencia',
	ISNULL ( ( SELECT TOP 1 c.CantonDescripcion FROM Residencia r INNER JOIN Distrito d ON d.DistritoID = r.DistritoID INNER JOIN Canton c ON c.CantonID = d.CantonID WHERE  r.ExpedienteNumero = e.ExpedienteNumero ORDER BY r.ResidenciaFecCrea ASC ), 'ND' ) 'Canton residencia', 
	ISNULL ( ( SELECT TOP 1 p.ProvinciaDescripcion FROM Residencia r INNER JOIN Distrito d ON d.DistritoID = r.DistritoID INNER JOIN Canton c ON c.CantonID = d.CantonID INNER JOIN Provincia p ON p.ProvinciaID = c.ProvinciaID WHERE r.ExpedienteNumero = e.ExpedienteNumero ORDER BY r.ResidenciaFecCrea ASC ), 'ND' ) 'Provincia residencia',
	-- Variables pendientes
    'ND' AS 'Reinserci�n educativa'
FROM ProcesoFormativo p INNER JOIN Expediente e ON e.ExpedienteNumero = p.ExpedienteNumero 
	INNER JOIN Sexo s ON s.SexoID = e.SexoID
	INNER JOIN Respuesta re ON re.RespuestaID = p.ProcesoFormativoEstadoProceso 
	INNER JOIN Nacionalidad n ON n.NacionalidadID = e.NacionalidadID
	INNER JOIN NivelEducativo ne ON ne.NivelEducativoID = e.ExpedienteNivelEducativo
	INNER JOIN CentroCivico c ON c.CentroCivicoSiglas = SUBSTRING( e.ExpedienteNumero , 1, 3 );
	
	SELECT * from VW_procesos_formacion vpf 

	/* 
	


 GO
*/

/*
 * 
 * 
	1 'Total', 
	DATEDIFF(YEAR, e.ExpedienteFecNac, GETDATE()) 'Edad actual', 
	dbo.fnc_get_rango_edad( e.ExpedienteFecNac ) 'Rango de edad',
	s.SexoDescripcion 'Sexo', 
	n.NacionalidadDescripcion 'Nacionalidad', 
	-- ISNULL ( ( SELECT c.CursoNombre FROM Curso c WHERE c.CursoCodigo = f.ProcesoFormativoCodigoCurso ), 'ND' ) 'Curso', 
	'ND' AS 'Curso',
	ISNULL ( ( SELECT cc.CentroCivicoDescripcion FROM CentroCivico cc WHERE cc.CentroCivicoSiglas = SUBSTRING( e.ExpedienteNumero , 1, 3 ) ), 'ND' ) 'Centro civico',
	ne.NivelEducativoDescripcion 'Educaci�n del estudiante',
	'ND' AS 'Reinserci�n educativa',
	ISNULL ( ( SELECT TOP 1 (CASE WHEN ee.ExpEducacionActual = 3 THEN 'Si' ELSE 'No' END) FROM ExpEducacion ee WHERE ee.ExpedienteNumero = e.ExpedienteNumero ORDER BY ee.ExpEducacionUsrCrea DESC), 'ND' ) 'Estudia actualmente',
	ISNULL ( ( SELECT TOP 1 (CASE WHEN et.ExpTrabajoSiNo = 3 THEN 'Si' ELSE 'No' END) FROM ExpTrabajo et WHERE et.ExpedienteNumero = e.ExpedienteNumero ORDER BY et.ExpTrabajoFecCrea DESC ), 'ND' ) 'Trabaja',
	ISNULL ( ( SELECT TOP 1 jt.JornadaTrabajoDescripcion FROM ExpTrabajo et INNER JOIN JornadaTrabajo jt ON jt.JornadaTrabajoID = et.JornadaTrabajoID WHERE et.ExpedienteNumero = e.ExpedienteNumero ORDER BY et.ExpTrabajoFecCrea DESC ), 'ND' ) 'Jornada laboral',
	ISNULL ( ( SELECT TOP 1 jt.AreaTrabajoDescripcion FROM ExpTrabajo et INNER JOIN AreaTrabajo jt ON jt.AreaTrabajoID = et.AreaTrabajoID WHERE et.ExpedienteNumero = e.ExpedienteNumero ORDER BY et.ExpTrabajoFecCrea DESC ), 'ND' ) 'Sector de trabajo',
	(CASE WHEN e.ExpedienteHijos = 3 THEN 'Si' ELSE 'No' END) 'Tiene hijos/as',
	(CASE WHEN e.ExpedienteHijos = 3 THEN e.ExpedienteHijosTener ELSE 0 END) 'Cantidad de hijos/as',
	'ND' AS 'Discapacidad',
	ISNULL ( ( SELECT TOP 1 ce.CentroEducNombre FROM ExpEducacion ee INNER JOIN CentroEduc ce on ce.CentroEducId = ee.CentroEducId WHERE ee.ExpedienteNumero = e.ExpedienteNumero ORDER BY ee.ExpEducacionUsrCrea DESC), 'ND' ) 'Centro de estudios',
	ISNULL ( ( SELECT TOP 1 nm.NivelMEPDescripcion FROM ExpEducacion ee INNER JOIN CentroEduc ce on ce.CentroEducId = ee.CentroEducId INNER JOIN NivelMEP nm on nm.NivelMEPId = ce.NivelMEPId WHERE ee.ExpedienteNumero = e.ExpedienteNumero ORDER BY ee.ExpEducacionUsrCrea DESC), 'ND' ) 'Modalidad',
	ISNULL ( ( SELECT TOP 1 d.DistritoDescripcion FROM Residencia r INNER JOIN Distrito d ON d.DistritoID = r.DistritoID WHERE  r.ExpedienteNumero = e.ExpedienteNumero ORDER BY r.ResidenciaFecCrea DESC ), 'ND' ) 'Distrito residencia',
	ISNULL ( ( SELECT TOP 1 c.CantonDescripcion FROM Residencia r INNER JOIN Distrito d ON d.DistritoID = r.DistritoID INNER JOIN Canton c ON c.CantonID = d.CantonID WHERE  r.ExpedienteNumero = e.ExpedienteNumero ORDER BY r.ResidenciaFecCrea DESC ), 'ND' ) 'Canton residencia', 
	ISNULL ( ( SELECT TOP 1 p.ProvinciaDescripcion FROM Residencia r INNER JOIN Distrito d ON d.DistritoID = r.DistritoID INNER JOIN Canton c ON c.CantonID = d.CantonID INNER JOIN Provincia p ON p.ProvinciaID = c.ProvinciaID ORDER BY r.ResidenciaFecCrea DESC ), 'ND' ) 'Provincia residencia'
FROM 
	Expediente e
	INNER JOIN Sexo s ON s.SexoID = e.SexoID 
	INNER JOIN Nacionalidad n ON n.NacionalidadID = e.NacionalidadID
	INNER JOIN NivelEducativo ne ON ne.NivelEducativoID = e.ExpedienteNivelEducativo GO
	
AFTER VIEW VW_procesos_formacion AS
SELECT 
	1 'Total', 
	DATEDIFF(YEAR, e.ExpedienteFecNac, GETDATE()) 'Edad actual', 
	dbo.fnc_get_rango_edad( e.ExpedienteFecNac ) 'Rango de edad',
	s.SexoDescripcion 'Sexo', 
	n.NacionalidadDescripcion 'Nacionalidad', 
	ISNULL ( ( SELECT c.CursoNombre FROM Curso c WHERE c.CursoCodigo = f.ProcesoFormativoCodigoCurso ), 'ND' ) 'Curso', 
	ISNULL ( ( SELECT cc.CentroCivicoDescripcion FROM Curso cu inner join CentroCivico cc on cc.CentroCivicoID = cu.CentroCivicoID WHERE cu.CursoCodigo = f.ProcesoFormativoCodigoCurso ), 'ND' ) 'Centro civico',
	ne.NivelEducativoDescripcion 'Educaci�n del estudiante',
	ISNULL ( ( SELECT TOP 1 'Si' FROM ExpTrabajo et WHERE et.ExpedienteNumero = e.ExpedienteNumero ORDER BY et.ExpTrabajoFecCrea DESC ), 'ND' ) 'Trabaja',
	ISNULL ( ( SELECT TOP 1 jt.JornadaTrabajoDescripcion FROM ExpTrabajo et INNER JOIN JornadaTrabajo jt ON jt.JornadaTrabajoID = et.JornadaTrabajoID WHERE et.ExpedienteNumero = e.ExpedienteNumero ORDER BY et.ExpTrabajoFecCrea DESC ), 'ND' ) 'Jornada de trabajo',
	(CASE WHEN e.ExpedienteHijos = 3 THEN 'Si' ELSE 'No' END) 'Tiene hijos',
	ISNULL ( ( SELECT TOP 1 ce.CentroEducNombre FROM ExpEducacion ee INNER JOIN CentroEduc ce on ce.CentroEducId = ee.CentroEducId WHERE ee.ExpedienteNumero = e.ExpedienteNumero ORDER BY ee.ExpEducacionUsrCrea DESC), 'ND' ) 'Centro de estudios',
	ISNULL ( ( SELECT TOP 1 nm.NivelMEPDescripcion FROM ExpEducacion ee INNER JOIN CentroEduc ce on ce.CentroEducId = ee.CentroEducId INNER JOIN NivelMEP nm on nm.NivelMEPId = ce.NivelMEPId WHERE ee.ExpedienteNumero = e.ExpedienteNumero ORDER BY ee.ExpEducacionUsrCrea DESC), 'ND' ) 'Modalidad',
	ISNULL ( ( SELECT TOP 1 d.DistritoDescripcion FROM Residencia r INNER JOIN Distrito d ON d.DistritoID = r.DistritoID WHERE �r.ExpedienteNumero = e.ExpedienteNumero ORDER BY r.ResidenciaFecCrea DESC ), 'ND' ) 'Distrito residencia',
	ISNULL ( ( SELECT TOP 1 c.CantonDescripcion FROM Residencia r INNER JOIN Distrito d ON d.DistritoID = r.DistritoID INNER JOIN Canton c ON c.CantonID = d.CantonID WHERE �r.ExpedienteNumero = e.ExpedienteNumero ORDER BY r.ResidenciaFecCrea DESC ), 'ND' ) 'Canton residencia', 
	ISNULL ( ( SELECT TOP 1 p.ProvinciaDescripcion FROM Residencia r INNER JOIN Distrito d ON d.DistritoID = r.DistritoID INNER JOIN Canton c ON c.CantonID = d.CantonID INNER JOIN Provincia p ON p.ProvinciaID = c.ProvinciaID ORDER BY r.ResidenciaFecCrea DESC ), 'ND' ) 'Provincia residencia'
FROM 
	ProcesoFormativo f 
	INNER JOIN Expediente e ON f.ExpedienteNumero = e.ExpedienteNumero 
	INNER JOIN Sexo s ON s.SexoID = e.SexoID 
	INNER JOIN Nacionalidad n ON n.NacionalidadID = e.NacionalidadID
	INNER JOIN NivelEducativo ne ON ne.NivelEducativoID = e.ExpedienteNivelEducativo
*/