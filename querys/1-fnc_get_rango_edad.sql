-- ================================================
-- Template generated from Template Explorer using:
-- Create Scalar Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		William Vides
-- Create date: 20200404
-- Description:	Retorna la categoria de rango de edad
-- =============================================
DROP FUNCTION  fnc_get_rango_edad;
CREATE FUNCTION fnc_get_rango_edad(
	@fecha DATE
)
RETURNS varchar(14)
AS
BEGIN
	DECLARE @edad INT;
	
	SELECT @edad = DATEDIFF(YEAR, @fecha, GETDATE());
	IF ( @edad < 0 ) 
		RETURN '---';
	ELSE IF ( @edad <= 6 )
		RETURN '00 - 06 a�os';
	ELSE IF ( @edad <= 12 )
		RETURN '07 - 12 a�os';
	ELSE IF ( @edad <= 17 )
		RETURN '13 - 17 a�os';
	ELSE IF ( @edad <= 24 )
		RETURN '18 - 24 a�os';
	ELSE IF ( @edad <= 35 )
		RETURN '25 - 35 a�os';
	ELSE IF ( @edad <= 64 )
		RETURN '36 - 64 a�os';
	ELSE
		RETURN '65 a�os+';

	RETURN '---';
END
GO