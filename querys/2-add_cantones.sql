SELECT p.ProvinciaID , p.ProvinciaDescripcion, c.CantonID , c.CantonDescripcion 
FROM Provincia p 
	inner join Canton c on c.ProvinciaID = p.ProvinciaID 
ORDER BY p.ProvinciaDescripcion , c.CantonDescripcion ;

-- INSERT INTO CCP.dbo.Canton (CantonDescripcion, ProvinciaID) VALUES ('Rio Cuarto', 2), ('Sarch�', 2);
UPDATE CCP.dbo.Provincia SET ProvinciaDescripcion = TRIM( ProvinciaDescripcion ) ;
UPDATE CCP.dbo.Canton SET CantonDescripcion= TRIM( CantonDescripcion ) ;
UPDATE CCP.dbo.Distrito SET DistritoDescripcion = TRIM( DistritoDescripcion ) ;



