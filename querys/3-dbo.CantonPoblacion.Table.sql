USE [CCP]
GO
/****** Object:  Table [dbo].[CantonPoblacion]    Script Date: 24/5/2020 22:53:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CantonPoblacion](
	[CantonID] [int] NOT NULL,
	[CantonPoblacionAnio] [int] NOT NULL,
	[CantonPoblacionEdad] [smallint] NOT NULL,
	[CantonPoblacionTotal] [real] NOT NULL,
	[CantonPoblacionHombre] [real] NOT NULL,
	[CantonPoblacionMujer] [real] NOT NULL,
 CONSTRAINT [PK_Canton_Poblacion] PRIMARY KEY CLUSTERED 
(
	[CantonID] ASC,
	[CantonPoblacionAnio] ASC,
	[CantonPoblacionEdad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CantonPoblacion]  WITH CHECK ADD FOREIGN KEY([CantonID])
REFERENCES [dbo].[Canton] ([CantonID])
GO
ALTER TABLE [dbo].[CantonPoblacion]  WITH CHECK ADD CHECK  (([CantonPoblacionAnio]>=(1980)))
GO
ALTER TABLE [dbo].[CantonPoblacion]  WITH CHECK ADD CHECK  (([CantonPoblacionEdad]>=(0)))
GO
ALTER TABLE [dbo].[CantonPoblacion]  WITH CHECK ADD CHECK  (([CantonPoblacionTotal]>=(0)))
GO
ALTER TABLE [dbo].[CantonPoblacion]  WITH CHECK ADD CHECK  (([CantonPoblacionHombre]>=(0)))
GO
ALTER TABLE [dbo].[CantonPoblacion]  WITH CHECK ADD CHECK  (([CantonPoblacionMujer]>=(0)))
GO
