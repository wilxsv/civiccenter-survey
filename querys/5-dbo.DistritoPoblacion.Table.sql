USE [CCP]
GO
/****** Object:  Table [dbo].[DistritoPoblacion]    Script Date: 24/5/2020 22:53:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DistritoPoblacion](
	[DistritoID] [int] NOT NULL,
	[DistritoPoblacionAnio] [int] NOT NULL,
	[DistritoPoblacionEdad] [smallint] NOT NULL,
	[DistritoPoblacionTotal] [real] NOT NULL,
	[DistritoPoblacionHombre] [real] NOT NULL,
	[DistritoPoblacionMujer] [real] NOT NULL,
 CONSTRAINT [PK_Distrito_Poblacion] PRIMARY KEY CLUSTERED 
(
	[DistritoID] ASC,
	[DistritoPoblacionAnio] ASC,
	[DistritoPoblacionEdad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DistritoPoblacion]  WITH CHECK ADD FOREIGN KEY([DistritoID])
REFERENCES [dbo].[Distrito] ([DistritoID])
GO
ALTER TABLE [dbo].[DistritoPoblacion]  WITH CHECK ADD CHECK  (([DistritoPoblacionAnio]>=(1980)))
GO
ALTER TABLE [dbo].[DistritoPoblacion]  WITH CHECK ADD CHECK  (([DistritoPoblacionEdad]>=(0)))
GO
ALTER TABLE [dbo].[DistritoPoblacion]  WITH CHECK ADD CHECK  (([DistritoPoblacionTotal]>=(0)))
GO
ALTER TABLE [dbo].[DistritoPoblacion]  WITH CHECK ADD CHECK  (([DistritoPoblacionHombre]>=(0)))
GO
ALTER TABLE [dbo].[DistritoPoblacion]  WITH CHECK ADD CHECK  (([DistritoPoblacionMujer]>=(0)))
GO
