-- Area de trabajo
ALTER TABLE CCP.dbo.AreaTrabajo ADD AreaTrabajo_ID int NULL ;
ALTER TABLE CCP.dbo.AreaTrabajo ADD CONSTRAINT AreaTrabajo_FK FOREIGN KEY (AreaTrabajo_ID) REFERENCES CCP.dbo.AreaTrabajo(AreaTrabajoID) ;
ALTER TABLE CCP.dbo.AreaTrabajo ALTER COLUMN AreaTrabajoDescripcion varchar(100) COLLATE Modern_Spanish_CI_AS NOT NULL ;
INSERT INTO AreaTrabajo (AreaTrabajoID, AreaTrabajoDescripcion, AreaTrabajo_ID) VALUES(10, 'Sector primario', NULL);
INSERT INTO AreaTrabajo (AreaTrabajoID, AreaTrabajoDescripcion, AreaTrabajo_ID) VALUES(11, 'Agricultura, ganader�a y pesca', 10);
INSERT INTO AreaTrabajo (AreaTrabajoID, AreaTrabajoDescripcion, AreaTrabajo_ID) VALUES(12, 'Sector secundario', NULL);
INSERT INTO AreaTrabajo (AreaTrabajoID, AreaTrabajoDescripcion, AreaTrabajo_ID) VALUES(13, 'Industria manufacturera', 12);
INSERT INTO AreaTrabajo (AreaTrabajoID, AreaTrabajoDescripcion, AreaTrabajo_ID) VALUES(14, 'Construcci�n', 12);
INSERT INTO AreaTrabajo (AreaTrabajoID, AreaTrabajoDescripcion, AreaTrabajo_ID) VALUES(15, 'Otros', 12);
INSERT INTO AreaTrabajo (AreaTrabajoID, AreaTrabajoDescripcion, AreaTrabajo_ID) VALUES(16, 'Sector comercio y servicios', NULL);
INSERT INTO AreaTrabajo (AreaTrabajoID, AreaTrabajoDescripcion, AreaTrabajo_ID) VALUES(17, 'Comercio y reparaci�n', 16);
INSERT INTO AreaTrabajo (AreaTrabajoID, AreaTrabajoDescripcion, AreaTrabajo_ID) VALUES(18, 'Transporte y almacenamiento', 16);
INSERT INTO AreaTrabajo (AreaTrabajoID, AreaTrabajoDescripcion, AreaTrabajo_ID) VALUES(19, 'Hoteles y restaurantes', 16);
INSERT INTO AreaTrabajo (AreaTrabajoID, AreaTrabajoDescripcion, AreaTrabajo_ID) VALUES(20, 'Intermediaci�n financiera y de seguros', 16);
INSERT INTO AreaTrabajo (AreaTrabajoID, AreaTrabajoDescripcion, AreaTrabajo_ID) VALUES(21, 'Actividades profesionales y administrativas de apoyo', 16);
INSERT INTO AreaTrabajo (AreaTrabajoID, AreaTrabajoDescripcion, AreaTrabajo_ID) VALUES(22, 'Administraci�n p�blica', 16);
INSERT INTO AreaTrabajo (AreaTrabajoID, AreaTrabajoDescripcion, AreaTrabajo_ID) VALUES(23, 'Ense�anza y salud', 16);
INSERT INTO AreaTrabajo (AreaTrabajoID, AreaTrabajoDescripcion, AreaTrabajo_ID) VALUES(24, 'Comunicaci�n y otros servicios', 16);
INSERT INTO AreaTrabajo (AreaTrabajoID, AreaTrabajoDescripcion, AreaTrabajo_ID) VALUES(25, 'Hogares como empleadores', 16);
INSERT INTO AreaTrabajo (AreaTrabajoID, AreaTrabajoDescripcion, AreaTrabajo_ID) VALUES(26, 'No especificado', NULL);

-- Discapacidad
CREATE TABLE Discapacidad (
	DiscapacidadID int IDENTITY(1,1) NOT NULL,
	DiscapacidadDescripcion varchar(120) COLLATE Modern_Spanish_CI_AS NOT NULL,
    CONSTRAINT UN_DiscapacidadDescripcion UNIQUE(DiscapacidadDescripcion),  
	CONSTRAINT PK_Discapacidad PRIMARY KEY (DiscapacidadID)
) ;
INSERT INTO Discapacidad (DiscapacidadDescripcion) VALUES('F�sica o motora'),('Sensorial auditiva'),('Sensorial visual'),('Sensorial del habla'),('Intelectual'),('Mental');

-- INTER�S EN OTRAS MODALIDADES EDUCATIVAS
CREATE TABLE InteresModalidadEducativa (
	IModalidadEducativaID int IDENTITY(1,1) NOT NULL,
	IModalidadEducativaDescripcion varchar(120) COLLATE Modern_Spanish_CI_AS NOT NULL,
    CONSTRAINT UN_IModalidadEducativaDescripcion UNIQUE(IModalidadEducativaDescripcion),  
	CONSTRAINT PK_IModalidadEducativa PRIMARY KEY (IModalidadEducativaID)
) ;
INSERT INTO InteresModalidadEducativa (IModalidadEducativaDescripcion) VALUES('Por madurez'),('A distancia'),('T�cnico Nocturno'),('Acad�mico Nocturno '),('Virtual'),('Liceo Rural (telesecundaria)'),('Facilidad para personas con discapacidades '),('Otro'),('NS/NR');

-- QU� NECESITA PARA VOLVER A ESTUDIAR 
CREATE TABLE NecesitaVolverAEstudiar (
	NecesitaVolverAEstudiarID int IDENTITY(1,1) NOT NULL,
	NecesitaVolverAEstudiarDescripcion varchar(120) COLLATE Modern_Spanish_CI_AS NOT NULL,
    CONSTRAINT UN_NecesitaVolverAEstudiarDescripcion UNIQUE(NecesitaVolverAEstudiarDescripcion),  
	CONSTRAINT PK_NecesitaVolverAEstudiar PRIMARY KEY (NecesitaVolverAEstudiarID)
) ;
INSERT INTO NecesitaVolverAEstudiar (NecesitaVolverAEstudiarDescripcion) VALUES('Apoyo educativo (acompa�amiento, t�cnicas de estudio, etc.)'),('Tutor�as acad�micas '),('Beca u otro apoyo econ�mico'),('M�s informaci�n sobre modalidades educativas'),('Acompa�amiento para realizar tr�mites'),('Alguna opci�n educativa con horarios flexibles'),('Otro'),('NS/NR');

-- RAZONES PORQUE NO ESTUDIA 
CREATE TABLE RazonNoEstudia (
	RazonNoEstudiaID int IDENTITY(1,1) NOT NULL,
	RazonNoEstudiaDescripcion varchar(120) COLLATE Modern_Spanish_CI_AS NOT NULL,
    CONSTRAINT UN_RazonNoEstudiaDescripcion UNIQUE(RazonNoEstudiaDescripcion),  
	CONSTRAINT PK_RazonNoEstudia PRIMARY KEY (RazonNoEstudiaID)
) ;
INSERT INTO RazonNoEstudia (RazonNoEstudiaDescripcion) VALUES('Por trabajo '),('Por cuido a otras personas o por realizaci�n de trabajo dom�stico'),('Porque no puede pagar estudios'),('Falta de transporte p�blico accesible'),('Falta de infraestructura accesible'),('Falta de apoyos educativos (Lesco, tecnol�gicos, adecuaciones curriculares o docentes)'),('Falta de Apoyo Familiar '),('Por el m�todo de ense�anza'),('Porque asiste a educaci�n no formal, o no le interesa el aprendizaje formal'),('Enfermedad o condici�n de salud'),('Falta ganar pruebas del MEP o ex�menes de admisi�n '),('Hostigamiento en el centro educativo'),('Otro'),('NS/NR');

-- DIFICULTADES PARA PERMANECER EN SISTEMA EDUCATIVO  
CREATE TABLE DificultadPermanecerSistemaEducativo (
	PermaneceSEducativoID int IDENTITY(1,1) NOT NULL,
	PermaneceSEducativoDescripcion varchar(120) COLLATE Modern_Spanish_CI_AS NOT NULL,
    CONSTRAINT UN_PermaneceSEducativoDescripcion UNIQUE(PermaneceSEducativoDescripcion),  
	CONSTRAINT PK_PermaneceSEducativo PRIMARY KEY (PermaneceSEducativoID)
) ;
INSERT INTO DificultadPermanecerSistemaEducativo (PermaneceSEducativoDescripcion) VALUES('Por trabajo'),('Por cuido a otras personas o por realizaci�n de trabajo dom�stico'),('Porque no puede pagar estudios'),('Falta de transporte p�blico accesible'),('Falta de infraestructura accesible'),('Falta de apoyos educativos (Lesco, tecnol�gicos, adecuaciones curriculares o docentes)'),('Por el m�todo de ense�anza'),('Porque asiste a educaci�n no formal, o no le interesa el aprendizaje formal'),('Enfermedad o condici�n de salud'),('Falta ganar pruebas del MEP o ex�menes de admisi�n'),('Hostigamiento en el centro educativo'),('Otro'),('NS/NR');
