use ccp go;

/* Distribuci�n de Personas Usuarias seg�n �rea de Influencia CCP 
-- Mujeres
SELECT cc.CentroCivicoDescripcion, COUNT(1) AS total
  FROM CentroCivico cc 
  INNER JOIN Expediente ex ON SUBSTRING( ex.ExpedienteNumero , 1, 3 ) = cc.CentroCivicoSiglas
WHERE CentroCivicoID < 8 AND ex.SexoID = 2
GROUP BY cc.CentroCivicoDescripcion
ORDER BY cc.CentroCivicoDescripcion
-- Hombres
SELECT cc.CentroCivicoDescripcion, COUNT(1) AS total
  FROM CentroCivico cc 
  INNER JOIN Expediente ex ON SUBSTRING( ex.ExpedienteNumero , 1, 3 ) = cc.CentroCivicoSiglas
WHERE CentroCivicoID < 8 AND ex.SexoID = 1
GROUP BY cc.CentroCivicoDescripcion
ORDER BY cc.CentroCivicoDescripcion
-- Total
SELECT cc.CentroCivicoDescripcion, COUNT(1) AS total
  FROM CentroCivico cc 
  INNER JOIN Expediente ex ON SUBSTRING( ex.ExpedienteNumero , 1, 3 ) = cc.CentroCivicoSiglas
WHERE CentroCivicoID < 8
GROUP BY cc.CentroCivicoDescripcion
ORDER BY cc.CentroCivicoDescripcion
*/

/* Permanencia de Personas Usuarias en Procesos Formativos CCP (Cantidad de Personas usuarias que finalizan procesos, distribuidas por sexo, vrs. los que no)
-- Concluye
SELECT s.SexoDescripcion, COUNT(1) AS total FROM ProcesoFormativo p INNER JOIN Expediente e ON e.ExpedienteNumero = p.ExpedienteNumero INNER JOIN Sexo s ON s.SexoID = e.SexoID WHERE p.ProcesoFormativoEstadoProceso = 28 GROUP BY s.SexoDescripcion 
--No concluye
SELECT s.SexoDescripcion, COUNT(1) AS total FROM ProcesoFormativo p INNER JOIN Expediente e ON e.ExpedienteNumero = p.ExpedienteNumero INNER JOIN Sexo s ON s.SexoID = e.SexoID WHERE p.ProcesoFormativoEstadoProceso != 28 GROUP BY s.SexoDescripcion 
*/
/* Permanencia de Personas Usuarias del CCP en el Sistema Educativo (Cantidad de Personas usuarias, distribuidas por sexo, seg�n su permanencia en el Sistema Educativo o no, para el rango de edad de: 7-12 a�os y de 13-17 a�os)*/
/*
SELECT re.RespuestaDescripcion, COUNT(1) AS total 
FROM Expediente ex INNER JOIN ExpEducacion ee ON ex.ExpedienteNumero = ee.ExpedienteNumero
     INNER JOIN Respuesta re ON re.RespuestaID = ee.ExpEducacionNivelEduc
WHERE ex.SexoID = 1 AND DATEDIFF(YEAR, ex.ExpedienteFecNac, GETDATE()) >= 7 AND DATEDIFF(YEAR, ex.ExpedienteFecNac, GETDATE()) <= 12
GROUP BY re.RespuestaDescripcion
ORDER BY re.RespuestaDescripcion 
SELECT re.RespuestaDescripcion, COUNT(1) AS total 
FROM Expediente ex INNER JOIN ExpEducacion ee ON ex.ExpedienteNumero = ee.ExpedienteNumero
     INNER JOIN Respuesta re ON re.RespuestaID = ee.ExpEducacionNivelEduc
WHERE ex.SexoID = 2 AND DATEDIFF(YEAR, ex.ExpedienteFecNac, GETDATE()) >= 7 AND DATEDIFF(YEAR, ex.ExpedienteFecNac, GETDATE()) <= 12
GROUP BY re.RespuestaDescripcion
ORDER BY re.RespuestaDescripcion 
-- 
SELECT re.RespuestaDescripcion, COUNT(1) AS total 
FROM Expediente ex INNER JOIN ExpEducacion ee ON ex.ExpedienteNumero = ee.ExpedienteNumero
     INNER JOIN Respuesta re ON re.RespuestaID = ee.ExpEducacionNivelEduc
WHERE ex.SexoID = 1 AND DATEDIFF(YEAR, ex.ExpedienteFecNac, GETDATE()) >= 13 AND DATEDIFF(YEAR, ex.ExpedienteFecNac, GETDATE()) <= 17
GROUP BY re.RespuestaDescripcion
ORDER BY re.RespuestaDescripcion 
SELECT re.RespuestaDescripcion, COUNT(1) AS total 
FROM Expediente ex INNER JOIN ExpEducacion ee ON ex.ExpedienteNumero = ee.ExpedienteNumero
     INNER JOIN Respuesta re ON re.RespuestaID = ee.ExpEducacionNivelEduc
WHERE ex.SexoID = 2 AND DATEDIFF(YEAR, ex.ExpedienteFecNac, GETDATE()) >= 13 AND DATEDIFF(YEAR, ex.ExpedienteFecNac, GETDATE()) <= 17
GROUP BY re.RespuestaDescripcion
ORDER BY re.RespuestaDescripcion 
*/

/*
 * Porcentaje de Cobertura de Oferta Formativa CCP seg�n �rea de Influencia
 * 
 * */

SELECT 
	c.CantonDescripcion, cp.CantonPoblacionTotal, d.DistritoDescripcion, dp.DistritoPoblacionTotal , CAST( (100*dp.DistritoPoblacionTotal/cp.CantonPoblacionTotal) AS DECIMAL(5,2)) 'porcent'
FROM Canton c INNER JOIN CantonPoblacion cp ON cp.CantonID = c.CantonID 
	INNER JOIN Distrito d ON d.CantonID = c.CantonID INNER JOIN DistritoPoblacion dp ON dp.DistritoID = d.DistritoID 
ORDER BY c.CantonDescripcion, d.DistritoDescripcion


-- SELECT s.SexoDescripcion,  COUNT(1) total FROM Expediente e INNER JOIN Sexo s ON e.SexoID = s.SexoID GROUP BY s.SexoDescripcion ORDER BY 1;
-- SELECT e.edad 'Edad (en en a�os cumplidos)',  COUNT(1) total FROM ( SELECT DATEDIFF(YEAR, el.ExpedienteFecNac, GETDATE()) 'edad' FROM Expediente el ) e GROUP BY e.edad ORDER BY 1;
-- SELECT COUNT(1) FROM Expediente el INNER JOIN Residencia r ON el.ExpedienteNumero = r.ExpedienteNumero;
/*
SELECT * FROM Expediente e 
	INNER JOIN Residencia r ON r.ExpedienteNumero = e.ExpedienteNumero
	INNER JOIN Distrito d ON d.DistritoID = r.DistritoID
	INNER JOIN Canton c ON c.CantonID = d.CantonID
	INNER JOIN Provincia p ON p.ProvinciaID = c.ProvinciaID
*/
-- SELECT n.NacionalidadDescripcion 'Nacionalidad', COUNT(1) FROM Expediente e INNER JOIN Nacionalidad n ON n.NacionalidadID = e.NacionalidadID GROUP BY n.NacionalidadDescripcion ORDER BY 1;

SELECT ExpedienteEducOtroNivel,ExpedienteEducUltAnno,ExpedienteEducActual,ExpedienteEducEstudio, ExpedienteEducConcluirEst,  ExpedienteNivelEducativo   FROM Expediente e 

*-- SELECT e.ExpedienteHijosCant, e.ExpedienteHijosTener, e.ExpedienteHijos FROM Expediente e group by e.ExpedienteHijosCant, e.ExpedienteHijosTener, e.ExpedienteHijos order by e.ExpedienteHijosCant, e.ExpedienteHijosTener, e.ExpedienteHijos