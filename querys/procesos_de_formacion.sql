
SELECT 
	1 'Total', 
	DATEDIFF(YEAR, e.ExpedienteFecNac, GETDATE()) 'Edad actual', 
	dbo.fnc_get_rango_edad( e.ExpedienteFecNac ) 'Rango de edad',
	s.SexoDescripcion 'Sexo', 
	n.NacionalidadDescripcion 'Nacionalidad', 
	ISNULL ( ( SELECT c.CursoNombre FROM Curso c WHERE c.CursoCodigo = f.ProcesoFormativoCodigoCurso ), 'ND' ) 'Curso', 
	ISNULL ( ( SELECT cc.CentroCivicoDescripcion FROM Curso cu inner join CentroCivico cc on cc.CentroCivicoID = cu.CentroCivicoID WHERE cu.CursoCodigo = f.ProcesoFormativoCodigoCurso ), 'ND' ) 'Centro civico',
	ne.NivelEducativoDescripcion 'Educación del estudiante',
	ISNULL ( ( SELECT TOP 1 'Si' FROM ExpTrabajo et WHERE et.ExpedienteNumero = e.ExpedienteNumero ORDER BY et.ExpTrabajoFecCrea DESC ), 'ND' ) 'Trabaja',
	ISNULL ( ( SELECT TOP 1 jt.JornadaTrabajoDescripcion FROM ExpTrabajo et INNER JOIN JornadaTrabajo jt ON jt.JornadaTrabajoID = et.JornadaTrabajoID WHERE et.ExpedienteNumero = e.ExpedienteNumero ORDER BY et.ExpTrabajoFecCrea DESC ), 'ND' ) 'Jornada de trabajo',
	(CASE WHEN e.ExpedienteHijos = 3 THEN 'Si' ELSE 'No' END) 'Tiene hijos',
	ISNULL ( ( SELECT TOP 1 ce.CentroEducNombre FROM ExpEducacion ee INNER JOIN CentroEduc ce on ce.CentroEducId = ee.CentroEducId WHERE ee.ExpedienteNumero = e.ExpedienteNumero ORDER BY ee.ExpEducacionUsrCrea DESC), 'ND' ) 'Centro de estudios',
	ISNULL ( ( SELECT TOP 1 nm.NivelMEPDescripcion FROM ExpEducacion ee INNER JOIN CentroEduc ce on ce.CentroEducId = ee.CentroEducId INNER JOIN NivelMEP nm on nm.NivelMEPId = ce.NivelMEPId WHERE ee.ExpedienteNumero = e.ExpedienteNumero ORDER BY ee.ExpEducacionUsrCrea DESC), 'ND' ) 'Modalidad'
FROM 
	ProcesoFormativo f 
	INNER JOIN Expediente e ON f.ExpedienteNumero = e.ExpedienteNumero 
	INNER JOIN Sexo s ON s.SexoID = e.SexoID 
	INNER JOIN Nacionalidad n ON n.NacionalidadID = e.NacionalidadID
	INNER JOIN NivelEducativo ne ON ne.NivelEducativoID = e.ExpedienteNivelEducativo
;

/*
SELECT 
	count(1) 'Total'
FROM 
	ProcesoFormativo f 
	INNER JOIN Curso c ON c.CursoCodigo = f.ProcesoFormativoCodigoCurso
*/